export interface Booking {
    link: string // link to admin if superuser, link to file if not
    unique_id: string
    booking_date: string
    cancelled: boolean
    name: string // display name
    booked_total: string
    status: {
        procentual: number
        text: string
    }
}
