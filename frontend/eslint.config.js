import eslint from '@eslint/js';
import pluginVue from 'eslint-plugin-vue'
import stylisticJs from '@stylistic/eslint-plugin-js'
import vueTsEslintConfig from "@vue/eslint-config-typescript"

export default [
    eslint.configs.recommended,
    ...pluginVue.configs['flat/recommended'],
    ...vueTsEslintConfig(),

    {
        plugins: { '@stylistic/js': stylisticJs },
        rules: {
            "@stylistic/js/comma-dangle": [
                "error", "always-multiline",
            ],

            "@stylistic/js/object-curly-newline": [
                "error", {
                    multiline:true,
                    minProperties: 2, 
                },
            ],

            "@stylistic/js/array-bracket-newline": [
                "error", {
                    multiline: true,
                    minItems: 2, 
                },
            ],

            "@stylistic/js/object-curly-spacing": [
                "error", "always",
            ],

            "@stylistic/js/array-bracket-spacing": [
                "error", "never",
            ],

            "@stylistic/js/array-element-newline": [
                "error", { minItems: 3 },
            ],

            "@stylistic/js/object-property-newline": [
                "error", { allowAllPropertiesOnSameLine: false },
            ],

            "@stylistic/js/indent": [
                "error", 4,
            ],
            "vue/html-indent": [
                "error", 4,
            ],

            "vue/component-name-in-template-casing": [
                "error",
                "PascalCase",
                {
                    registeredComponentsOnly: false,
                    ignores: [],
                },
            ],

            "tailwindcss/no-custom-classname": ["off"],

            "vue/block-lang": [
                "error", { script: { lang: "ts" } },
            ],
        }, 
    },
    {
        files: ["**/pages/**/*.{js,ts,vue}"],

        rules: {
            "vue/multi-word-component-names": "off",
            "vue/no-multiple-template-root": "error",
        },
    },
];