import type { App } from 'vue'
import PrimeVue, { type PrimeVueConfiguration } from 'primevue/config'

export default {
    install(app: App) {
        const config: PrimeVueConfiguration = {}
        app.use(PrimeVue, config)    
    }, 
}
