import { defineConfig } from 'cypress'
import coverageTask from "@cypress/code-coverage/task"

export default defineConfig({
    env: { codeCoverage: { exclude: 'cypress/**/*.*' } },
    e2e: {
        baseUrl: 'http://localhost',
        setupNodeEvents(on, config) {
            coverageTask(on, config)
            return config
        },
    },
    video: false,
    screenshotOnRunFailure: false,
})
