describe('Dashboard - Recent Items Card', () => {
    beforeEach(() => {
        cy.visit('/api-auth/login?next=/new/')

        cy.get('#id_username').type('superuser')

        cy.get('#id_password').type('pw')

        cy.get('input[type="submit"]').click()

        cy.visit('/new/')
    })

    it('shows recent invoices', () => {
        cy.get('[cy-index-recentinvoices]').find('[cy-invoicerow-requester-name="200"]').contains('Megaparty').should('be.visible')
        cy.get('[cy-index-recentinvoices]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item="/admin/core/receivedinvoice/1/change"]').should('exist')
        cy.get('[cy-index-recentinvoices]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item-status="/admin/core/receivedinvoice/1/change"]').contains('Rechnung eingegangen')
        cy.get('[cy-index-recentinvoices]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item-due="/admin/core/receivedinvoice/1/change"]').contains('1 days ago')
    })
    
    
    it('does not show requesters that do not have recent items', () => {
        cy.get('[cy-index-recentinvoices]').get('[cy-invoicerow-requester-name="202"]').should('not.exist')
        cy.get('[cy-index-tabview-recent]').find('li[data-pc-index="1"]').find('a').click()
        cy.get('[cy-index-recentrequests]').get('[cy-invoicerow-requester-name="202"]').should('not.exist')
    })

    it('shows due requests', () => {
        cy.get('[cy-index-tabview-recent]').find('li[data-pc-index="1"]').find('a').click()
        cy.get('[cy-index-recentrequests]').find('[cy-invoicerow-requester-name="200"]').contains('Megaparty').should('be.visible')
        cy.get('[cy-index-recentrequests]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item="/admin/core/request/1/change"]').should('exist')
        cy.get('[cy-index-recentrequests]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item-status="/admin/core/request/1/change"]').contains('Antrag eingegangen')
        cy.get('[cy-index-recentrequests]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item-due="/admin/core/request/1/change"]').contains('1 days ago')
    })

})