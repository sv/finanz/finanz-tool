describe('Dashboard - Stale Items Card', () => {
    beforeEach(() => {
        cy.visit('/api-auth/login?next=/new/')

        cy.get('#id_username').type('superuser')

        cy.get('#id_password').type('pw')

        cy.get('input[type="submit"]').click()

        cy.visit('/new/')
    })

    it('shows stale invoices', () => {
        cy.get('[cy-index-staleinvoices-badge]').contains('Erfordert Aktion!')
        cy.get('[cy-index-staleinvoices]').find('[cy-invoicerow-requester-name="200"]').contains('Megaparty').should('be.visible')
        cy.get('[cy-index-staleinvoices]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item="/admin/core/receivedinvoice/1001/change"]').should('exist')
        cy.get('[cy-index-staleinvoices]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item-status="/admin/core/receivedinvoice/1001/change"]').contains('Rechnung eingegangen')
        cy.get('[cy-index-staleinvoices]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item-due="/admin/core/receivedinvoice/1001/change"]').contains('61 days ago')
    })
    
    
    it.skip('does not show requesters that do not have stale invoices', () => {
        cy.get('[cy-index-staleinvoices]').get('[cy-invoicerow-requester-name="201"]').should('not.exist')
        cy.get('[cy-index-tabview-stale]').find('li[data-pc-index="1"]').find('a').click()
        cy.get('[cy-index-stalerequests] [cy-invoicerow-requester-name="201"]').should('not.exist')
    })

    it('shows stale requests', () => {
        cy.get('[cy-index-tabview-stale]').find('li[data-pc-index="1"]').find('a').click()
        cy.get('[cy-index-stalerequests]').find('[cy-invoicerow-requester-name="200"]').contains('Megaparty').should('be.visible')
        cy.get('[cy-index-stalerequests]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item="/admin/core/request/222/change"]').should('exist')
        cy.get('[cy-index-stalerequests]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item-status="/admin/core/request/222/change"]').contains('Antrag eingegangen')
        cy.get('[cy-index-stalerequests]').find('[cy-invoicerow-requester="200"]').find('[cy-invoicerow-item-due="/admin/core/request/222/change"]').contains('91 days ago')
    })
})