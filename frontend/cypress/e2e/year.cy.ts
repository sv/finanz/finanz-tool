describe('Year View', () => {
    beforeEach(() => {
        cy.visit('/api-auth/login?next=/new/')

        cy.get('#id_username').type('user')

        cy.get('#id_password').type('pw')

        cy.get('input[type="submit"]').click()

        cy.visit('/new/year/2023/0')
    })

    it('lists subbudgets correctly', () => {
        cy.get('[cy-annualplan-tabview-fond="100"]').parent().click()
        cy.get('[cy-budgettreetablerow-tr="100"]').should('exist')   

        cy.get('[cy-annualplan-tabview-fond="200"]').parent().click()
        cy.get('[cy-budgettreetablerow-tr="200"]').should('exist')
        cy.get('[cy-budgettreetablerow-tr="220"]').should('not.exist')
        cy.get('[cy-budgettreetablerow-pbutton-expand="200"]').click()
        // Show subbudget correctly (depth 1)
        cy.get('[cy-budgettreetablerow-tr="220"]').should('exist')
        cy.get('[cy-budgettreetablerow-tag-budget="220"]').should('exist')
        cy.get('[cy-budgettreetablerow-tag-request="220"]').should('not.exist')
        cy.get('[cy-budgettreetablerow-tag-status="220"]').contains('Antrag eingegangen')
        cy.get('[cy-budgettreetablerow-pbutton-expand="220"]').click()

        // Show subrequest correctly (depth 2)
        cy.get('[cy-budgettreetablerow-tr="222"]').should('exist')
        cy.get('[cy-budgettreetablerow-tag-request="222"]').should('not.exist')
        cy.get('[cy-budgettreetablerow-tag-budget="222"]').should('exist')

        // Show request tag
        cy.get('[cy-budgettreetablerow-pbutton-expand="250"]').click()
        cy.get('[cy-budgettreetablerow-tr="221"]').should('exist')
        cy.get('[cy-budgettreetablerow-tag-request="221"]').should('exist')
        cy.get('[cy-budgettreetablerow-tag-budget="221"]').should('not.exist')
    })

    it('shows error', () => {
        cy.get('[cy-annualplan-tabview-fond="200"]').parent().click()
        cy.get('[cy-budgettreetablerow-create-budget="200"]').click()
        cy.get('[cy-budgetmodal-pbutton-submit]').click()

        cy.get('[cy-budgetmodal-inputtext-name-help]').contains('Dieses Feld darf nicht leer sein.')
        cy.get('[cy-budgetmodal-inputtext-name]').should('exist')
    })

    it('open and close budget modal', () => {
        cy.get('[cy-annualplan-tabview-fond="200"]').parent().click()
        // Root level
        cy.get('[cy-budgetmodal-inputtext-name]').should('not.exist')
        cy.get('[cy-budgettreetablerow-create-budget="200"]').click()
        cy.get('[cy-budgetmodal-span-parent]').contains('Megaparty')
        cy.get('[cy-budgetmodal-inputtext-name]').should('exist')
        cy.get('[cy-budgetmodal-dialog]').within(() => {
            cy.get('.p-dialog-header-close').click()
        })
        cy.get('[cy-budgetmodal-inputtext-name]').should('not.exist')

        cy.get('[cy-budgettreetablerow-pbutton-expand="200"]').click()
        cy.get('[cy-budgettreetablerow-create-budget="210"]').click()
        cy.get('[cy-budgetmodal-span-parent]').contains('Fixkosten')
        cy.get('[cy-budgetmodal-inputtext-name]').should('exist')
        cy.get('[cy-budgetmodal-dialog]').within(() => {
            cy.get('.p-dialog-header-close').click()
        })
        cy.get('[cy-budgetmodal-inputtext-name]').should('not.exist')
    })

    it('creates budget', () => {
        cy.get('[cy-annualplan-tabview-fond="200"]').parent().click()
        cy.get('[cy-budgettreetablerow-create-budget="200"]').click()

        // Add mock data
        cy.get('[cy-budgetmodal-inputtext-name]').type('Test')
        cy.get('[cy-budgetmodal-inputtext-target]').type('200.00')

        cy.get('[cy-budgetmodal-pbutton-submit]').click()

        cy.get('[cy-budgetmodal-pbutton-submit]').should('not.exist')

        // Check if item exits
        cy.get('[cy-budgettreetablerow-pbutton-expand="200"]').click()
        cy.get('[cy-budgettreetablerow-tr="2"]').should('exist')
        cy.get('[cy-budgettreetablerow-span-name="2"]').contains('Test')
        cy.get('[cy-budgettreetablerow-tag-budget="2"]').should('exist')
        cy.get('[cy-budgettreetablerow-euroamount-target="2"]').contains('200.00€')
    })

    it('edit budget', () => {
        // TODO
    })
})
