import { createFetch } from '@vueuse/core'
import Cookies from 'universal-cookie'

export async function login() {
    const cookies = new Cookies()
    const useMyFetch = createFetch({
        fetchOptions: {
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': cookies.get('csrftoken'),
            },
        }, 
    })
    await useMyFetch(`/api/login/`, {
        method: 'POST',
        body: null,
    })
}

describe('login correctly', () => {
    it('logging in via gui', () => {
        cy.visit('/api-auth/login?next=/new/')

        cy.get('#id_username').type('user')

        cy.get('#id_password').type('pw')

        cy.get('input[type="submit"]').click()
    })

    it('logging in via post', () => {
        // TODO
    })
})
