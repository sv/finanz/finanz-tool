describe('Request View', () => {
    beforeEach(() => {
        cy.visit('/api-auth/login?next=/new/')

        cy.get('#id_username').type('user')

        cy.get('#id_password').type('pw')

        cy.get('input[type="submit"]').click()

        cy.visit('/new/request')
    })

    const checkIfExists = (pk: string, name: string, requestername: string, target: string) => {
        // Check if request is added temporary
        cy.get(`[cy-request-tr="${pk}"]`).should('exist')
        cy.get(`[cy-request-td-name="${pk}"]`).contains(name)
        cy.get(`[cy-request-td-requestername="${pk}"]`).contains(requestername)
        cy.get(`[cy-request-td-target="${pk}"]`).contains(target)

        // Check if request is added permanent
        cy.reload()
        cy.get(`[cy-request-tr="${pk}"]`).should('exist')
        cy.get(`[cy-request-td-name="${pk}"]`).contains(name)
        cy.get(`[cy-request-td-requestername="${pk}"]`).contains(requestername)
        cy.get(`[cy-request-td-target="${pk}"]`).contains(target)
    }

    it('shows error', () => {
        cy.get('[cy-request-button-create-request]').click()
        cy.get('[cy-requestmodal-button-submit]').click()

        cy.get('[cy-requestmodal-dropdown-requester-help]').contains('Ungültiger pk "-1" - Object existiert nicht.')
        cy.get('[cy-requestmodal-dropdown-requester]').should('exist')
    })

    it('creates request', () => {
        cy.get('[cy-request-button-create-request]').click()

        // Select requester
        cy.get('[cy-requestmodal-dropdown-requester]').click()
        cy.get('[aria-label="Megaparty"]').click()

        // Select budget
        cy.get('[cy-requestmodal-dropdown-budget]').click()
        cy.get('[aria-label="Einkäufe"').click()

        // Add mock data
        cy.get('[cy-requestmodal-inputtext-name]').type('Test')
        cy.get('[cy-requestmodal-ptextarea-reason]').type('Reasoning')
        cy.get('[cy-requestmodal-inputtext-netsingleamount]').type('12.45')
        cy.get('[cy-requestmodal-inputtext-target]').type('234.56')

        cy.get('[cy-requestmodal-fileupload-receipt]').selectFile('cypress/fixtures/test.pdf')

        cy.get('[cy-requestmodal-button-submit]').click()

        // Check if modal is closed
        cy.wait(500)
        cy.get('[cy-requestmodal-dialog]').should('not.exist')

        checkIfExists("1", "Test", "Megaparty", "-234.56€")
    })

    it('edit request', () => {
        cy.get('[cy-request-button-edit="221"]').click()

        // Check previous values
        cy.get('[cy-requestmodal-inputtext-name]').should('have.value', 'Einkauf Hamberger')
        cy.get('[cy-requestmodal-ptextarea-reason]').should('have.value', 'Einkauf Helfergetränke')
        cy.get('[cy-requestmodal-dropdown-requester]').contains('Megaparty')

        // Update values
        cy.get('[cy-requestmodal-inputtext-name]').clear().type('Einkauf Hamberger 2')
        cy.get('[cy-requestmodal-ptextarea-reason]').clear().type('Einkauf Helferessen')
        cy.get('[cy-requestmodal-inputtext-target]').clear().type('255.00')
        cy.get('[cy-requestmodal-inputtext-netsingleamount]').clear().type('22.45')
        cy.get('[cy-requestmodal-dropdown-requester]').click()
        cy.get('[aria-label="Miniparty"]').click()

        // Submit and check if modal is closed
        cy.get('[cy-requestmodal-button-submit]').click()
        cy.wait(500)
        cy.get('[cy-requestmodal-dialog]').should('not.exist')

        checkIfExists("221", "Einkauf Hamberger 2", "Miniparty", "-255.00€")
    })
})
