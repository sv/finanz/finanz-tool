export default {
    core: { builder: '@storybook/builder-vite' },
    stories: [
        '../src/components/**/*.stories.mdx', '../src/components/**/*.stories.@(js|ts)',
    ],
    addons: [
        '@storybook/addon-links',
        '@storybook/addon-essentials',
        '@storybook/addon-interactions',
    ],
    framework: '@storybook/vue3',
}
