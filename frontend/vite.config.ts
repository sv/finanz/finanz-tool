import {
    fileURLToPath, URL, 
} from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import istanbul from 'vite-plugin-istanbul'

// https://vitejs.dev/config/
export default defineConfig({
    build: { sourcemap: 'inline' },
    plugins: [
        vue(), istanbul({
            cypress: true,
            requireEnv: true,
            forceBuildInstrument: true,
        }),
    ],
    resolve: { alias: { '@': fileURLToPath(new URL('./src', import.meta.url)) } },
    server: {
        port: 3000,
        watch: { usePolling: true },
    },
})
