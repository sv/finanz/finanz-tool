[![pipeline status](https://gitlab.lrz.de/sv/finanz-tool/badges/master/pipeline.svg)](https://gitlab.lrz.de/sv/finanz-tool/-/commits/master)
[![coverage report](https://gitlab.lrz.de/sv/finanz-tool/badges/master/coverage.svg)](https://gitlab.lrz.de/sv/finanz-tool/-/commits/master)
[![release version](https://gitlab.lrz.de/sv/finanz-tool/-/badges/release.svg)](https://gitlab.lrz.de/sv/finanz-tool/-/commits/master)

# Start development server

You need a valid installation of docker compose. Follow the instructions on the official [docker website](https://docs.docker.com/compose/install/compose-desktop/)
Then execute the following command in the root directory:
`bash run dev`

# Usage without docker

Individual components can also be started without docker. Relevant calls can be found inside the docker-compose and Dockerfiles.

# Useful Commands

## First Start

`docker compose exec backend python -u manage.py migrate`

`docker compose exec backend python -u manage.py createsuperuser`

## Running Migrations

`bash run migrate`

This command needs a running instance (`bash run dev`)

## Creating new Migrations

`bash run makemigrations`

This command needs a running instance (`bash run dev`)

## Rename app

Current relevant use: `rechnungstool` to `core` after commit <todo>
`docker compose exec backend python -u manage.py renameapp <old> <new>`

# Run frontend e2e tests

`bash run test:frontend`

# Run backend unit tests

`bash run test:backend`

# Contributing

If you want to contribute, there are many different options to do so:

- If you have an LRZ-Account you can easily create a merge request in this project
- Fork this project, apply your changes and send us an email to `asta-finanz@fs.tum.de` with a link and a commit email address and name, we will include your changes on your behalf (of course with you as author)
- Send us an email with your changes as patch to `asta-finanz@fs.tum.de`, using [git send-email](https://git-send-email.io), we will include your changes on your behalf (of course with you as author)
