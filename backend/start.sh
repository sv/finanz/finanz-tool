#!/bin/sh

echo  -n "Wait for postgres server"
sleep 2

echo "Applying pending migrations"
python manage.py migrate

echo "Starting Gunicorn."
exec gunicorn finanztool.config.wsgi:application --bind 0.0.0.0:8000 --workers 3
