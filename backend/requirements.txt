black==24.10.0 # formatter for python
cffi==1.17.1 # required for cryptography ?! needed for wheel build
coverage==7.6.5 # coverage stats for tests
django-admin-sortable2==2.2.3 # sorting in admin fields
Django==4.2.16 # django framework
djangorestframework==3.15.2
drf-jwt==1.19.2
drf-spectacular==0.27.2 # Open-API 3.0 generator
drf-spectacular-sidecar==2024.11.1 # Open-API UI interface
freezegun==1.5.1 # time setting for tests
gunicorn==23.0.0 # production web server
isort==5.13.2 # import formatting tool for python
jellyfish==1.1.0 # levenshtein distance
num2words==0.5.13 # self-explainatory
mozilla-django-oidc==4.0.1 # keycloak OIDC integration
requests==2.32.3
pdfrw==0.4 # pdf manipulation for tests
psycopg2-binary==2.9.10 # postgres client library
pylint==3.3.1 # static code analysis
pylint-django==2.6.1 # pylint plugin for django code-style
PyMuPDF==1.24.13 # pdf manipulation - DO NOT UPGRADE OR THE PRINTING PROCESS WILL FAIL

## To update deps remove frozen versions (==) and run the following command
# pip install --upgrade -r requirements.txt
# for x in $(cat requirements.txt | grep -v '^#' | cut -f 1 -d' '); do pip freeze | grep $x; done
## Then replace the latest version
