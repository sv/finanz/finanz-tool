ARG IMAGE_PREFIX
ARG CI_JOB_TOKEN

FROM --platform=linux/amd64 ${IMAGE_PREFIX}python:3.12-alpine
RUN apk upgrade --no-cache -a
RUN apk add --no-cache qpdf postgresql17-client

WORKDIR /usr/src/app

# Install requirements seperately so they can be cached
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir --extra-index-url https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.lrz.de/api/v4/projects/165010/packages/pypi/simple -r requirements.txt 

COPY finanztool/api /usr/src/app/finanztool/api
COPY finanztool/config /usr/src/app/finanztool/config
COPY finanztool/core /usr/src/app/finanztool/core
COPY finanztool/pdf /usr/src/app/finanztool/pdf
COPY manage.py \
    .coveragerc \
    .pylintrc \
    pyproject.toml \
    start.sh \
    /usr/src/app/

RUN mkdir -p /usr/src/app/data/database ;\
    mkdir -p /usr/src/app/data/media ;\
    chmod +x /usr/src/app/start.sh ;\
    python manage.py collectstatic --noinput
COPY . /usr/src/app/
EXPOSE 8000
HEALTHCHECK CMD wget --no-verbose --tries=1 --spider http://localhost:8000/accounts/login || exit 1
CMD ["sh", "/usr/src/app/start.sh"]
