import base64

from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework import serializers
from rest_framework.fields import CharField
from rest_framework.serializers import ValidationError


class PDFBase64FileField(serializers.Serializer):
    # pylint: disable=abstract-method
    name = CharField()
    data = CharField()

    def validate_name(self, name):
        if not name.endswith(".pdf"):
            raise ValidationError("Filetype must be pdf")

    def validate_data(self, data):
        try:
            return base64.b64decode(data, validate=True)
        except:
            raise ValidationError("Data must be valid base64")

    @staticmethod
    def get_file(validated_data):
        return SimpleUploadedFile(validated_data["name"], validated_data["data"], "application/pdf")
