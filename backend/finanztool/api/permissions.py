from rest_framework import permissions


class IsSuperUser(permissions.BasePermission):
    message = "This is not for you."

    def has_permission(self, request, view):
        return request.user.is_superuser
