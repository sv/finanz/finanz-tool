from rest_framework.routers import DefaultRouter

from finanztool.api.views import (
    BookingViewSet,
    BudgetViewSet,
    MiscViewSet,
    OfferViewSet,
    RequesterViewSet,
    RequestViewSet,
    UserViewSet,
)

router = DefaultRouter()
router.include_root_view = False
router.register(r"budgets", BudgetViewSet, basename="budget")
router.register(r"bookings", BookingViewSet, basename="booking")
router.register(r"misc", MiscViewSet, basename="misc")
router.register(r"offers", OfferViewSet, basename="offer")
router.register(r"requests", RequestViewSet, basename="request")
router.register(r"requesters", RequesterViewSet, basename="requester")
router.register(r"users", UserViewSet, basename="user")
