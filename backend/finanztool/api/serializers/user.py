from django.contrib.auth.models import Group, User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        ref_name = "User"
        fields = [
            "username",
            "first_name",
            "last_name",
            "last_login",
            "date_joined",
            "is_superuser",
        ]


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = [
            "pk",
            "name",
        ]
