from rest_framework import serializers

from finanztool.api.fields import PDFBase64FileField
from finanztool.api.serializers.requester import RequesterSerializer
from finanztool.api.serializers.status import StatusSerializer
from finanztool.core.models import Budget, Offer, Request


## Offer
class OfferSerializerMeta:
    model = Offer
    ref_name = "Offer"
    fields = ["pk", "bidder", "net", "is_desired", "offer_file"]


class OfferResponseSerializer(serializers.ModelSerializer):
    Meta = OfferSerializerMeta


class OfferRequestSerializer(serializers.ModelSerializer):
    Meta = OfferSerializerMeta
    offer_file = PDFBase64FileField()


## Request
class RequestSerializerMeta:
    model = Request
    ref_name = "Request"
    fields = [
        "pk",
        "reason",
        "net_single_amount",
        "receipt",
        "offers",
    ]


class RequestReplySerializer(serializers.ModelSerializer):
    Meta = RequestSerializerMeta
    offers = OfferResponseSerializer(many=True, source="offer_set")


class RequestRequestSerializer(serializers.ModelSerializer):
    Meta = RequestSerializerMeta
    offers = OfferRequestSerializer(many=True, source="offer_set", required=False)
    receipt = PDFBase64FileField(required=False)


## Budget
class BudgetSerializerMeta:
    model = Budget
    ref_name = "Budget"
    fields = [
        "pk",
        "name",
        "parent",
        "requester",
        "target",
        "description",
        "date",
        "request",
    ]
    read_only_fields = ["date"]


class BudgetRequestSerializer(serializers.ModelSerializer):
    Meta = BudgetSerializerMeta
    request = RequestRequestSerializer(required=False)


class BudgetReplySerializer(serializers.ModelSerializer):
    status = StatusSerializer(source="display_status")
    requester = RequesterSerializer(read_only=True)
    request = RequestReplySerializer(read_only=True)

    class Meta:
        model = Budget
        ref_name = "BudgetReply"
        fields = [
            "pk",
            "name",
            "parent",
            "requester",
            "request",
            "target",
            "description",
            "date",
            "status",
        ]
        read_only_fields = ["date"]


class BudgetListSerializer(serializers.ModelSerializer):
    requester = RequesterSerializer(read_only=True)
    status = StatusSerializer(source="display_status")

    class Meta:
        model = Budget
        ref_name = "BudgetList"
        fields = [
            "pk",
            "requester",
            "target",
            "name",
            "date",
            "status",
        ]


class BudgetParentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Budget
        ref_name = "BudgetParentList"
        fields = [
            "pk",
            "name",
        ]
