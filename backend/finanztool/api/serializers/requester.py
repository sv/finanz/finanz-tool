from rest_framework import serializers

from finanztool.core.models import Requester
from finanztool.core.models.fond import BOOKING_TYPE


class RequesterSerializer(serializers.ModelSerializer):
    booking_type = serializers.ChoiceField(choices=BOOKING_TYPE, read_only=True, source="fond.booking_type")

    class Meta:
        model = Requester
        ref_name = "Requester"
        fields = [
            "pk",
            "name",
            "booking_type",
        ]
