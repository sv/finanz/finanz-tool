from rest_framework import serializers


class StatusSerializer(serializers.Serializer):
    progress = serializers.FloatField()
    text = serializers.CharField()
