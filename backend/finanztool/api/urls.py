from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, re_path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from finanztool.api import views
from finanztool.api.router import router

urlpatterns = [
    path("whoami/", views.WhoAmIView.as_view(), name="api-whoami"),
    re_path(r"^year/(?P<year>[0-9]{4})/$", views.year.YearView.as_view(), name="api-year"),
    re_path(r"^year/(?P<year>[0-9]{4})/fond/(?P<pk>[0-9]+)$", views.year.FondView.as_view(), name="api-year"),
    re_path(r"^year/budgetreceivers/(?P<pk>[0-9]+)$", views.year.BudgetReceiverView.as_view(), name="api-year"),
    re_path(r"^year/budgets/(?P<pk>[0-9]+)$", views.year.BudgetView.as_view(), name="api-year"),
    re_path(r"^schema/$", SpectacularAPIView.as_view(), name="schema"),
    re_path(r"^swagger/$", SpectacularSwaggerView.as_view(), name="schema-swagger-ui"),
]

urlpatterns.extend(router.get_urls())
urlpatterns.extend(static(settings.STATIC_URL, document_root=settings.STATIC_ROOT))
