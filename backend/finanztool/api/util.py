from functools import wraps

from django.core.exceptions import ValidationError
from rest_framework.exceptions import ParseError


def filter_dict(in_dict, *keys):
    return {x: in_dict[x] for x in keys if x in in_dict}


def clean_save(obj):
    obj.full_clean()
    obj.save()


def validation_error_to_400(func):
    @wraps(func)
    def ret(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ValidationError as ex:
            raise ParseError(detail=ex.message_dict)

    return ret
