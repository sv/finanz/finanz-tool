from rest_framework.exceptions import APIException


class ConflictError(APIException):
    status_code = 409
    default_detail = "Das Element existiert bereits."
    default_code = "already_existing"
