from finanztool.core.models import (
    AnnualPlan,
    Budget,
    Fond,
    IssuedInvoice,
    Person,
    ReceivedInvoice,
    Request,
    Requester,
)


def get_requester_queryset(request):
    qs = Requester.objects.all()
    if request.user.is_superuser:
        # superuser can see requests for all budgets
        return qs
    if request.user.is_authenticated:
        # only show requests that user is allowed to see
        return qs.filter(user=request.user)
    # unauthenticated can't see anything (this should not happen)
    return qs.none()


def get_fond_queryset(request):
    qs = Fond.objects.all().distinct()
    if request.user.is_superuser:
        return qs
    if request.user.is_authenticated:
        requester_queryset = get_requester_queryset(request)
        return qs.filter(requester__in=requester_queryset).distinct()
    return qs.none()


def get_budget_queryset(request):
    return Budget.objects.filter(requester__in=get_requester_queryset(request)).filter(
        annual_plan=AnnualPlan.get_current()
    )


def get_receivedinvoice_queryset(request):
    return ReceivedInvoice.objects.filter(
        annual_plan=AnnualPlan.get_current(), requester__in=get_requester_queryset(request)
    )


def get_issuedinvoice_queryset(request):
    return IssuedInvoice.objects.filter(
        annual_plan=AnnualPlan.get_current(), requester__in=get_requester_queryset(request)
    )


def get_request_queryset(request):
    return Request.objects.filter(budget__in=get_budget_queryset(request))


def get_person_queryset(request):
    qs = Person.objects.all()
    if request.user.is_superuser:
        # superuser can see requests for all budgets
        return qs
    # unauthenticated or other users can't see anything (this should not happen)
    return qs.none()
