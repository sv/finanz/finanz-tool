from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = "finanztool.api"
    label = "api"
