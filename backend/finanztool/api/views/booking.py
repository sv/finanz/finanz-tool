import datetime

from django.contrib.postgres.search import SearchVector
from django.db.models import Q
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import serializers
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from finanztool.api.serializers import StatusSerializer
from finanztool.api.views.requester import get_requester_queryset
from finanztool.core.models import Booking, Invoice, IssuedInvoice, ReceivedInvoice


class BookingSerializer(serializers.Serializer):
    unique_id = serializers.CharField()
    date = serializers.DateField()
    display_name = serializers.CharField()
    requester = serializers.CharField(source="requester__name")
    link = serializers.CharField()
    status = StatusSerializer(source="display_status")
    booked_total = serializers.DecimalField(10, 2)


class RecentInvoiceGroupedSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    name = serializers.CharField()
    items = BookingSerializer(many=True)


param_year = OpenApiParameter(
    name="search",
    location=OpenApiParameter.QUERY,
    description="Search Value",
    required=True,
    type=OpenApiTypes.STR,
)

param_days = OpenApiParameter(
    name="days",
    location=OpenApiParameter.QUERY,
    description="Number of Days ago until all logs are displayed",
    required=False,
    default="30",
    type=OpenApiTypes.STR,
)


## ViewSets
class BookingViewSet(ViewSet):
    @extend_schema(responses={200: BookingSerializer(many=True)}, parameters=[param_year])
    def list(self, request):
        booking_fields = [f.name for f in Booking._meta.get_fields()]
        bookings = [
            booking.objects.filter(requester__in=get_requester_queryset(request), budget=None)
            .annotate(unique_id=booking.annotate_unique_id())
            .annotate(search=SearchVector(*[f.name for f in booking._meta.get_fields()]) + SearchVector("unique_id"))
            .filter(search__icontains=request.GET["search"])
            .values(*booking_fields, "requester__name")
            .annotate(
                booked_total=booking.annotate_booked_total(),
                unique_id=booking.annotate_unique_id(),
                display_name=booking.annotate_display_name(),
                link=booking.annotate_link(request.user.is_superuser, str(booking.__name__.lower())),
                display_status=booking.annotate_status(),
            )
            for booking in Booking.types()
        ]
        bookings = bookings[0].union(*bookings[1:]).order_by("-date")
        serializer = BookingSerializer(bookings, many=True)
        return Response(serializer.data)

    @extend_schema(responses={200: RecentInvoiceGroupedSerializer(many=True)}, parameters=[param_days])
    @action(detail=False, methods=["get"])
    def recent(self, request):
        invoice_fields = [f.name for f in Invoice._meta.get_fields()]
        invoices = []

        for requester in get_requester_queryset(request):
            items = [
                invoice.objects.filter(requester=requester)
                .filter(
                    date__gte=datetime.datetime.today()
                    - datetime.timedelta(days=int(request.query_params.get("days") or "30"))
                )
                .order_by("date")
                .values(*invoice_fields, "requester__name")
                .annotate(
                    booked_total=invoice.annotate_booked_total(),
                    unique_id=invoice.annotate_unique_id(),
                    display_name=invoice.annotate_display_name(),
                    display_status=invoice.annotate_status(),
                    link=invoice.annotate_link(request.user.is_superuser, str(invoice.__name__.lower())),
                )
                for invoice in [ReceivedInvoice, IssuedInvoice]
            ]
            items = items[0].union(*items[1:])
            if len(items) > 0:
                invoices.append({"pk": requester.pk, "name": requester.name, "items": items})
        serializer = RecentInvoiceGroupedSerializer(invoices, many=True)
        return Response(serializer.data)

    @extend_schema(responses={200: RecentInvoiceGroupedSerializer(many=True)})
    @action(detail=False, methods=["get"])
    def stale(self, request):
        invoice_fields = [f.name for f in Invoice._meta.get_fields()]
        invoices = []

        for requester in get_requester_queryset(request):
            items = [
                invoice.objects.filter(requester=requester)
                .filter(annual_plan__year__gte=datetime.date.today().year - 2)  # Display last two years
                .filter(
                    ~Q(status="paid"),
                    ~Q(status="cancelled"),
                    date__lt=datetime.datetime.today() - datetime.timedelta(days=30),
                )
                .order_by("date")
                .values(*invoice_fields, "requester__name")
                .annotate(
                    booked_total=invoice.annotate_booked_total(),
                    unique_id=invoice.annotate_unique_id(),
                    display_name=invoice.annotate_display_name(),
                    display_status=invoice.annotate_status(),
                    link=invoice.annotate_link(request.user.is_superuser, str(invoice.__name__.lower())),
                )
                for invoice in [ReceivedInvoice, IssuedInvoice]
            ]
            items = items[0].union(*items[1:])
            if len(items) > 0:
                invoices.append({"pk": requester.pk, "name": requester.name, "items": items})
        serializer = RecentInvoiceGroupedSerializer(invoices, many=True)
        return Response(serializer.data)
