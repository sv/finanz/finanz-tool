from rest_framework import viewsets

from finanztool.api.querysets import get_requester_queryset
from finanztool.api.serializers import RequesterSerializer


class RequesterViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = RequesterSerializer

    def get_queryset(self):
        qs = get_requester_queryset(self.request)
        # speed optimization
        return qs.select_related("fond").only("name", "fond__booking_type")
