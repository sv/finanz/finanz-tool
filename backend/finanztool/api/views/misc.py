import datetime
import json
from decimal import Decimal

from django.contrib.admin.models import LogEntry
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import Http404
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import serializers
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from finanztool.api.querysets import (
    get_fond_queryset,
    get_issuedinvoice_queryset,
    get_receivedinvoice_queryset,
    get_request_queryset,
    get_requester_queryset,
)
from finanztool.core.models import (
    Budget,
    BudgetReceiver,
    IssuedInvoice,
    ReceivedInvoice,
    Request,
    SAPBooking,
)
from finanztool.core.models.budget import BUDGET_STATUS
from finanztool.core.models.invoice import INVOICE_STATES


class ComparisonSerializer(serializers.Serializer):
    recent = serializers.IntegerField()
    previous = serializers.IntegerField()


class BudgetUtilizationSerializer(serializers.Serializer):
    name = serializers.CharField()
    labels = serializers.ListField(child=serializers.CharField())
    relative_values = serializers.ListField(child=serializers.DecimalField(10, 2))
    absolute_values = serializers.ListField(child=serializers.DecimalField(10, 2))


class ByStatusSerializer(serializers.Serializer):
    labels = serializers.ListField(child=serializers.CharField())
    recent_values = serializers.ListField(child=serializers.IntegerField())
    stale_values = serializers.ListField(child=serializers.IntegerField())
    count_previous_year = serializers.IntegerField()


class StatsSerializer(serializers.Serializer):
    sap = serializers.DateField()
    request_processing_time = serializers.IntegerField()
    budget_utilization = BudgetUtilizationSerializer(many=True)
    invoices_by_status = ByStatusSerializer()
    requests_by_status = ByStatusSerializer()


class LogSerializer(serializers.Serializer):
    action_time = serializers.DateTimeField()
    user = serializers.CharField(source="user.get_full_name", default="")
    name = serializers.SerializerMethodField(default="")
    color = serializers.CharField(source="content_type.model_class.color", default="#ABB5BE")
    action = serializers.SerializerMethodField()
    link = serializers.CharField(source="get_admin_url")
    class_name = serializers.CharField(source="content_type.name")

    def get_name(self, obj):
        try:
            if isinstance(obj.get_edited_object(), Request):
                return obj.get_edited_object().budget.name
            return getattr(obj.get_edited_object(), "name", "Unbekannt")
        except ObjectDoesNotExist:
            return "Unbekannt"

    def get_action(self, obj):
        if obj.is_addition():
            return "Neu hinzugefügt"
        if obj.is_change():
            if obj.change_message and obj.change_message[0] == "[":
                change_message = json.loads(obj.change_message)
                for message in change_message:
                    if "changed" in message:
                        if len(message["changed"]["fields"]) == 0:
                            return "Geändert"
                        if len(message["changed"]["fields"]) == 1:
                            return "Feld {} geändert".format(message["changed"]["fields"][0])
                        return "Mehrere Felder geändert"
            return "Geändert"
        if obj.is_deletion():
            return "Gelöscht"
        return "Sonstiges"


param_from = OpenApiParameter(
    name="from",
    location=OpenApiParameter.QUERY,
    description="Number of Days ago from which all logs are displayed",
    required=False,
    type=OpenApiTypes.BYTE,
)

param_to = OpenApiParameter(
    name="to",
    location=OpenApiParameter.QUERY,
    description="Number of Days ago until all logs are displayed",
    required=False,
    type=OpenApiTypes.BYTE,
)


def get_relative_deviation(budget_receiver: BudgetReceiver) -> Decimal:
    if budget_receiver.target == 0.0:
        return Decimal("0.0")

    remainder = budget_receiver.get_values().expected - budget_receiver.target

    return remainder / abs(budget_receiver.target)


def get_absolute_deviation(budget_receiver: BudgetReceiver):
    return budget_receiver.get_values().expected - budget_receiver.target


## ViewSets
class MiscViewSet(ViewSet):
    @extend_schema(responses={200: LogSerializer(many=True)}, parameters=[param_from, param_to])
    @action(detail=False, methods=["get"])
    def logs(self, request):
        if not request.user.is_superuser:
            raise Http404()
        logs = LogEntry.objects.all()
        if request.query_params.get("from"):
            logs = logs.filter(
                action_time__gte=datetime.datetime.now()
                - datetime.timedelta(days=int(request.query_params.get("from")))
            )
        if request.query_params.get("to"):
            logs = logs.filter(
                action_time__lte=datetime.datetime.now() - datetime.timedelta(days=int(request.query_params.get("to")))
            )
        serializer = LogSerializer(logs[:50], many=True)
        return Response(serializer.data)

    @extend_schema(responses={200: StatsSerializer()})
    @action(detail=False, methods=["get"])
    def stats(self, request):
        stats = {}

        previous_year = [
            datetime.datetime.now() - datetime.timedelta(days=30 + 365),
            datetime.datetime.now() - datetime.timedelta(days=365),
        ]
        current_month = [datetime.datetime.now() - datetime.timedelta(days=30), datetime.datetime.now()]

        stats["sap"] = (
            SAPBooking.objects.order_by("-buchungsdatum").first().buchungsdatum
            if SAPBooking.objects.count() > 0
            else "-"
        )

        requests = get_request_queryset(request).filter(budget__status="approved").order_by("-budget__date")
        logentries = LogEntry.objects.filter(
            content_type__model=Budget._meta.model_name, change_message__contains="approved"
        )

        days = (0, 0)

        for pk in list(requests[:20].values_list("budget__pk", flat=True)):
            if not requests.filter(budget__pk=pk).exists() or (not logentries.filter(object_id=pk).exists()):
                continue
            request_date = requests.get(budget__pk=pk).budget.date
            close_date = logentries.filter(object_id=pk).order_by("action_time")[0].action_time
            days = ((close_date.date() - request_date).days + days[0], days[1] + 1)

        stats["request_processing_time"] = days[0] / days[1] if days[1] > 0 else 0

        budget_receivers = BudgetReceiver.objects.filter(
            annual_plan__year=datetime.date.today().year, receiver__in=get_requester_queryset(request)
        ).order_by("display_order")

        stats["budget_utilization"] = [
            {
                "name": fond.name,
                "labels": budget_receivers.filter(receiver__fond=fond).values_list("receiver__name", flat=True),
                "relative_values": [
                    (
                        0.0
                        if budget_receiver.target == 0.0
                        else (budget_receiver.get_values().expected - budget_receiver.target)
                        / abs(budget_receiver.target)
                    )
                    for budget_receiver in budget_receivers.filter(receiver__fond=fond)
                ],
                "absolute_values": [
                    budget_receiver.get_values().expected - budget_receiver.target
                    for budget_receiver in budget_receivers.filter(receiver__fond=fond)
                ],
            }
            for fond in get_fond_queryset(request)
        ]

        stats["invoices_by_status"] = {}

        recent_received_invoices = get_receivedinvoice_queryset(request).filter(date__range=current_month)
        recent_issued_invoices = get_issuedinvoice_queryset(request).filter(date__range=current_month)
        stale_received_invoices = get_receivedinvoice_queryset(request).filter(
            ~Q(date__range=current_month), ~Q(status="paid"), ~Q(status="cancelled")
        )
        stale_issued_invoices = get_issuedinvoice_queryset(request).filter(
            ~Q(date__range=current_month), ~Q(status="paid"), ~Q(status="cancelled")
        )

        stats["invoices_by_status"] = {
            "labels": map(lambda label: label[1], INVOICE_STATES),
            "recent_values": [],
            "stale_values": [],
            "count_previous_year": ReceivedInvoice.objects.filter(
                requester__in=get_requester_queryset(request), date__range=previous_year
            ).count()
            + IssuedInvoice.objects.filter(
                requester__in=get_requester_queryset(request), budget__date__range=previous_year
            ).count(),
        }
        for status in INVOICE_STATES:
            stats["invoices_by_status"]["recent_values"].append(
                recent_received_invoices.filter(status=status[0]).count()
                + recent_issued_invoices.filter(status=status[0]).count()
            )
            stats["invoices_by_status"]["stale_values"].append(
                stale_received_invoices.filter(status=status[0]).count()
                + stale_issued_invoices.filter(status=status[0]).count()
            )

        stats["requests_by_status"] = {
            "labels": map(lambda label: label[1], BUDGET_STATUS),
            "recent_values": [],
            "stale_values": [],
            "count_previous_year": Request.objects.filter(
                budget__requester__in=get_requester_queryset(request), budget__date__range=previous_year
            ).count(),
        }

        recent_requests = get_request_queryset(request).filter(budget__date__range=current_month)
        stale_requests = get_request_queryset(request).filter(
            ~Q(budget__date__range=current_month), ~Q(budget__status="closed"), ~Q(budget__status="done")
        )

        for status in BUDGET_STATUS:
            stats["requests_by_status"]["recent_values"].append(
                recent_requests.filter(budget__status=status[0]).count()
            )
            stats["requests_by_status"]["stale_values"].append(stale_requests.filter(budget__status=status[0]).count())

        return Response(StatsSerializer(stats).data)
