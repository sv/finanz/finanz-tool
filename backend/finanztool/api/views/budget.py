import datetime
import io
from socket import gaierror

from django.db import transaction
from django.db.models import Q
from django.shortcuts import get_object_or_404
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import serializers, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from finanztool.api.serializers import (
    BudgetListSerializer,
    BudgetParentListSerializer,
    BudgetReplySerializer,
    BudgetRequestSerializer,
    OfferRequestSerializer,
    OfferResponseSerializer,
    StatusSerializer,
)
from finanztool.api.util import clean_save, filter_dict, validation_error_to_400
from finanztool.api.views.requester import get_requester_queryset
from finanztool.core.models import AnnualPlan, Budget, Offer, Request

param_requestsOnly = OpenApiParameter(
    name="requestsOnly",
    location=OpenApiParameter.QUERY,
    description="Nur Budgets mit Request anzeigen",
    required=True,
    type=OpenApiTypes.BOOL,
)

param_requester = OpenApiParameter(
    name="requester",
    location=OpenApiParameter.QUERY,
    description="Primary Key des Requesters",
    required=True,
    type=OpenApiTypes.INT,
)


def get_budget_queryset(request):
    return (
        Budget.objects.filter(status="requested")
        .filter(Q(request__isnull=True) | Q(request__za3_requested=False))
        .filter(requester__in=get_requester_queryset(request))
        .filter(annual_plan=AnnualPlan.get_current())
    )


def get_budget_request_queryset(request):
    return (
        Budget.objects.filter(Q(request__isnull=False))
        .filter(requester__in=get_requester_queryset(request))
        .filter(annual_plan=AnnualPlan.get_current())
    )


def get_potential_parent_queryset(request):
    return (
        Budget.objects.filter(status__in=["requested", "approved"])
        .filter(request__isnull=True)
        .filter(requester__in=get_requester_queryset(request))
        .filter(annual_plan=AnnualPlan.get_current())
    )


## ViewSets
class BudgetViewSet(ViewSet):
    def get_queryset(self):
        return get_budget_queryset(self.request)

    @extend_schema(responses={200: BudgetListSerializer(many=True)}, parameters=[param_requestsOnly])
    def list(self, request):
        budget_queryset = self.get_queryset()
        if "requestsOnly" in request.GET and request.GET["requestsOnly"] == "true":
            budget_queryset = budget_queryset.filter(request__isnull=False)
        # query speed optimization, could be left out
        budget_queryset = (
            budget_queryset.select_related("requester", "requester__fond")
            .only(
                "name",
                "parent",
                "requester__fond__booking_type",
                "requester__name",
                "target",
                "description",
                "date",
                "request",
            )
            .annotate(display_status=Budget.annotate_status())
        )
        serializer = BudgetListSerializer(budget_queryset, many=True)
        return Response(serializer.data)

    @extend_schema(responses={200: BudgetParentListSerializer(many=True)}, parameters=[param_requester])
    @action(detail=False)
    def potential_parents(self, request):
        budget_queryset = get_potential_parent_queryset(request)
        try:
            pk = int(request.GET.get("requester", default=""))
        except ValueError:
            return Response("infalid format for requester", status=status.HTTP_400_BAD_REQUEST)
        requester = get_object_or_404(get_requester_queryset(request), pk=pk)
        budget_queryset = budget_queryset.filter(requester=requester)
        serializer = BudgetParentListSerializer(budget_queryset, many=True)
        return Response(serializer.data)

    def create_offers(self, request, ser: BudgetRequestSerializer, req: Request):
        if "offer_set" not in ser.validated_data["request"]:
            return
        for o in ser.validated_data["request"]["offer_set"]:
            obj = Offer(**filter_dict(o, "bidder", "net", "is_desired"))
            obj.request = req
            obj.offer_file.save("offer.pdf", io.BytesIO(o["offer_file"]["data"]), save=False)
            clean_save(obj)

    def create_request(self, request, ser: BudgetRequestSerializer, budget: Budget):
        if "request" in ser.validated_data:
            obj_request = Request(**filter_dict(ser.validated_data["request"], "reason", "net_single_amount"))
            obj_request.budget = budget
            if "receipt" in ser.validated_data["request"]:
                obj_request.receipt.save(
                    "receipt.pdf", io.BytesIO(ser.validated_data["request"]["receipt"]["data"]), save=False
                )
            clean_save(obj_request)
            self.create_offers(request, ser, obj_request)
            try:
                obj_request.send_status_email("new")
            except gaierror:
                print("WARN: Could not send status email: could not reach mail-host")

    @extend_schema(responses={200: BudgetReplySerializer()}, request=BudgetRequestSerializer)
    @validation_error_to_400
    def create(self, request):
        ser = BudgetRequestSerializer(data=request.data)
        if not ser.is_valid():
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        requester = get_object_or_404(get_requester_queryset(request), pk=ser.validated_data["requester"].pk)
        obj_budget = Budget(**filter_dict(ser.validated_data, "name", "parent", "target", "description"))
        obj_budget.annual_plan = AnnualPlan.get_current()
        obj_budget.requester = requester
        with transaction.atomic():
            clean_save(obj_budget)
            self.create_request(request, ser, obj_budget)

        return Response(
            BudgetReplySerializer(
                Budget.objects.filter(pk=obj_budget.pk).annotate(display_status=Budget.annotate_status()).first()
            ).data
        )

    @extend_schema(responses={200: BudgetReplySerializer()}, request=BudgetRequestSerializer)
    @validation_error_to_400
    def partial_update(self, request, pk):
        budget_queryset = self.get_queryset()
        budget_queryset = budget_queryset.annotate(display_status=Budget.annotate_status())
        obj = get_object_or_404(budget_queryset, pk=pk)
        ser = BudgetRequestSerializer(data=request.data, partial=True)
        if not ser.is_valid():
            return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)
        with transaction.atomic():
            for field in ("name", "parent", "target", "description"):
                if field in ser.validated_data:
                    setattr(obj, field, ser.validated_data[field])
            if "requester" in ser.validated_data:
                requester = get_object_or_404(get_requester_queryset(request), pk=ser.validated_data["requester"].pk)
                obj.requester = requester
            clean_save(obj)
            if "request" in ser.validated_data:
                if hasattr(obj, "request"):
                    # partial-update nested request
                    for field in ("reason", "net_single_amount"):
                        if field in ser.validated_data["request"]:
                            setattr(obj.request, field, ser.validated_data["request"][field])
                    if "receipt" in ser.validated_data["request"]:
                        obj.request.receipt.save(
                            "receipt.pdf", io.BytesIO(ser.validated_data["request"]["receipt"]["data"]), save=False
                        )
                    clean_save(obj.request)
                    self.create_offers(request, ser, obj.request)
                else:
                    # create nested request
                    self.create_request(request, ser, obj)
        return Response(BudgetReplySerializer(obj).data)

    @extend_schema(responses={200: BudgetReplySerializer()})
    def retrieve(self, request, pk):
        budget_queryset = self.get_queryset()
        obj = get_object_or_404(budget_queryset.annotate(display_status=Budget.annotate_status()), pk=pk)
        serializer = BudgetReplySerializer(obj)
        return Response(serializer.data)


class OfferViewSet(ViewSet):
    @extend_schema(responses={200: OfferResponseSerializer()}, request=OfferRequestSerializer)
    @validation_error_to_400
    def partial_update(self, request, pk):
        budget_queryset = get_budget_queryset(request)
        obj = get_object_or_404(Offer.objects.filter(request__budget__in=budget_queryset), pk=pk)
        serializer = OfferRequestSerializer(data=request.data, partial=True)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        for field in ("pk", "bidder", "net", "is_desired"):
            if field in serializer.validated_data:
                setattr(obj, field, serializer.validated_data[field])
        if "offer_file" in serializer.validated_data:
            obj.offer_file.save("offer.pdf", io.BytesIO(serializer.validated_data["offer_file"]["data"]), save=False)
        clean_save(obj)
        return Response(OfferResponseSerializer(obj).data)


class RecentRequestSerializer(serializers.Serializer):
    date = serializers.DateField(source="budget.date")
    display_name = serializers.CharField(source="budget.name")
    status = StatusSerializer(source="display_status")
    requester = serializers.CharField(source="budget.requester.name")
    link = serializers.CharField()
    booked_total = serializers.DecimalField(10, 2, source="budget.target")


class RecentRequestGroupedSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    name = serializers.CharField()
    items = RecentRequestSerializer(many=True)


param_days = OpenApiParameter(
    name="days",
    location=OpenApiParameter.QUERY,
    description="Number of Days ago until all logs are displayed",
    required=False,
    default="30",
    type=OpenApiTypes.STR,
)


class RequestViewSet(ViewSet):
    def destroy(self, request, pk):
        budget_queryset = get_budget_queryset(request)
        obj = get_object_or_404(Request.objects.filter(budget__in=budget_queryset), pk=pk)
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @extend_schema(responses={200: RecentRequestGroupedSerializer(many=True)}, parameters=[param_days])
    @action(detail=False, methods=["get"])
    def recent(self, request):
        items = Request.objects.filter(
            budget__in=get_budget_queryset(request),
            budget__date__gte=datetime.datetime.now()
            - datetime.timedelta(days=int(request.query_params.get("days") or "30")),
        ).annotate(
            link=Request.annotate_link(request.user.is_superuser),
            display_status=Request.annotate_status(),
        )
        requests = []
        for requester in get_requester_queryset(request):
            items_filtered = items.filter(budget__requester=requester)
            if items_filtered.count() > 0:
                requests.append(
                    {
                        "pk": requester.pk,
                        "name": requester.name,
                        "items": items_filtered,
                    }
                )

        serializer = RecentRequestGroupedSerializer(requests, many=True)
        return Response(serializer.data)

    @extend_schema(responses={200: RecentRequestGroupedSerializer(many=True)})
    @action(detail=False, methods=["get"])
    def stale(self, request):
        items = Request.objects.filter(
            ~Q(budget__status="done"),
            ~Q(budget__status="closed"),
            budget__annual_plan__year__gte=datetime.date.today().year - 2,  # Filter last two years
            budget__date__lte=datetime.datetime.now() - datetime.timedelta(days=60),
            budget__in=get_budget_queryset(request),
        ).annotate(
            link=Request.annotate_link(request.user.is_superuser),
            display_status=Request.annotate_status(),
        )
        requests = []
        for requester in get_requester_queryset(request):
            items_filtered = items.filter(budget__requester=requester)
            if items_filtered.count() > 0:
                requests.append(
                    {
                        "pk": requester.pk,
                        "name": requester.name,
                        "items": items_filtered,
                    }
                )

        serializer = RecentRequestGroupedSerializer(requests, many=True)
        return Response(serializer.data)
