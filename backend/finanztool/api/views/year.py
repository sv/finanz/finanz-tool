# pylint:disable=abstract-method
from django.db.models import F, Value
from django.shortcuts import get_object_or_404
from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import OpenApiParameter, extend_schema
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from finanztool.api.querysets import get_fond_queryset, get_requester_queryset
from finanztool.api.serializers import StatusSerializer
from finanztool.core.models import (
    AnnualPlan,
    Booking,
    Budget,
    BudgetReceiver,
    Fond,
    InternalBooking,
)

param_year = OpenApiParameter(
    name="year",
    location=OpenApiParameter.PATH,
    description="Jahr",
    required=True,
    type=OpenApiTypes.STR,
    pattern="[0-9]{4}",
)


class FondListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fond
        fields = ["pk", "name"]


class YearSerializer(serializers.Serializer):
    fonds = FondListSerializer(many=True)
    year = serializers.CharField()


class YearBudgetReceiverSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    name = serializers.CharField()
    requester = serializers.IntegerField()

    actual = serializers.DecimalField(10, 2, source="get_values.actual")
    expected = serializers.DecimalField(10, 2, source="get_values.expected")
    target = serializers.DecimalField(10, 2)


class FondSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    name = serializers.CharField()
    budgets = YearBudgetReceiverSerializer(many=True)


class YearBookingSerializer(serializers.Serializer):
    unique_id = serializers.CharField()
    date = serializers.DateField()
    display_name = serializers.CharField()
    status = StatusSerializer(source="display_status")
    link = serializers.CharField()
    booked_total = serializers.DecimalField(10, 2)


class YearBudgetSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    name = serializers.CharField()
    description = serializers.CharField()
    status = StatusSerializer(source="display_status")
    date = serializers.DateField()
    actual = serializers.DecimalField(10, 2, source="get_values.actual")
    expected = serializers.DecimalField(10, 2, source="get_values.expected")
    target = serializers.DecimalField(10, 2, source="get_target_or_none")
    has_request = serializers.BooleanField()


class YearBudgetDetailSerializer(serializers.Serializer):
    bookings = YearBookingSerializer(many=True)
    budgets = YearBudgetSerializer(many=True)


class YearBudgetReceiverDetailsSerializer(serializers.Serializer):
    bookings = YearBookingSerializer(many=True)
    budgets = YearBudgetSerializer(many=True)


class BudgetView(APIView):
    @extend_schema(request=None, responses={200: YearBudgetDetailSerializer})
    def get(self, request, pk):
        budget = get_object_or_404(Budget, pk=pk, requester__in=get_requester_queryset(request))

        bookings = [
            booking.objects.filter(budget=budget)
            .annotate(
                booked_total=booking.annotate_booked_total(),
                unique_id=booking.annotate_unique_id(),
                display_name=booking.annotate_display_name(),
                link=booking.annotate_link(request.user.is_superuser, str(booking.__name__.lower())),
                display_status=booking.annotate_status(),
                booking_type=Value(i),
            )
            .select_related("annual_plan")
            .values("unique_id", "link", "date", "display_name", "display_status", "booked_total", "booking_type")
            for i, booking in enumerate(Booking.types())
        ]
        bookings = bookings[0].union(*bookings[1:]).order_by("-date")

        budgets = Budget.objects.filter(
            parent=budget,
        ).annotate(display_status=Budget.annotate_status())
        serializer = YearBudgetReceiverDetailsSerializer(
            {
                "bookings": bookings,
                "budgets": budgets,
            }
        )
        return Response(serializer.data)


class BudgetReceiverView(APIView):
    @extend_schema(request=None, responses={200: YearBudgetReceiverDetailsSerializer})
    def get(self, request, pk):
        budget_receiver = get_object_or_404(BudgetReceiver, pk=pk, receiver__in=get_requester_queryset(request))

        bookings = [
            booking.objects.filter(
                requester=budget_receiver.receiver, annual_plan=budget_receiver.annual_plan, budget=None
            )
            .annotate(
                booked_total=booking.annotate_booked_total(),
                unique_id=booking.annotate_unique_id(),
                display_name=booking.annotate_display_name(),
                link=booking.annotate_link(request.user.is_superuser, str(booking.__name__.lower())),
                display_status=booking.annotate_status(),
                booking_type=Value(i),
            )
            .select_related("annual_plan")
            .values("unique_id", "link", "date", "display_name", "display_status", "booked_total", "booking_type")
            for i, booking in enumerate(Booking.types())
        ]
        bookings.append(
            InternalBooking.objects.filter(sender=budget_receiver.receiver, annual_plan=budget_receiver.annual_plan)
            .annotate(
                booked_total=-InternalBooking.annotate_booked_total(),
                unique_id=InternalBooking.annotate_unique_id(),
                display_name=InternalBooking.annotate_display_name_outgoing(),
                link=InternalBooking.annotate_link(request.user.is_superuser, "internalbooking"),
                display_status=InternalBooking.annotate_status(),
                booking_type=Value(-1),
            )
            .values("unique_id", "link", "date", "display_name", "display_status", "booked_total", "booking_type")
        )
        bookings = bookings[0].union(*bookings[1:]).order_by("-date")

        budgets = Budget.objects.filter(
            parent=None,
            requester=budget_receiver.receiver,
            annual_plan=budget_receiver.annual_plan,
        ).annotate(display_status=Budget.annotate_status())
        serializer = YearBudgetReceiverDetailsSerializer(
            {
                "bookings": bookings,
                "budgets": budgets,
            }
        )
        return Response(serializer.data)


class YearView(APIView):
    @extend_schema(request=None, responses={200: YearSerializer}, parameters=[param_year])
    def get(self, request, year):
        annual_plan = get_object_or_404(AnnualPlan, year=year)
        annual_plan.fonds = get_fond_queryset(request)
        serializer = YearSerializer(annual_plan)
        return Response(serializer.data)


class FondView(APIView):
    @extend_schema(request=None, responses={200: FondSerializer}, parameters=[param_year])
    def get(self, request, year, pk):
        annual_plan = get_object_or_404(AnnualPlan, year=year)
        fond = get_object_or_404(Fond, pk=pk)
        fond.budgets = BudgetReceiver.objects.filter(
            annual_plan=annual_plan, receiver__fond=fond, receiver__in=get_requester_queryset(request)
        ).annotate(name=F("receiver__name"), requester=F("receiver__pk"))

        serializer = FondSerializer(fond)
        return Response(serializer.data)
