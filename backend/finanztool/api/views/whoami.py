from drf_spectacular.utils import extend_schema
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView

from finanztool.api.serializers import UserSerializer
from finanztool.config import settings


# pylint: disable=abstract-method
class WhoAmIResponseSerializer(serializers.Serializer):
    user = UserSerializer()
    debug_branding = serializers.CharField()


class WhoAmIView(APIView):
    @extend_schema(request=None, responses={200: WhoAmIResponseSerializer})
    def get(self, request):
        ser = WhoAmIResponseSerializer(
            {
                "user": request.user,
                "debug_branding": settings.DEBUG_BRANDING if settings.DEBUG else None,
            }
        )
        return Response(ser.data)
