from django.contrib.auth.models import Group, User
from django.http import Http404
from django.shortcuts import get_object_or_404
from drf_spectacular.utils import extend_schema
from rest_framework import serializers
from rest_framework.decorators import action
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from finanztool.api.errors import ConflictError
from finanztool.api.serializers import GroupSerializer, UserSerializer
from finanztool.api.util import clean_save, validation_error_to_400
from finanztool.core.models import UserProfile


class CreateSSOUserSerializer(serializers.Serializer):
    sso_username = serializers.CharField()
    groups = GroupSerializer(many=True, required=False)


class UserViewSet(ViewSet):
    @extend_schema(responses={200: UserSerializer}, request=CreateSSOUserSerializer)
    @action(detail=False, methods=["post"])
    @validation_error_to_400
    def new_sso_user(self, request):
        if not request.user.is_superuser:
            raise Http404()
        ser = CreateSSOUserSerializer(data=request.data)
        if not ser.is_valid():
            raise ParseError(ser.errors)
        username = "sso_" + ser.validated_data["sso_username"]
        if User.objects.filter(username=username).exists():
            raise ConflictError("Nutzer existiert bereits!")
        user = User(username=username)
        user.set_unusable_password()
        for group in ser.validated_data.get("groups", []):
            gp = get_object_or_404(Group.objects, pk=group.pk)
            user.groups.add(gp)
        clean_save(user)
        profile = UserProfile(user=user, sso_username=ser.validated_data["sso_username"])
        clean_save(profile)
        return Response(UserSerializer(user).data)
