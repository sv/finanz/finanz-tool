import subprocess


def tar(path):
    # pylint: disable=consider-using-with
    # We will close this through _WrapClose
    proc = subprocess.Popen(["tar", "-cz", path], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    return _WrapClose(proc.stdout, proc)


def pg_dump(host, user, password, database):
    # pylint: disable=consider-using-with
    # We will close this through _WrapClose
    proc = subprocess.Popen(
        ["pg_dump", "-h", host, "-U", user, "-w", database],
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
        env={
            "PGPASSWORD": password,
        },
    )
    return _WrapClose(proc.stdout, proc)


# Helper for command line functions -- a proxy for a file whose close kills the process (adapted from os standard lib)
class _WrapClose:
    def __init__(self, stream, proc):
        self._stream = stream
        self._proc = proc

    def close(self):
        self._stream.close()
        # kill proc in case the stream was aborted
        self._proc.kill()
        # reap proc so it doesn't stay a zombie
        self._proc.wait()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def __getattr__(self, name):
        return getattr(self._stream, name)

    def __iter__(self):
        return iter(self._stream)
