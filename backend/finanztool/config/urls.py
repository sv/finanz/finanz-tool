import os

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import FileResponse
from django.urls import include, path, re_path
from django.views.generic.base import RedirectView
from django.views.static import serve

from finanztool.config.utils import pg_dump, tar

admin.sites.AdminSite.site_header = "AStA Finanz Tool"
admin.sites.AdminSite.site_title = "AStA Finanz Tool"
admin.sites.AdminSite.index_title = "Finanz Tool"

favicon_view = RedirectView.as_view(url="/static/favicon.ico", permanent=True)


@login_required
def protected_serve(request, path, document_root=None, show_indexes=False):  # pylint: disable=W0621
    # ToDo extend to protect certain files from certain users
    return serve(request, path, document_root, show_indexes)


@user_passes_test(lambda user: user.is_superuser)
def dump_db(_):
    """Serve the database dump to superuserers. They can see all data via the admin panel anyway"""
    if settings.DATABASES["default"]["ENGINE"] == "django.db.backends.sqlite3":
        return FileResponse(
            open(settings.DATABASES["default"]["NAME"], "rb"), as_attachment=True, filename="db.sqlite3"
        )
    if settings.DATABASES["default"]["ENGINE"] == "django.db.backends.postgresql_psycopg2":
        return FileResponse(
            pg_dump(
                settings.DATABASES["default"]["HOST"],
                settings.DATABASES["default"]["USER"],
                settings.DATABASES["default"]["PASSWORD"],
                settings.DATABASES["default"]["NAME"],
            ),
            as_attachment="True",
            filename="db.sql",
        )
    raise RuntimeError("db type not supported")


@user_passes_test(lambda user: user.is_superuser)
def dump_data(_):
    """Serve all stored data to superuserers for backup purposes. They can see all data via the admin panel anyway"""
    return FileResponse(tar(os.path.join(settings.BASE_DIR, "data")), as_attachment=True, filename="finanztool.tar.gz")


urlpatterns = [
    path("", include("finanztool.core.urls")),
    path("api/", include("finanztool.api.urls")),
    path("api-auth/", include("rest_framework.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
    path("oidc/", include("mozilla_django_oidc.urls")),
    # allow exporting db for debugging/developement
    path("dump_db", dump_db),
    # allow exporting all as tar for backup
    path("dump_data", dump_data),
    re_path("media/(?P<path>.*)", protected_serve, {"document_root": settings.MEDIA_ROOT}),
    # path("", include("finanztool.core.urls"), name="home"),
    path("admin/", admin.site.urls),
    path("favicon.ico", favicon_view),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
