import logging
import os
from urllib.parse import urljoin

import requests
from django.contrib.auth import logout
from mozilla_django_oidc.middleware import SessionRefresh

from finanztool.core.models import UserProfile


# This method overrides the build_absolute_uri from django to work properly in connection with a reverse proxy
def fix_build_absolute_uri(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        if "URL" in os.environ:

            def build_absolute_uri(location=None):
                if location is None:
                    location = "//%s" % request.get_full_path()
                ret = urljoin(os.environ["URL"], location)
                return ret

            request.build_absolute_uri = build_absolute_uri
        response = get_response(request)

        return response

    return middleware


LOGGER = logging.getLogger(__name__)


class KeycloakSessionRefresh(SessionRefresh):
    def __init__(self, get_response):
        super().__init__(get_response)
        self.OIDC_OP_TOKEN_ENDPOINT = self.get_settings("OIDC_OP_TOKEN_ENDPOINT")  # pylint: disable=C0103
        self.OIDC_RP_CLIENT_SECRET = self.get_settings("OIDC_RP_CLIENT_SECRET")  # pylint: disable=C0103

    # THIS METHOD IS MODIFIED FROM https://github.com/mozilla/mozilla-django-oidc/blob/main/mozilla_django_oidc/middleware.py
    def process_request(self, request):
        if not self.is_refreshable_url(request) or not UserProfile.objects.filter(user=request.user).exists():
            LOGGER.debug("request is not refreshable")
            return

        if not request.session.get("oidc_refresh_token"):
            logout(request)
            return

        request.session["oidc_login_next"] = request.get_full_path()

        token_payload = {
            "client_id": self.OIDC_RP_CLIENT_ID,
            "client_secret": self.OIDC_RP_CLIENT_SECRET,
            "grant_type": "refresh_token",
            "refresh_token": request.session.pop("oidc_refresh_token"),
        }

        # Calls the token endpoint.
        token_response = requests.post(self.OIDC_OP_TOKEN_ENDPOINT, data=token_payload, timeout=5000)

        try:
            token_response.raise_for_status()
        except requests.exceptions.HTTPError:
            logout(request)
            return

        token_response_data = token_response.json()

        # Retrieves the new refresh token and and stores it
        refresh_token = token_response_data.get("refresh_token")
        request.session["oidc_refresh_token"] = refresh_token
