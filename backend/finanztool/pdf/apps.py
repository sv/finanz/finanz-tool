from django.apps import AppConfig


class PdfConfig(AppConfig):
    name = "finanztool.pdf"
    label = "pdf"
