from django.core.exceptions import ValidationError
from pdfrw import PdfReader


def is_pdf(file):
    try:
        x = PdfReader(file)
        if not (x and x.pages and len(x.pages) > 0):
            raise ValidationError("Diese Datei ist keine .pdf Datei!")
    except:
        raise ValidationError("Diese Datei ist keine .pdf Datei!")


def validate_file_size(file):
    limit = 10485760
    if file.size > limit:
        raise ValidationError("Die maximale Dateigröße beträgt 10MB!")
