import io
import os
import subprocess
import tempfile
from decimal import Decimal

import fitz

from finanztool.core.models.fond import GROSS
from finanztool.pdf.generation.template_values.award_notice import (
    direktauftrag_template_values,
)
from finanztool.pdf.generation.template_values.cashtransfer import (
    cashtransfer_deposit_template_values,
    cashtransfer_withdraw_template_values,
)
from finanztool.pdf.generation.template_values.contract import (
    extern_contract_template_values,
    intern_contract_template_values,
)
from finanztool.pdf.generation.template_values.inventory import (
    inventory_template_values,
)
from finanztool.pdf.generation.template_values.invoice import (
    cashsettlement_expense_template_values,
    cashsettlement_income_template_values,
    changeinvoice_template_values,
    issuedinvoice_template_values,
    receivedinvoice_template_values,
)


def generate_receivedinvoice(receivedinvoice):
    if receivedinvoice.requester.fond.booking_type == GROSS:
        # if the received invoice has a cashsettlement assigned, it is handled as such
        if receivedinvoice.cashsettlement_set.count():
            return _generate_pdf(
                cashsettlement_expense_template_values(receivedinvoice), "Muster_30", receivedinvoice.unique_id()
            )
        return _generate_pdf(receivedinvoice_template_values(receivedinvoice), "Muster_30", receivedinvoice.unique_id())
    if receivedinvoice.cashsettlement_set.count():
        return _generate_pdf(
            cashsettlement_expense_template_values(receivedinvoice), "Muster_32", receivedinvoice.unique_id()
        )
    return _generate_pdf(receivedinvoice_template_values(receivedinvoice), "Muster_32", receivedinvoice.unique_id())


def generate_issuedinvoice(issuedinvoice):
    if issuedinvoice.requester.fond.booking_type == GROSS:
        # if the issued invoice has a cashsettlement assigned, it is handled as such
        if issuedinvoice.cashsettlement_set.count():
            return _generate_pdf(
                cashsettlement_income_template_values(issuedinvoice), "Muster_01", issuedinvoice.unique_id()
            )
        return _generate_pdf(issuedinvoice_template_values(issuedinvoice), "Muster_01", issuedinvoice.unique_id())
    if issuedinvoice.cashsettlement_set.count():
        return _generate_pdf(
            cashsettlement_income_template_values(issuedinvoice), "Muster_09", issuedinvoice.unique_id()
        )
    return _generate_pdf(issuedinvoice_template_values(issuedinvoice), "Muster_09", issuedinvoice.unique_id())


def generate_changeinvoice(newinvoice, oldinvoice, gross_old):
    return _generate_pdf(
        changeinvoice_template_values(newinvoice, oldinvoice, gross_old), "Muster_60", oldinvoice.unique_id()
    )


def generate_inventory_letter(inventory):
    return _generate_pdf(inventory_template_values(inventory), "Anlagenbuchhaltung", inventory.pk)


def generate_cashtransfer(cashtransfer):
    if cashtransfer.calculate_booked_total() > Decimal("0"):
        return _generate_pdf(cashtransfer_deposit_template_values(cashtransfer), "Muster_01", cashtransfer.unique_id())
    return _generate_pdf(cashtransfer_withdraw_template_values(cashtransfer), "Muster_30", cashtransfer.unique_id())


def generate_transponder_contract(person, request):
    if person.external_transponder_institution:
        return _generate_pdf(extern_contract_template_values(person, request), "Transpondervertrag_extern", person.pk)
    return _generate_pdf(intern_contract_template_values(person, request), "Transpondervertrag_intern", person.pk)


def generate_direktauftrag(request):
    return _generate_pdf(direktauftrag_template_values(request), "Vergabevermerk_Direktauftrag", request.pk)


def _generate_pdf(template_values, template_name, unique_id):
    template_name = "finanztool/pdf/generation/templates/" + template_name

    doc = fitz.Document(template_name + ".pdf")
    for page in doc:
        widget: fitz.Widget = page.first_widget
        while widget:
            if widget.field_name in template_values:
                # This should work for almost all field types
                widget.field_value = template_values[widget.field_name]
                widget.field_name = "{}_{}".format(widget.field_name, unique_id)
                widget.update()
            widget = widget.next
    doc.set_metadata(
        {
            "title": str(unique_id),
            "author": "Studentische Vertretung - Finanztool",
            "subject": "PDF-Formular",
            "creator": "Studentische Vertretung - Finanztool",
        }
    )
    return io.BytesIO(doc.write())


def _render_pdf(doc: fitz.Document) -> str:
    """Re-render the given pdf document using qpdf to a temp file and return it"""
    try:
        in_path = tempfile.mkstemp()[1]
        out_path = tempfile.mkstemp()[1]
        doc.save(in_path)
        subprocess.run(["qpdf", "--flatten-annotations=screen", in_path, out_path], check=True)
        return out_path
    finally:
        os.remove(in_path)


def fleischwolf(doc: fitz.Document) -> fitz.Document:
    """Re-render the given pdf document using qpdf and parse it again"""
    try:
        out_path = _render_pdf(doc)
        return fitz.Document(filename=out_path, filetype="application/pdf")
    finally:
        os.remove(out_path)


def pdf_to_bytes(doc: fitz.Document) -> bytes:
    """Re-render the given pdf document using qpdf and return it as bytes"""
    try:
        out_path = _render_pdf(doc)
        with open(out_path, "rb") as f:
            return f.read()
    finally:
        os.remove(out_path)


def stamp_copy(doc: fitz.Document):
    for page in doc.pages():
        _add_stamp(page, "KOPIE")


def stamp_electronic(doc: fitz.Document):
    for page in doc.pages():
        _add_stamp(page, "Elektronische\nRechnung", fontsize=12, height=35, width=100)


def stamp_offer_number(doc: fitz.Document, number: int):
    if number < 0 or number > 9:
        raise Exception("Can only print numbers from 0 to 9")
    for page in doc.pages():
        _add_stamp(page, "Angebot {}".format(number))


def _add_stamp(page: fitz.Page, text, fontsize=20, width=150, height=30):
    center = fitz.Point(page.bound().width * 3 / 7, 40.0)
    textbox = fitz.Rect(center.x - width / 2, center.y - height / 2, center.x + width / 2, center.y + height / 2)
    vpadding = 3
    box = fitz.Rect(
        center.x - width / 2, center.y - height / 2 - vpadding, center.x + width / 2, center.y + height / 2 + vpadding
    )
    morph = [center, fitz.Matrix(5.0)]
    color = fitz.utils.getColor("black")
    shape = page.new_shape()
    shape.insert_textbox(textbox, text, fontsize=fontsize, fill=color, morph=morph, align=fitz.TEXT_ALIGN_CENTER)
    shape.draw_rect(box)
    shape.finish(width=3, morph=morph, color=color, fill_opacity=0, stroke_opacity=0.6)
    shape.commit()
