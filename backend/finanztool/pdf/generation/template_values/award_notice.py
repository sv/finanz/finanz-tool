def direktauftrag_template_values(request):
    template_values = {
        "DetailedReason": request.reason,
        "OfficialInCharge": "",
        "OrganizationalUnit_1": "Studentische Vertretung - " + request.budget.requester.name,
        "OrganizationalUnit": "Studentische Vertretung - " + request.budget.requester.name,
        "Reason": "siehe Anlage",
        "ServiceName": request.budget.name,
    }

    for index in range(0, 4):
        template_values["BidderPrice_{}".format(index + 1)] = "xxxxx"

    for index, offer in enumerate(request.offer_set.all(), start=1):
        if index > 4:
            break
        template_values["Bidder_{}".format(index)] = offer.bidder
        template_values["BidderNumber_{}".format(index)] = str(index)
        template_values["BidderPrice_{}".format(index)] = "€ {}".format(offer.net)
        if offer.is_desired:
            template_values["FinalBidder"] = str(index)

    return template_values
