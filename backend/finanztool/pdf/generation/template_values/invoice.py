from datetime import date, timedelta
from decimal import Decimal

from django.core.exceptions import ValidationError

from finanztool.core.models.fond import GROSS, NET


def _invoice_template_values(invoice):
    return {
        # (internalfieldname): fieldvalue #fieldnumber
        "BookingOffice_Budget_0": invoice.requester.cost_center,  # 01
        "Budgetyear": invoice.annual_plan.year,  # has no explicit number
        "DescriptionBookingOffice_0": "Finanzbuchhaltung",
        "Fonds_0": invoice.requester.fond.number,  # 02
        "GeneralLedger_0": invoice.general_ledger_account.number,  # 01
        "InWords": "**" + invoice.total_in_words + "**" if invoice.total_in_words else "",  # has no explicit number
        "IssueOffice_0": "Studentische Vertretung",
        "IssueOffice_1": "TUM",
        "PlaceDate": "München, {0:%d.%m.%Y}".format(date.today()),  # has no explicit number
    }


def changeinvoice_template_values(newinvoice, oldinvoice, gross_old):
    template_values = {
        "BookingNumber": oldinvoice.bkz,
        "BookingOffice_0": "15 12/111 40-1",
        "IssueOffice_2": "Aktenzeichen: " + newinvoice.unique_id(),
        "IssuingOffice_Nr": "151201-1",
        "NameIssuerShort_0": newinvoice.signer,
        "Name": oldinvoice.recipient.name,
        "Total_New": "€ {}".format(gross_old).replace(".", ","),
    }
    template_values = {**template_values, **_invoice_template_values(oldinvoice)}
    if newinvoice.general_ledger_account != oldinvoice.general_ledger_account:
        template_values["GeneralLedger_0_New"] = newinvoice.general_ledger_account
    if newinvoice.recipient != oldinvoice.recipient:
        template_values["CountryKey_New"] = newinvoice.recipient.country_code
        template_values["Name_New"] = newinvoice.recipient.name
        template_values["PostPlace_New"] = newinvoice.recipient.post_place or "99999 Unbekannt"
        template_values["StreetNr_New"] = newinvoice.recipient.street or "Unbekannt"
    if newinvoice.monition != oldinvoice.monition:
        template_values["Monition_New"] = newinvoice.monition
        template_values["Reason_Change"] = "Änderung des Mahnschlüssels"
    if newinvoice.calculate_gross() != gross_old:
        template_values["Total"] = "€ {}".format(newinvoice.calculate_gross()).replace(".", ",")
        if newinvoice.is_cancelled():
            template_values["Reason_Change"] = "Storno der Rechnung"
    if newinvoice.bkz != oldinvoice.bkz:
        template_values["BookingNumber_New"] = newinvoice.bkz

    #'MonitionKey': "0",
    #'PaymentDisplay': "11",
    return template_values


def receivedinvoice_template_values(receivedinvoice):
    template_values = {
        "BIC": receivedinvoice.vendor.bic,  # 12
        "BookingOffice_0": "15 12/547 40-5",  # 01
        "CountryKey": receivedinvoice.vendor.country_code,  # 114
        "CreditInstitute": receivedinvoice.vendor.bankname,  # 11
        "IBAN": receivedinvoice.vendor.iban,  # 13
        "IssueOffice_2": "Aktenzeichen: " + receivedinvoice.unique_id(),  # 45
        "MannerOfPayment": "2",
        "Name": receivedinvoice.vendor.name,  # 07
        "OrderAmount_0": "€ " + str(receivedinvoice.calculate_gross()).replace(".", ","),  # 05
        "PostPlace": receivedinvoice.vendor.post_place or "99999 Unbekannt",  # 09
        "Reasoning": receivedinvoice.reason,  # this field has no explicit number
        "StreetNr": receivedinvoice.vendor.street or "Unbekannt",  # 08
        # The usage fields' value should be splitted into three parts with 27 characters according to the EDVBK
        "Usage_0": receivedinvoice.reference[:27],
        "Usage_1": receivedinvoice.reference[27 : 2 * 27],
        "Usage_2": receivedinvoice.reference[2 * 27 : 3 * 27],
    }
    template_values = {**template_values, **_invoice_template_values(receivedinvoice)}
    if receivedinvoice.requester.fond.booking_type == NET:
        for i in range(0, 5):
            template_values["OrderAmount_{}".format(i)] = "xxxxx"
        template_values["Subtotal"] = "€ {}".format(receivedinvoice.calculate_gross()).replace(".", ",")

        # add gross values with tax values (all unused fields are devalued in the .json files)
        sums_per_tax = {}
        for amount in receivedinvoice.receivedamount_set.all():
            k = (amount.tax, amount.tax_string_short())
            sums_per_tax[k] = (
                sums_per_tax.get(k, (Decimal("0.00"), None))[0] + amount.net,
                sums_per_tax.get(k, (None, Decimal("0.00")))[1] + amount.calc_tax_amount(),
            )
        if len(sums_per_tax) > 3 or (len(sums_per_tax) == 3 and sorted(sums_per_tax.keys())[0][0] != Decimal("1.00")):
            raise Exception("Form only supports 2 different tax amounts plus zero percent tax amount")
        # if it is a cashsettlement it is filled afterwards
        if not receivedinvoice.cashsettlement_set.count():
            index = 0
            # add amounts to form sorted by descending tax
            for (_, tax_string), (amount_net, amount_tax) in sorted(sums_per_tax.items(), reverse=True):
                template_values["BookingOffice_Budget_{}".format(index)] = "1302400000"
                template_values["BookingOffice_{}".format(index)] = "15 12/547 40-5"
                template_values["GeneralLedger_{}".format(index)] = receivedinvoice.general_ledger_account.number
                template_values["OrderAmount_{}".format(index)] = "€ {}".format(amount_net).replace(".", ",")
                template_values["OrderAmount_{}".format(index + 1)] = "€ {}".format(amount_tax).replace(".", ",")
                template_values["HUELNR_{}".format(index)] = "Netto ({})".format(tax_string)
                if len(sums_per_tax) == 3 and tax_string == "0%":
                    template_values["HUELNR_{}".format(index)] = "(0%) Vorst."
                template_values["HUELNR_{}".format(index + 1)] = "{} Vorsteu.".format(tax_string)
                template_values["NameIssuerShort_{}".format(index)] = receivedinvoice.signer
                index += 2
    else:
        taxstrs = {amount.tax: amount.tax_string_short() for amount in receivedinvoice.receivedamount_set.all()}
        template_values["ValueAddedTax"] = ", ".join(v for _, v in sorted(taxstrs.items()))
        template_values["NameIssuerShort_0"] = receivedinvoice.signer

    return template_values


def issuedinvoice_template_values(issuedinvoice):
    template_values = {
        "BookingNumber": issuedinvoice.bkz,  # 03
        "BookingOffice_0": "15 12/111 40-1",  # 01
        "CountryKey": issuedinvoice.recipient.country_code,  # 114
        "FileReferenceNumber": issuedinvoice.unique_id(),
        "Fonds_0": issuedinvoice.requester.fond.number,  # 02
        "MaturityDate": "{0:%d.%m.%Y}".format(issuedinvoice.booking_date + timedelta(days=30)),  # 15
        "Name": issuedinvoice.recipient.name,  # 07
        "PostPlace": issuedinvoice.recipient.post_place or "99999 Unbekannt",  # 09
        "Salutation": issuedinvoice.recipient.salutation,  # 06
        "NameIssuerShort_5": issuedinvoice.signer,
        "Monition": issuedinvoice.monition,
        "MonitionKey": "0",
        "PaymentDisplay": "11",
        "StreetNr": issuedinvoice.recipient.street or "Unbekannt",  # 08
        "Reason_0": issuedinvoice.reason[:27],
        "Reason_1": issuedinvoice.reason[27 : 2 * 27],
        "Reason_2": issuedinvoice.reason[2 * 27 : 3 * 27],
        "Total": "€ {}".format(issuedinvoice.calculate_gross()).replace(".", ","),
    }
    template_values = {**template_values, **_invoice_template_values(issuedinvoice)}
    if issuedinvoice.requester.fond.booking_type == NET:
        for index in range(0, 5):
            template_values["SubAmount_{}".format(index)] = "xxxxx"

        # add net values with tax values (all unused fields are devalued in the .json files)
        sums_per_tax = {}
        for amount in issuedinvoice.issuedamount_set.all():
            k = (amount.tax, amount.tax_string_short())
            sums_per_tax[k] = sums_per_tax.get(k, Decimal("0.00")) + amount.net
        if len(sums_per_tax) > 5:
            raise Exception("Form only supports 5 different tax amounts")
        index = 0
        # add amounts to form sorted by descending tax
        taxstr = []
        for (_, tax_string), amount_net in sorted(sums_per_tax.items(), reverse=True):
            template_values["Label_{}".format(4 * index)] = "Nettobetrag, {} USt".format(tax_string)
            template_values["BookingOffice_{}".format(index)] = "15 12/111 40-1"
            template_values["BookingOffice_Budget_{}".format(index)] = issuedinvoice.requester.cost_center
            template_values["Fonds_{}".format(index)] = issuedinvoice.requester.fond.number
            template_values["GeneralLedger_{}".format(index)] = issuedinvoice.general_ledger_account.number
            template_values["SubAmount_{}".format(index)] = "€ {}".format(amount_net).replace(".", ",")
            taxstr.append(tax_string)
            index += 1

        # list all taxes comma separated
        template_values["MwSt"] = ", ".join(taxstr)
        template_values["MwStAmount"] = "€ {}".format(issuedinvoice.calculate_tax()).replace(".", ",")
        template_values["Subtotal"] = "€ {}".format(issuedinvoice.calculate_net()).replace(".", ",")
        template_values["Total"] = "€ {}".format(issuedinvoice.calculate_gross()).replace(".", ",")

        if issuedinvoice.booking_date is not None:
            # This should not be a problem because booking date is enforced at another place
            template_values["MaturityDate"] = "{0:%d.%m.%Y}".format(issuedinvoice.booking_date + timedelta(days=30))
    else:
        template_values["NameIssuerShort_0"] = issuedinvoice.signer

    return template_values


_CASH_INCOME_EXPENSE_OVERRIDES = {
    "DescriptionBookingOffice_0": "Zahlstelle der TUM - Barzahlung",
    "IssuingOffice_Nr": "1512011",
    "IBAN": "",
    "BIC": "",
    "CreditInstitute": "",
    "MannerOfPayment": "",
}


def cashsettlement_income_template_values(issuedinvoice):
    template_values = issuedinvoice_template_values(issuedinvoice)
    template_values.update(_CASH_INCOME_EXPENSE_OVERRIDES)
    template_values.update(
        {
            "Reason_1": issuedinvoice.cashsettlement_set.first().name + " vom",
            "Reason_2": "{0:%d.%m.%Y}".format(issuedinvoice.cashsettlement_set.first().beginning_date),
            "Monition": "",
            "MonitionKey": "",
            "PaymentDisplay": "",
            "BookingNumber": "",
            "MaturityDate": "",
        }
    )

    # add net values with tax values (all unused fields are devalued in the .json files)
    sums_per_tax = {}
    for amount in issuedinvoice.issuedamount_set.all():
        k = (amount.tax, amount.tax_string_short())
        sums_per_tax[k] = sums_per_tax.get(k, Decimal("0.00")) + amount.gross
    if len(sums_per_tax) > 5:
        raise Exception("Form only supports 5 different tax amounts")
    index = 0
    # add amounts to form sorted by descending tax
    taxstr = []
    for (_, tax_string), amount_gross in sorted(sums_per_tax.items(), reverse=True):
        ledger_account = (
            "543530"
            if tax_string == "19%"
            else "543570" if tax_string == "7%" else "60990" if tax_string == "0%" else "543000"
        )
        template_values["Label_{}".format(4 * index)] = "Bruttobetrag, {} USt".format(tax_string)
        template_values["GeneralLedger_{}".format(index)] = ledger_account
        template_values["SubAmount_{}".format(index)] = "€ {}".format(amount_gross).replace(".", ",")
        taxstr.append(tax_string)
        index += 1

    if issuedinvoice.requester.fond.booking_type == GROSS:
        template_values["Reason_0"] = "Einnahmen AStA"
    else:
        template_values["Reason_0"] = "Einnahmen Veranst."

    return template_values


def cashsettlement_expense_template_values(receivedinvoice):
    template_values = receivedinvoice_template_values(receivedinvoice)
    template_values.update(_CASH_INCOME_EXPENSE_OVERRIDES)
    template_values.update(
        {
            "Usage_1": receivedinvoice.cashsettlement_set.first().name + " am",
            "Usage_2": "{0:%d.%m.%Y}".format(receivedinvoice.cashsettlement_set.first().beginning_date),
            "Reasoning": "",
        }
    )

    # add gross values with tax values (all unused fields are devalued in the .json files)
    sums_per_tax = {}
    for amount in receivedinvoice.receivedamount_set.all():
        k = (amount.tax, amount.tax_code(), amount.tax_string_short())
        sums_per_tax[k] = sums_per_tax.get(k, Decimal("0.00")) + amount.gross
    if len(sums_per_tax) > 5:
        raise Exception("Form only supports 5 different tax amounts")

    # gross values are already filled in by generic invoice template
    if receivedinvoice.requester.fond.booking_type == NET:
        index = 0
        # add amounts to form sorted by descending tax
        for (_, tax_c, tax_str), amount in sorted(sums_per_tax.items(), reverse=True):
            template_values["BookingOffice_Budget_{}".format(index)] = "1302400000"
            template_values["BookingOffice_{}".format(index)] = "15 12/547 40-5 " + tax_c
            template_values["GeneralLedger_{}".format(index)] = receivedinvoice.general_ledger_account.number
            template_values["OrderAmount_{}".format(index)] = "€ {}".format(amount).replace(".", ",")
            template_values["HUELNR_{}".format(index)] = "Brutto ({})".format(tax_str)
            template_values["NameIssuerShort_{}".format(index)] = receivedinvoice.signer
            index += 1

    if receivedinvoice.requester.fond.number == "6140006" or receivedinvoice.requester.fond.number == "2013769":
        template_values["Usage_0"] = "Ausgaben AStA"
    else:
        template_values["Usage_0"] = "Ausgaben Veranst."
    return template_values
