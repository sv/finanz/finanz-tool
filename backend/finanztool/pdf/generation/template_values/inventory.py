from datetime import date


def inventory_template_values(inventory):
    return {
        "PlaceDate": "München, den {0:%d.%m.%Y}".format(date.today()),
        "Name": inventory.invoice.name,
        "Contact": "{}, {}".format(inventory.contact, inventory.contact_mail),
        "RoomNumber": inventory.location,
        "DFGKey": inventory.dfg_key,
        "Treasurer": inventory.invoice.signer,
    }
