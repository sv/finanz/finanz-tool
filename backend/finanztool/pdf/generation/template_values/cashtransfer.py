from datetime import date, timedelta

from finanztool.core.models.fond import GROSS


def _is_change(cashtransfer):
    return cashtransfer.requester.fond.number == "-"  # TODO: update fond number


def _cashtransfer_template_values(cashtransfer):
    return {
        # (internalfieldname): fieldvalue #fieldnumber
        "IssueOffice_0": "Studentische Vertretung",
        "IssueOffice_1": "TUM",
        "BookingOffice_0": "75 29/400 20-3" if _is_change(cashtransfer) else "15 12/547 40-5",  # 01
        "BookingOffice_Budget_0": cashtransfer.requester.cost_center,  # 01
        "Budgetyear": cashtransfer.annual_plan.year,
        "CountryKey": "DE",  # 114
        "DescriptionBookingOffice_0": "Zahlstelle der TUM - Barzahlung",
        "Fonds_0": cashtransfer.requester.fond.number,
        "GeneralLedger_0": "286011" if _is_change(cashtransfer) else "689300",
        "InWords": "**" + cashtransfer.total_in_words + "**" if cashtransfer.total_in_words else "",
        "IssuingOffice_Nr": "" if _is_change(cashtransfer) else "1512011",
        "Name": "Studentische Vertretung - TUM",  # 07
        "NameIssuerShort_0": cashtransfer.signer,
        "PlaceDate": "München, {0:%d.%m.%Y}".format(date.today()),
        "PostPlace": "80333 München",  # 09
        "StreetNr": "Arcisstraße 21",  # 08
    }


def cashtransfer_deposit_template_values(cashtransfer):
    template_values = {
        "FileReferenceNumber": cashtransfer.unique_id(),
        "GeneralLedger_0": "286011" if _is_change(cashtransfer) else "689300",
        "Monition": "",  # 16
        "MonitionKey": "",  # 17
        "PaymentDisplay": "",  # 18
        "Reason_0": "Rückzahlung Wechselgeld" if _is_change(cashtransfer) else "RZ Abschlag",
        "Reason_1": cashtransfer.cashsettlement.name[:24] + " v.",
        "Reason_2": cashtransfer.transfer_date[:27],
        "Total": "€ {}".format(cashtransfer.calculate_booked_total()).replace(".", ","),
    }
    return {**template_values, **_cashtransfer_template_values(cashtransfer)}


def cashtransfer_withdraw_template_values(cashtransfer):
    template_values = {
        "IssueOffice_2": "Aktenzeichen: " + cashtransfer.unique_id(),
        "OrderAmount_0": "€ {}".format(-cashtransfer.calculate_booked_total()).replace(".", ","),
        "Usage_0": "Barvorschuss Wechselgeld" if _is_change(cashtransfer) else "Abschlag bar",
        "Usage_1": cashtransfer.cashsettlement.name + " am",
        "Usage_2": cashtransfer.transfer_date[:27],
    }
    return {**template_values, **_cashtransfer_template_values(cashtransfer)}
