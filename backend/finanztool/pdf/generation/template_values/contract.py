from datetime import date, datetime

from finanztool.core.models.locking_authorization import (
    LockingAuthorization,
    LockingAuthorizationGroup,
)


def _contract_valid_until_date():
    """
    Generates the expiration date of the contract. If it is october or later it is the 30.11. of next year.
    Otherwise its the 30.11. of the same year.
    """
    if datetime.today().date().month < 10:
        return datetime(datetime.now().date().year, 11, 30).date().strftime("%d.%m.%Y")
    else:
        return datetime(datetime.now().date().year + 1, 11, 30).date().strftime("%d.%m.%Y")


def _contract_template_values(person, request):
    template_values = {
        "ContractValidUntil": _contract_valid_until_date(),
        "Role": person.role,
        "TransponderNr": person.get_transponder().transponder_id,
    }

    # set defaults and override them later
    for index in range(0, 20):
        template_values["TransponderAuth_{}".format(index)] = ""
        template_values["TransponderAuthYes_{}".format(index)] = False
        template_values["TransponderAuthNo_{}".format(index)] = True

    for index in range(0, 4):
        template_values["Date_{}".format(index)] = date.today().strftime("%d.%m.%Y")
        if index < 2:
            template_values["Carrier_{}".format(index)] = person.name
            template_values["Treasurer_{}".format(index)] = request.user.get_full_name()

    transponder_auth_names = []
    for group in LockingAuthorizationGroup.objects.all():
        group_auths = map(lambda x: x.id, person.get_transponder().locking_authorizations.all())
        has_whole_group = (
            LockingAuthorization.objects.filter(internal_group__id=group.id).count() != 0
            and LockingAuthorization.objects.filter(internal_group__id=group.id).exclude(id__in=group_auths).count()
            == 0
        )
        if has_whole_group:
            auths = []
            for locking_authorization in LockingAuthorization.objects.filter(internal_group__id=group.id):
                if person.get_transponder().locking_authorizations.filter(id=locking_authorization.id).first():
                    auths.append(locking_authorization.lock_id)
            transponder_auth_names.append("{} ({})".format(group.name, ", ".join(auths)))
        else:
            for locking_authorization in LockingAuthorization.objects.filter(internal_group__id=group.id):
                if person.get_transponder().locking_authorizations.filter(id=locking_authorization.id).first():
                    transponder_auth_names.append(
                        "{} - {}".format(locking_authorization.lock_id, locking_authorization.name)
                    )
    for locking_authorization in LockingAuthorization.objects.filter(internal_group=None):
        if person.get_transponder().locking_authorizations.filter(id=locking_authorization.id).first():
            transponder_auth_names.append("{} - {}".format(locking_authorization.id, locking_authorization.name))

    for index, name in enumerate(transponder_auth_names, start=1):
        template_values["TransponderAuth_{}".format(index)] = name
        template_values["TransponderAuthYes_{}".format(index)] = True
        template_values["TransponderAuthNo_{}".format(index)] = False

    return template_values


def extern_contract_template_values(person, request):
    template_values = {"Institution": person.external_transponder_institution}
    template_values = {**template_values, **_contract_template_values(person, request)}

    return template_values


def intern_contract_template_values(person, request):
    template_values = _contract_template_values(person, request)

    # set defaults
    for index in range(0, 15):
        template_values["KeyAuth_{}".format(index)] = ""
        template_values["KeyAuthYes_{}".format(index)] = False
        template_values["KeyAuthNo_{}".format(index)] = True

    index = 1
    for key in person.keys.all():
        template_values["KeyAuth_{}".format(index)] = key.group.description
        template_values["KeyNr_{}".format(index)] = key.number
        template_values["KeyGroup_{}".format(index)] = key.group.name
        template_values["KeyAuthYes_{}".format(index)] = True
        template_values["KeyAuthNo_{}".format(index)] = False
        index += 1

    return template_values
