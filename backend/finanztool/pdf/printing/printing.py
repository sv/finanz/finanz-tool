import fitz

from finanztool.core.models import Invoice, IssuedInvoice
from finanztool.pdf.generation.pdf_generation import (
    fleischwolf,
    pdf_to_bytes,
    stamp_copy,
    stamp_electronic,
)
from finanztool.pdf.printing.lpr import print_file

PROFILE_INVOICE_COPY = "invoice_copy"
PROFILE_INVOICE_ORIG_POSTAL = "invoice_orig_postal"
PROFILE_INVOICE_ORIG_DIGITAL = "invoice_orig_digital"


def print_invoice(invoice: Invoice):
    if isinstance(invoice, IssuedInvoice):
        # issued invoices are always electronic/copies
        electronic = True
    else:
        # received invoices contain a flag. Skip invoice with an error flag is not set
        if invoice.electronic_invoice is None:
            raise Exception('invoice does not contain "electronic_invoice" flag')
        electronic = invoice.electronic_invoice
    with invoice.form_file.open("rb") as form_file_reader:
        doc = fitz.Document(stream=form_file_reader.read(), filetype="application/pdf")
    if not electronic:
        doc_copy: fitz.Document = fleischwolf(doc)
        if hasattr(invoice, "inventory_set"):
            for inventory in invoice.inventory_set.all():
                with inventory.form_file.open("rb") as inventory_file_reader:
                    doc_copy.insert_pdf(
                        fleischwolf(fitz.Document(stream=inventory_file_reader.read(), filetype="application/pdf"))
                    )
        print_file(pdf_to_bytes(doc_copy), PROFILE_INVOICE_ORIG_POSTAL, "{} (Formular)".format(invoice.unique_id()))
    with invoice.invoice_file.open("rb") as invoice_file_reader:
        doc_invoice = fleischwolf(fitz.Document(stream=invoice_file_reader.read(), filetype="application/pdf"))
        if electronic:
            stamp_electronic(doc_invoice)
        else:
            stamp_copy(doc_invoice)
        doc.insert_pdf(doc_invoice)
    if hasattr(invoice, "inventory_set"):
        for inventory in invoice.inventory_set.all():
            with inventory.generated_file.open("rb") as inventory_file_reader:
                doc.insert_pdf(
                    fleischwolf(fitz.Document(stream=inventory_file_reader.read(), filetype="application/pdf"))
                )
    if electronic:
        print_file(pdf_to_bytes(doc), PROFILE_INVOICE_ORIG_DIGITAL, "{} (Original)".format(invoice.unique_id()))
    print_file(pdf_to_bytes(doc), PROFILE_INVOICE_COPY, title="{} (Kopie)".format(invoice.unique_id()))
