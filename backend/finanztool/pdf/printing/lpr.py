import requests


def _print_file(file: bytes, profile: str, title: str):
    try:
        resp = requests.post(
            "http://cups:8123/print",
            data=file,
            params=dict(profile=profile, title=title),
            headers={"Content-Type": "application/pdf"},
        )
    except requests.ConnectionError:
        raise Exception("could not connect to printer server")
    resp_data = resp.json()
    if resp_data == {} and resp.status_code == 200:
        return
    if resp_data is None:
        resp.raise_for_status()
    if "error" in resp_data:
        raise Exception(f"error printing file: {resp_data['error']}")
    raise Exception("unkown error while printing file")


def print_file(file: bytes, profile: str, title: str):
    _print_file(file, profile, title)
