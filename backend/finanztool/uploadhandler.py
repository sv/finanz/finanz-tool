"""
Upload handlers to test the upload API.
"""

from django.core.files.uploadhandler import FileUploadHandler


class QuotaUploadHandler(FileUploadHandler):
    """
    This test upload handler terminates the connection if more than a quota
    (50MB) is uploaded.
    """

    QUOTA = 50 * 2**20  # 50 MB

    def __init__(self, request=None):
        super().__init__(request)
        self.total_upload = 0

    def receive_data_chunk(self, raw_data, start):
        self.total_upload += len(raw_data)
        return raw_data

    def file_complete(self, file_size):
        return self.total_upload >= self.QUOTA
