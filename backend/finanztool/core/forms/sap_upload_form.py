from django import forms


class SAPUploadForm(forms.Form):
    file = forms.FileField()
