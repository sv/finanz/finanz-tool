from decimal import Decimal

from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet

from finanztool.core.models import Amount


class AmountInlineFormSet(BaseInlineFormSet):
    def clean(self):
        super().clean()

        total_gross = Decimal("0.00")
        for form in self.forms:
            if form.instance.gross:
                total_gross += Decimal(form.instance.gross)

        # Validation of Amounts
        errors = []
        if total_gross < Decimal("0.0"):
            errors.append(ValidationError('Betrag darf ab Status "Erledigt" nicht kleiner 0 sein!'))
        if self.instance.total_in_words == "" and total_gross >= Decimal("1000.00"):
            errors.append(ValidationError("Betrag in Worten ab 1000€ benötigt"))
        if self.instance.total_in_words != "" and self.instance.total_in_words != Amount.calculate_total_in_words(
            total_gross
        ):
            errors.append(
                ValidationError(
                    "Betrag in Worten passt nicht zum Gesamtbetrag. Erwartet: {}".format(
                        Amount.calculate_total_in_words(total_gross)
                    )
                )
            )
        if errors:
            raise ValidationError(errors)
