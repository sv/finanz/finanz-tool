from django.template import Library

register = Library()


@register.filter
def is_public(queryset, value):
    return queryset.filter(is_public=value)
