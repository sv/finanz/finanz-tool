from django.template import Library
from django.urls import reverse

register = Library()


@register.filter
def link_to_admin(obj):
    """This method returns the link to the admin site of the booking"""
    # type(self)()._meta.model_name computes the modelname of the instance
    if type(obj)()._meta.model_name == "cashtransfer":
        return reverse("admin:core_cashsettlement_change", args=(obj.cashsettlement.id,))
    return reverse("admin:core_" + type(obj)()._meta.model_name + "_change", args=(obj.id,))


@register.filter
def invoice_type(obj):
    """Return the type of the given invoice (received or issued)"""
    return type(obj)()._meta.model_name.removesuffix("invoice")
