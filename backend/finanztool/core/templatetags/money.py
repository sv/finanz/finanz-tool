from django.template import Library
from django.utils.safestring import mark_safe

register = Library()


@register.filter
def amount(value):
    return "{:,.2f} €".format(value)


@register.filter
def negative(value):
    return -value


@register.filter(safe=True)
def amount_bold(value):
    tag = '<b style="color:red;">' if value < 0 else "<b>"
    return mark_safe(tag + amount(value) + "</b>")
