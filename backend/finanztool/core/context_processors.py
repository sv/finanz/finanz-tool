from django.conf import settings


def debug_branding(request):
    if settings.DEBUG:
        return {
            "DEBUG_BRANDING": settings.DEBUG_BRANDING,
        }
    return {}
