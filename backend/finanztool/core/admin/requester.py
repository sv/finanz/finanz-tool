from django import forms
from django.contrib import admin
from django.contrib.auth.hashers import make_password

from finanztool.core.models import Requester


class RequesterAdminForm(forms.ModelForm):
    def clean_password(self):
        for changed_field in self.changed_data:
            if changed_field == "password":
                self.cleaned_data["password"] = make_password(self.data.get("password"))
        return self.cleaned_data["password"]


@admin.register(Requester)
class RequesterAdmin(admin.ModelAdmin):
    form = RequesterAdminForm
    filter_horizontal = ["user"]

    def get_list_display(self, request):
        if request.user.is_superuser:
            return ["name", "fond", "cost_center", "karma"]
        return ["name", "fond", "cost_center"]

    list_display = [
        "get_list_display",
    ]
    ordering = ["-cost_center", "fond"]
    search_fields = ["name", "cost_center"]
