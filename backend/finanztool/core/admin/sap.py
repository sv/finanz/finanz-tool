from django.contrib import admin, messages
from django.db.models import F, Q, Sum
from django.utils.html import format_html

from finanztool.core.frontend.views import MatchingCheckView
from finanztool.core.models import SAPBooking, fond
from finanztool.core.sap_matching import match_sap
from finanztool.core.templatetags.util import link_to_admin


class SAPBookingMatchingFilter(admin.SimpleListFilter):
    title = "Zugeordnet"
    parameter_name = "matched"

    def lookups(self, request, model_admin):
        return (
            ("yes", "Ja"),
            ("no", "Nein"),
        )

    def queryset(self, request, queryset):
        is_unmatched = Q()
        for field in SAPBooking.INTERNAL_BOOKING_FIELDS:
            is_unmatched &= Q(**{field: None})
        if self.value() == "yes":
            return queryset.exclude(is_unmatched)
        if self.value() == "no":
            return queryset.filter(is_unmatched)
        if self.value() is None:
            return None
        raise NotImplementedError(f"Lookup {self.value()} not implemented")


class SAPBookingProblemFilter(admin.SimpleListFilter):
    title = "Problemfinder"
    parameter_name = "problemfinder"

    def lookups(self, request, model_admin):
        return (
            ("internal_not_booked", 'Zugeordnete interne Buchung nicht auf Status "gebucht"'),
            ("year_mismatch", "Geschäftsjahre passen nicht zusammen"),
            ("fond_mismatch", "Fonds passen nicht zusammen"),
            ("tax_fond_mismatch", "Vorsteuerabzug passt nicht zum Fond"),
        )

    def queryset(self, request, queryset):
        booked_states = ("sap_done", "paid", "cancelled")
        if self.value() == "internal_not_booked":
            queryset_filter = Q()
            for field in SAPBooking.INTERNAL_BOOKING_FIELDS:
                queryset_filter |= ~Q(**{field: None}) & ~Q(**{f"{field}__status__in": booked_states})
            return queryset.filter(queryset_filter)
        if self.value() == "year_mismatch":
            queryset_filter = Q()
            for field in SAPBooking.INTERNAL_BOOKING_FIELDS:
                queryset_filter |= ~Q(**{field: None}) & ~Q(**{f"{field}__annual_plan__year": F("geschaeftsjahr")})
            return queryset.filter(queryset_filter)
        if self.value() == "fond_mismatch":
            queryset_filter = Q()
            for field in SAPBooking.INTERNAL_BOOKING_FIELDS:
                queryset_filter |= ~Q(**{field: None}) & ~Q(**{f"{field}__requester__fond__number": F("fond")})
            return queryset.filter(queryset_filter)
        if self.value() == "tax_fond_mismatch":
            queryset_filter = Q()
            for field in SAPBooking.INTERNAL_BOOKING_FIELDS:
                queryset_filter |= ~Q(**{field: None}) & (
                    (
                        # Bruttobuchungen nur auf Bruttofonds
                        Q(**{f"{field}__requester__fond__booking_type": fond.NET})
                        & (Q(steuerkennzeichen__startswith="N") | Q(steuerkennzeichen__startswith="F"))
                    )
                    | (
                        # Vorsteuerabzug nur auf Nettofonds
                        Q(**{f"{field}__requester__fond__booking_type": fond.GROSS})
                        & (Q(steuerkennzeichen__startswith="V") | Q(steuerkennzeichen__startswith="E"))
                    )
                )
            # V0 für Privatrechnungen auf beiden Fonds ok -> ignorieren
            return queryset.exclude(steuerkennzeichen="V0").filter(queryset_filter)
        if self.value() is None:
            return None
        raise NotImplementedError(f"Lookup {self.value()} not implemented")


def sap_matching(_, request, queryset):
    return MatchingCheckView.render_form(request, match_sap(queryset))


sap_matching.short_description = "Automatisch zuordnen"
sap_matching.allowed_permissions = ("delete",)


def verify_matching(modeladmin, request, queryset):
    del modeladmin  # unused
    return MatchingCheckView.render_form(
        request,
        {
            "sections": [
                {
                    "title": "Alle Zuordnungen",
                    "matches": [
                        {
                            "booking": booking,
                            "matches": [
                                {
                                    "invoice": booking.get_internal_booking(),
                                    "score": 0,
                                }
                            ],
                        }
                        for booking in queryset
                        if booking.get_internal_booking()
                    ],
                },
            ],
        },
    )


verify_matching.short_description = "Matches manuell verifizieren"


def reset_matching(modeladmin, request, queryset):
    del modeladmin, request  # unused
    for booking in queryset:
        booking.set_internal_booking(None)
        booking.clean()
        booking.save()


reset_matching.short_description = "Zuordnung zurücksetzen"
reset_matching.allowed_permissions = ("delete",)


def stats(modeladmin, request, queryset):
    res = queryset.aggregate(sum_tw_betrag=Sum("tw_betrag"))
    modeladmin.message_user(
        request,
        format_html(
            "Summe der Beträge: <b>{} €</b>.",
            res["sum_tw_betrag"],
        ),
        messages.SUCCESS,
    )


stats.short_description = "Statistiken (Summe, etc.) über Auswahl berechnen"


@admin.register(SAPBooking)
class SAPBookingAdmin(admin.ModelAdmin):
    list_display = [
        "fi_belegnummer",
        "buchungsdatum",
        "position_fi_belegnr",
        "tw_betrag",
        "name",
        "hauptbuchkonto",
        "intern",
    ]

    ordering = ["-buchungsdatum", "fi_belegnummer", "position_fi_belegnr"]
    list_filter = [
        SAPBookingProblemFilter,
        SAPBookingMatchingFilter,
        "zahlungsstatus",
        "geschaeftsjahr",
        "fond",
        "finanzstelle",
        "hauptbuchkonto",
    ]
    search_fields = ["fi_belegnummer"]
    autocomplete_fields = list(SAPBooking.INTERNAL_BOOKING_FIELDS)
    actions = [reset_matching, sap_matching, verify_matching, stats]

    def get_readonly_fields(self, request, obj=None):
        return list(
            set(
                [field.name for field in self.opts.local_fields]
                + [field.name for field in self.opts.local_many_to_many]
            ).difference(set(SAPBooking.INTERNAL_BOOKING_FIELDS))
        )

    def intern(self, obj):
        booking = obj.get_internal_booking()
        if not booking:
            return None
        return format_html(
            '<a href="{}">{}</a>',
            link_to_admin(booking),
            booking.unique_id(),
        )

    def has_add_permission(self, request):
        # SAPBookings should only be added using CSV import, never mannually
        return False
