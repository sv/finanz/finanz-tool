import datetime
import re

from django.contrib import admin, messages
from django.core.exceptions import ValidationError
from django.db.models import Sum
from django.urls import reverse
from django.utils.formats import localize
from django.utils.html import format_html, mark_safe

from finanztool.core.admin.util import admin_action_individual_errorhandling, log_change
from finanztool.core.models import InternalBooking, Invoice, SAPBooking, ZeroSumBooking

INTERNAL_ID = re.compile(r"(?:^|\s)(\d+)?/(\d{2,4})?(?:$|\s)")


class BookingNumberAutoIncrementMixin:
    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        if obj is None:
            num, annual_plan = self.model.create_booking_id()
            form.base_fields["internal_booking_number"].initial = num
            form.base_fields["annual_plan"].initial = annual_plan
        return form


@admin_action_individual_errorhandling
def update_sap_status(modeladmin, request, queryset):
    for booking in queryset:
        qs = SAPBooking.objects

        found = False
        for field, t in SAPBooking.INTERNAL_BOOKING_FIELDS.items():
            if isinstance(booking, t):
                qs = qs.filter(**{field: booking})
                found = True
                break
        if not found:
            raise RuntimeError("Unsupported invoice type")

        if booking.compare_status("done") < 0:
            # ignore new bookings
            continue

        is_paid = True
        sap_amount = abs(sum(map(lambda x: x.tw_betrag, qs))) - abs(booking.calculate_booked_total())

        def set_status(status):
            if status == booking.status:
                return
            old_status = booking.status
            booking.status = status
            booking.clean()
            booking.save()
            log_change(booking, request, f'(SAP) changed status to "{status}"')
            modeladmin.message_user(
                request,
                format_html(
                    '<a href="{}">{}</a>: Status geändert von {} auf {}.',
                    reverse("admin:core_{}_change".format(type(booking).__name__.lower()), args=(booking.pk,)),
                    str(booking),
                    old_status,
                    status,
                ),
                messages.SUCCESS if booking.compare_status(old_status) > 0 else messages.WARNING,
            )

        if isinstance(booking, Invoice):
            if booking.compare_status("cancelled") >= 0 or booking.compare_status("done") <= 0:
                # ignore cancelled and done invoices
                continue
            is_paid = all(map(lambda x: x.zahlungsstatus, qs))

            if not is_paid or not abs(sap_amount) == 0.00:
                if abs(sap_amount) == 0.00:
                    set_status("sap_done")
                else:
                    set_status("sent")
                continue

        if not abs(sap_amount) == 0 or not is_paid:
            if abs(sap_amount) == 0:
                set_status("sap_done")
            else:
                set_status("done")
            continue

        if abs(sap_amount) == 0:
            if is_paid:
                set_status("paid")
            else:
                set_status("sap_done")


update_sap_status.short_description = "SAP Status abgleichen"
update_sap_status.allowed_permissions = ("delete",)


@admin_action_individual_errorhandling
def move_unpaid_to_current_year(modeladmin, request, queryset):
    for booking in queryset:
        if booking.compare_status("paid") < 0:
            # generate new booking id
            new_num, new_annual_plan = booking.create_booking_id()
            if new_annual_plan == booking.annual_plan:
                raise ValidationError("Buchung ist bereits in neuestem Jahr")
            # store old data for change message
            old_id = booking.unique_id()
            old_booking_date = booking.booking_date
            old_booking_date = "{:%d.%m.%Y}".format(old_booking_date) if old_booking_date else "-"
            # set new id and booking date
            booking.internal_booking_number = new_num
            booking.annual_plan = new_annual_plan
            if booking.booking_date:
                booking.booking_date = datetime.date(int(new_annual_plan.year), 1, 1)
            # add note
            booking.add_note(
                f"Verschoben nach {new_annual_plan.year} von ID {old_id} mit Buchungsdatum {old_booking_date}"
            )
            booking.clean()
            booking.save()
            modeladmin.message_user(
                request, "Buchung Verschoben: {} -> {}".format(old_id, booking.unique_id()), messages.SUCCESS
            )


move_unpaid_to_current_year.short_description = "Unbezahlte Buchungen in aktuelles Jahr verschieben"
move_unpaid_to_current_year.allowed_permissions = ("delete",)


class BookingAdmin(BookingNumberAutoIncrementMixin, admin.ModelAdmin):
    readonly_fields_done = []
    forcible_readonly_fields = []
    search_fields = [
        "annual_plan__year__endswith",
        "internal_booking_number",
        "name",
        "requester__name",
        "note",
    ]

    def get_readonly_fields(self, request, obj=None):
        if "force_edit" in request.GET and request.user.is_superuser:
            return self.readonly_fields
        additional_fields = []
        if obj and obj.compare_status("done") >= 0:
            additional_fields += [
                "annual_plan",
                "internal_booking_number",
                "name",
                "requester",
                "booking_date",
            ]
            additional_fields += self.readonly_fields_done
        additional_fields += ["status", "sapbookings"]
        additional_fields += self.forcible_readonly_fields
        additional_fields += self.readonly_fields
        if request.user.is_superuser:
            additional_fields += ["force_edit_button"]
        return additional_fields

    def force_edit_button(self, _):
        return format_html(
            '<a class="button" href="?force_edit">Editieren erzwingen</a>',
        )

    def sapbookings(self, obj):
        if not obj.sapbooking_set.count():
            return "-"
        return mark_safe(
            "<table>"
            + "".join(
                [
                    format_html(
                        '<tr><td><a href="{}">{}</a></td><td>{}</td>',
                        reverse("admin:core_sapbooking_change", args=(booking.pk,)),
                        str(booking),
                        localize(booking.tw_betrag),
                    )
                    for booking in obj.sapbooking_set.all()
                ]
            )
            + format_html(
                "<tr><th>Sum</th><th>{}</th>",
                localize(obj.sapbooking_set.aggregate(Sum("tw_betrag"))["tw_betrag__sum"]),
            )
            + "</table>"
        )

    actions = [update_sap_status, move_unpaid_to_current_year]

    def get_search_results(self, request, queryset, search_term):
        # allow for searching by the "internal_booking_number/year" syntax
        # internal_booking_number is optional. If it is not given, all bookings
        # of the given year are matched
        # year is optional. If it is not given, the current year is matched
        # examples:
        #  - 635/ (booking 635 of current year)
        #  - 635/22 (booking 635 of year 2022)
        #  - /22 (all bookings of the year 2022)
        #  - / (all bookings of the current year)

        # if we find the custom search tag, remove it from the search for django
        m = INTERNAL_ID.search(search_term)
        if m:
            search_term = INTERNAL_ID.sub("", search_term)

        # execute search using other criteria
        res_queryset, may_have_duplicates = super().get_search_results(
            request,
            queryset,
            search_term,
        )

        # apply our custom criteria
        if m:
            if m.group(1):
                res_queryset = res_queryset.filter(internal_booking_number__endswith=m.group(1))
            if m.group(2):
                res_queryset = res_queryset.filter(annual_plan__year__endswith=m.group(2))
            else:
                res_queryset = res_queryset.filter(annual_plan__year=str(datetime.date.today().year))
        return res_queryset, may_have_duplicates


@admin.register(InternalBooking)
class InternalBookingAdmin(BookingAdmin):
    list_filter = ["annual_plan__year"]

    autocomplete_fields = ["requester", "sender"]


@admin.register(ZeroSumBooking)
class ZeroSumBookingAdmin(BookingAdmin):
    autocomplete_fields = ["requester"]
