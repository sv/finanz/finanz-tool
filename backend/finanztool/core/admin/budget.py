import io
import zipfile

import fitz
from django.contrib import admin, messages
from django.core.exceptions import ValidationError
from django.http import FileResponse, HttpResponseRedirect
from django.urls import reverse
from django.utils.datetime_safe import datetime
from django.utils.http import urlencode

from finanztool.core.admin.util import (
    ActionButtonMixin,
    admin_action_individual_errorhandling,
    log_change,
)
from finanztool.core.models import Budget, Offer, Request
from finanztool.pdf.generation.pdf_generation import fleischwolf, stamp_offer_number


def create_invoice(modeladmin, request, queryset):
    if queryset.count() == 1:
        budget = queryset[0]
        if budget.status == "approved":
            query = {
                "name": budget.name,
                "requester": budget.budget.id,
                "date": datetime.now().date(),
                "budget": budget.id,
            }
            return HttpResponseRedirect(reverse("admin:core_receivedinvoice_add") + "?" + urlencode(query))
        modeladmin.message_user(
            request,
            'Es kann nur eine Rechnung erstellt werden wenn der Status auf "Genehmigt" gesetzt ist!',
            messages.ERROR,
        )
    else:
        modeladmin.message_user(request, "Es kann nur eine Rechnung gleichzeitig erstellt werden!", messages.ERROR)
    return None


create_invoice.short_description = "Neue Rechnung für Budget erstellen."
create_invoice.button_text = "Neue Rechnung"
create_invoice.button_show = lambda obj: obj.status == "approved"


@admin_action_individual_errorhandling
def set_approved(modeladmin, request, queryset):
    del modeladmin
    for budget in queryset:
        if budget.status == "requested":
            budget.status = "approved"
            budget.clean()
            budget.save()
            log_change(budget, request, 'changed status to "approved"')
            if hasattr(budget, "request"):
                budget.request.send_status_email("approved")
        else:
            raise ValidationError('Aus diesem Status kann nicht in den Status "Genehmigt" gewechselt werden!')


set_approved.short_description = "Status auf genehmigt setzen"
set_approved.button_text = "Genehmigen"
set_approved.button_show = lambda obj: obj.status == "requested"
set_approved.allowed_permissions = ("delete",)


@admin_action_individual_errorhandling
def set_request_approved(modeladmin, request, queryset):
    set_approved(modeladmin, request, map(lambda req: req.budget, queryset))


set_request_approved.short_description = "Status der Anfrage auf genehmigt setzen"
set_request_approved.button_text = "Genehmigen"
set_request_approved.button_show = lambda obj: obj.budget.status == "requested"
set_request_approved.allowed_permissions = ("delete",)


@admin_action_individual_errorhandling
def set_za3_requested(modeladmin, request, queryset):
    del modeladmin
    for req in queryset:
        if req.budget.status == "requested" and not req.za3_requested:
            req.za3_requested = True
            req.clean()
            req.save()
            req.generate_form()
            log_change(req, request, 'changed status to "ZA3 requested"')
        else:
            raise ValidationError('Aus diesem Status kann nicht in den Status "ZA3 Anfrage" gewechselt werden!')


set_za3_requested.short_description = 'Status von "Angebote benötigt" auf "ZA3 Anfrage" setzen.'
set_za3_requested.button_text = "ZA3 anfragen"
set_za3_requested.button_show = lambda obj: (obj.budget.status == "requested" and not obj.za3_requested)
set_za3_requested.allowed_permissions = ("delete",)


@admin_action_individual_errorhandling
def set_declined(modeladmin, request, queryset):
    del modeladmin
    for req in queryset:
        if not req.denial_reason:
            raise ValidationError("Es muss ein Ablehnungsgrund angegeben werden!")
        if req.budget.status == "requested":
            req.budget.status = "closed"
            req.budget.clean()
            req.budget.save()
            req.clean()
            req.save()
            req.send_status_email("declined")
            log_change(req, request, 'changed status to "declined"')
        else:
            raise ValidationError('Aus diesem Status kann nicht in den Status "Abgelehnt" gewechselt werden!')


set_declined.short_description = "Status abgelehnt setzen."
set_declined.button_text = "Ablehnen"
set_declined.button_show = lambda obj: obj.budget.status == "requested"
set_declined.allowed_permissions = ("delete",)


def print_contract_award_notice(modeladmin, request, queryset):
    out = None
    if queryset.count() > 1:
        file_handle = io.BytesIO()
        # pylint: disable=consider-using-with
        # writes to BytesIO and does not actually lock resources
        out = zipfile.ZipFile(file_handle, "w")
    for req in queryset:
        # check if status is right
        if not req.za3_requested:
            modeladmin.message_user(
                request,
                req.budget.name + ' - Der Vergabevermerk kann nur ab Status "ZA3 Anfrage" gedruckt werden!',
                messages.ERROR,
            )
            return None
        with req.contract_award_notice_file.open("rb") as generated_file_reader:
            doc = fitz.Document(stream=generated_file_reader.read(), filetype="application/pdf")
        for num, offer in enumerate(req.offer_set.all(), start=1):
            with offer.offer_file.open("rb") as offer_file_reader:
                offer_doc = fleischwolf(fitz.Document(stream=offer_file_reader.read(), filetype="application/pdf"))
                stamp_offer_number(offer_doc, num)
                doc.insert_pdf(offer_doc)
        filename = "Direktauftrag_" + str(req)
        if out is None:
            return FileResponse(io.BytesIO(doc.write()), content_type="application/pdf", filename=filename)
        out.writestr(filename, doc.write())
    out.close()
    file_handle.seek(0)
    return FileResponse(file_handle, as_attachment=True, content_type="application/zip", filename=filename)


print_contract_award_notice.short_description = "Vergabevermerke mit Anhängen herunterladen."
print_contract_award_notice.button_text = "Vergabevermerk"
print_contract_award_notice.button_show = lambda obj: (obj.za3_requested and obj.budget.status == "requested")


# def move_to_new_year(modeladmin: ModelAdmin, request: HttpRequest, queryset: set[Request]) -> None:
#     today: date = date.today()
#     current_year: int = today.year
#     last_annual_plan: AnnualPlan = AnnualPlan.objects.last()

#     budget: Budget
#     for budget in queryset:
#         if budget.status == "closed" or budget.is_locked():
#             modeladmin.message_user(
#                 request,
#                 budget.name
#                 + " - Angebrochene, abgelehnte oder geschlossene Anträge können nicht ins neue Haushaltsjahr verschoben werden!",
#                 messages.WARNING,
#             )
#             continue
#         if int(budget.annual_plan.year) != current_year:
#             modeladmin.message_user(
#                 request,
#                 budget.name + " - Anträge die nicht im aktuellen Haushaltsplan liegen, können nicht verschoben werden!",
#                 messages.WARNING,
#             )
#             continue
#         if int(last_annual_plan.year) == current_year:
#             modeladmin.message_user(
#                 request,
#                 budget.name + " - Der neue Jahresplan konnte nicht gefunden werden! Der Antrag wurde nicht verschoben.",
#                 messages.WARNING,
#             )
#             continue

#         requester: Requester = Requester.get_by_name("Rücklagen - Ausgaben Vorjahr")
#         if requester is not None:
#             budget.requester = requester
#         budget.annual_plan = last_annual_plan

#         budget.add_note(
#             "Antrag in Jahresplan {0:} verschoben "
#             'und Antragssteller auf "{1:}" geändert.\n'.format(last_annual_plan, budget.requester.name)
#         )

#         budget.clean()
#         budget.save()

#         modeladmin.message_user(
#             request,
#             budget.name
#             + ' - Der Antrag wurde mit dem Antragssteller "{0:}" in den Jahresplan "{1:}" verschoben.'.format(
#                 last_annual_plan, requester.name
#             ),
#             messages.SUCCESS,
#         )


# move_to_new_year.short_description = "In neuen Jahresplan verschieben"
# move_to_new_year.allowed_permissions = ("delete",)


class OfferInlineAdmin(admin.TabularInline):
    model = Offer


@admin.register(Request)
class RequestAdmin(ActionButtonMixin, admin.ModelAdmin):
    list_display = ["name", "requester", "target", "status"]
    list_filter = ["budget__status"]
    readonly_fields = ["contract_award_notice_file", "za3_requested"]
    actions = [
        print_contract_award_notice,
        set_request_approved,
        set_za3_requested,
        set_declined,
    ]
    # TODO: make everything readonly one in status approved or finished

    inlines = [
        OfferInlineAdmin,
    ]

    def name(self, obj):
        return obj.budget.name

    def requester(self, obj):
        return obj.budget.requester

    def target(self, obj):
        return obj.budget.target

    def status(self, obj):
        return obj.budget.status


@admin.register(Budget)
class BudgetAdmin(ActionButtonMixin, admin.ModelAdmin):
    list_display = ["requester", "name", "target"]
    autocomplete_fields = ["requester", "parent"]
    search_fields = ["requester__name", "name"]

    actions = [create_invoice, set_approved]

    def get_readonly_fields(self, request, obj=None):
        additional_fields = ()
        if obj is not None and obj.is_locked():
            additional_fields += ("annual_plan", "requester")
        return super().get_readonly_fields(request, obj) + additional_fields
