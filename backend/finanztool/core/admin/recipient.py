from django.contrib import admin

from finanztool.core.models import Recipient, Vendor


@admin.register(Vendor)
class VendorAdmin(admin.ModelAdmin):
    ordering = ["name"]
    search_fields = ["name"]


@admin.register(Recipient)
class RecipientAdmin(admin.ModelAdmin):
    ordering = ["name"]
    search_fields = ["name"]
