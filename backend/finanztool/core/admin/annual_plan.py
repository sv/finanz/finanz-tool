from decimal import Decimal

from adminsortable2.admin import SortableAdminBase, SortableInlineAdminMixin
from django.contrib import admin
from django.utils.formats import localize
from django.utils.html import format_html, mark_safe

from finanztool.core.models import (
    AnnualPlan,
    BudgetReceiver,
    Fond,
    GeneralLedgerAccount,
    Requester,
)


class AnnualPlanBudgetReceiverInlineAdmin(SortableInlineAdminMixin, admin.TabularInline):
    model = BudgetReceiver
    autocomplete_fields = ["receiver"]

    def __init__(self, model, admin_site, fond=None):
        super().__init__(model, admin_site)
        self._fond = fond

    @property
    def verbose_name(self):
        if hasattr(self, "_fond") and self._fond:
            return f"Budget - {self._fond.name}"
        return "Budget"

    @property
    def verbose_name_plural(self):
        return self.verbose_name.replace("Budget", "Budgets")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if hasattr(self, "_fond") and self._fond:
            return qs.filter(receiver__fond=self._fond)
        return qs

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "receiver" and hasattr(self, "_fond") and self._fond:
            kwargs["queryset"] = Requester.objects.filter(fond=self._fond)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.display(description="Budgets pro Fond")
def fond_budgets(obj: AnnualPlan):
    sums = {}
    sums["Budget"] = obj.budget_allocation
    for fond in Fond.objects.all():
        sums[fond.name] = Decimal("0")
        for receiver in BudgetReceiver.objects.filter(receiver__fond=fond, annual_plan=obj):
            sums[fond.name] += receiver.target
    sums["Übrig"] = sum(sums.values())
    return mark_safe(
        "<table>"
        + "".join(
            [
                format_html(
                    "<tr><td>{}</td><td>{}</td></tr>",
                    name,
                    localize(val),
                )
                for name, val in sums.items()
            ]
        )
        + "</table>"
    )


@admin.register(AnnualPlan)
class AnnualPlanAdmin(SortableAdminBase, admin.ModelAdmin):
    def get_readonly_fields(self, request, obj=None):
        fields = [fond_budgets]
        if obj:
            fields += ["year"]
        return fields

    list_display = ["year", "budget_allocation"]
    inlines = (AnnualPlanBudgetReceiverInlineAdmin,)

    def get_inline_instances(self, request, obj=None):
        return [AnnualPlanBudgetReceiverInlineAdmin(self.model, self.admin_site, fond) for fond in Fond.objects.all()]


admin.site.register(Fond)
admin.site.register(GeneralLedgerAccount)
