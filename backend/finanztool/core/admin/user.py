from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from finanztool.core.models import UserProfile


class UserProfileAdmin(admin.StackedInline):
    model = UserProfile
    extra = 0


class CustomUserAdmin(UserAdmin):
    # Function to count objects of each user from another Model (where user is FK)
    inlines = [UserProfileAdmin]


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
