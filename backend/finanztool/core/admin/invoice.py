from decimal import Decimal

from django import forms
from django.contrib import admin, messages
from django.core.exceptions import ValidationError
from django.core.files import File
from django.db.models.functions import Lower
from django.forms.fields import CheckboxInput
from django.urls import reverse
from django.utils.datetime_safe import date, datetime
from django.utils.html import format_html

from finanztool.core.admin.booking import BookingAdmin
from finanztool.core.admin.util import ActionButtonMixin, log_change
from finanztool.core.forms import AmountInlineFormSet
from finanztool.core.models import (
    Budget,
    GeneralLedgerAccount,
    Inventory,
    IssuedAmount,
    IssuedInvoice,
    ReceivedAmount,
    ReceivedInvoice,
    Recipient,
    Request,
    Vendor,
)
from finanztool.pdf.generation.pdf_generation import generate_changeinvoice
from finanztool.pdf.printing.printing import print_invoice


class ForceEditInlineMixin:
    """Mixin class for inline ModelAdmins whose ModelAdmins have a force-edit button
    feature that only makes most fields editable for superusers after pressing the
    "force-edit" button. This also makes the displayed inline objects readonly
    unless force-edit was activated"""

    def is_editable(self, request, obj: ReceivedInvoice = None):
        if "force_edit" in request.GET and request.user.is_superuser:
            return True
        return not obj or obj.status == "new"

    def has_add_permission(self, request, obj: ReceivedInvoice = None):
        return self.is_editable(request, obj)

    def has_change_permission(self, request, obj: ReceivedInvoice = None):
        return self.is_editable(request, obj)

    def has_delete_permission(self, request, obj: ReceivedInvoice = None):
        return self.is_editable(request, obj)


class InventoryInlineAdmin(admin.StackedInline):
    model = Inventory
    extra = 0
    readonly_fields = ["generated_file"]


class ReceivedAmountInlineAdmin(ForceEditInlineMixin, admin.TabularInline):
    model = ReceivedAmount
    formset = AmountInlineFormSet
    readonly_fields = ["net"]


class IssuedAmountInlineAdmin(ForceEditInlineMixin, admin.TabularInline):
    model = IssuedAmount
    formset = AmountInlineFormSet
    readonly_fields = ["net"]


def print_invoices(modeladmin, request, queryset):
    for invoice in queryset:
        try:
            print_invoice(invoice)
        # pylint: disable=broad-except
        except Exception as e:
            modeladmin.message_user(
                request,
                "({}) {} - {}".format(invoice.internal_booking_number, invoice.name, "{} - skipped".format(e)),
                messages.ERROR,
            )


print_invoices.short_description = "Drucke Rechnungen + Anweisungsformulare"
print_invoices.button_text = "Drucken"
print_invoices.button_show = lambda inv: inv.status == "done"


def set_invoices_payable(modeladmin, request, queryset):
    for invoice in queryset:
        if invoice.status == "new":
            invoice.status = "payable"
            try:
                invoice.clean()
                invoice.save()
                log_change(invoice, request, 'changed status to "payable"')
                modeladmin.message_user(
                    request,
                    "({}) {} - Der Status der Rechnung wurde erfolgreich geändert.".format(
                        invoice.internal_booking_number, invoice.name
                    ),
                    messages.INFO,
                )
            except ValidationError as err:
                for message in err.messages:
                    modeladmin.message_user(
                        request,
                        "({}) {} - {}".format(invoice.internal_booking_number, invoice.name, message),
                        messages.ERROR,
                    )


set_invoices_payable.short_description = 'Setze Status "zahlbar" (von "neu")'
set_invoices_payable.button_show = lambda obj: obj.status == "new"
set_invoices_payable.button_text = "Zahlbar"
set_invoices_payable.allowed_permissions = ("delete",)


def finish_invoices(modeladmin, request, queryset):
    for invoice in queryset:
        if invoice.compare_status("payable") <= 0:
            invoice.status = "done"
            invoice.signer = "  "
            full_name = request.user.get_full_name().split(" ")
            if len(full_name) >= 2:  # First and last name are required
                first_name = full_name[0]
                last_name = full_name[len(full_name) - 1]
                invoice.signer = first_name[0] + last_name[0]
            else:
                modeladmin.message_user(
                    request,
                    "Dein Namenskürzel konnte nicht automatisch erkannt werden! Bitte kontaktiere einen Administrator.",
                    messages.ERROR,
                )
                return
            invoice.booking_date = datetime.today()
            try:
                invoice.clean()
                invoice.save()
                invoice.generate_form()
                log_change(invoice, request, 'changed status to "done" and generated form')
                modeladmin.message_user(
                    request,
                    "({}) {} - Der Status der Rechnung wurde erfolgreich geändert.".format(
                        invoice.internal_booking_number, invoice.name
                    ),
                    messages.INFO,
                )
            except ValidationError as err:
                for message in err.messages:
                    modeladmin.message_user(
                        request,
                        "({}) {} - {}".format(invoice.internal_booking_number, invoice.name, message),
                        messages.ERROR,
                    )


finish_invoices.short_description = 'Setze Status "erledigt"'
finish_invoices.button_show = lambda obj: obj.compare_status("payable") <= 0
finish_invoices.button_text = "Erledigt"
finish_invoices.allowed_permissions = ("delete",)


def finish_invoices_without_signer(modeladmin, request, queryset):
    for invoice in queryset:
        if invoice.compare_status("payable") <= 0:
            invoice.status = "done"
            invoice.booking_date = datetime.today()
            try:
                invoice.clean()
                invoice.save()
                invoice.generate_form()
                log_change(invoice, request, 'changed status to "done" and generated form')
                modeladmin.message_user(
                    request,
                    "({}) {} - Der Status der Rechnung wurde erfolgreich geändert.".format(
                        invoice.internal_booking_number, invoice.name
                    ),
                    messages.INFO,
                )
            except ValidationError as err:
                for message in err.messages:
                    modeladmin.message_user(
                        request,
                        "({}) {} - {}".format(invoice.internal_booking_number, invoice.name, message),
                        messages.ERROR,
                    )


finish_invoices_without_signer.short_description = 'Setze Status "erledigt" (behalte Finanzerkürzel)'
finish_invoices_without_signer.allowed_permissions = ("delete",)


def mark_invoice_as_sent(modeladmin, request, queryset):
    for invoice in queryset:
        if invoice.status == "done":
            invoice.status = "sent"
            try:
                invoice.clean()
                invoice.save()
                log_change(invoice, request, 'changed status to "sent"')
                modeladmin.message_user(
                    request,
                    "({}) {} - Der Status der Rechnung wurde erfolgreich geändert.".format(
                        invoice.internal_booking_number, invoice.name
                    ),
                    messages.INFO,
                )
            except ValidationError as err:
                for message in err.messages:
                    modeladmin.message_user(
                        request,
                        "({}) {} - {}".format(invoice.internal_booking_number, invoice.name, message),
                        messages.ERROR,
                    )


mark_invoice_as_sent.short_description = 'Setze Status "versandt" (von "erledigt")'
mark_invoice_as_sent.button_show = lambda obj: obj.status == "done"
mark_invoice_as_sent.button_text = "Versandt"
mark_invoice_as_sent.allowed_permissions = ("delete",)


def calculate_inwords(modeladmin, request, queryset):
    for invoice in queryset:
        if invoice.compare_status("sent") < 0:
            # these are valid
            continue
        if not invoice.booking_date or invoice.booking_date >= date(2020, 11, 24):
            # only fix invoices that were created after this was merged
            continue
        if invoice.calculate_gross() < Decimal("1000.00"):
            continue
        if invoice.total_in_words:
            continue
        invoice.total_in_words = invoice.calculate_total_in_words()
        modeladmin.message_user(
            request,
            format_html(
                'Betrag in Worten berechnet für <a href="{}">{}</a>: {}',
                reverse("admin:core_{}_change".format(type(invoice).__name__.lower()), args=(invoice.pk,)),
                str(invoice),
                invoice.total_in_words,
            ),
            messages.SUCCESS,
        )
        invoice.save()
        log_change(invoice, request, "calculated amount in words")


calculate_inwords.short_description = "Betrag in Worten ergänzen"
calculate_inwords.allowed_permissions = ("delete",)


class InvoiceAdmin(BookingAdmin):
    # fields that are readonly by default but can be edited by superusers
    forcible_readonly_fields = []

    autocomplete_fields = [
        "budget",
    ]

    def get_readonly_fields(self, request, obj=None):
        if "force_edit" in request.GET and request.user.is_superuser:
            return self.readonly_fields
        additional_fields = ["form_file"]
        if obj and obj.compare_status("done") >= 0:
            additional_fields += [
                "general_ledger_account",
                "date",
                "reason",
                "invoice_file",
            ]
        return super().get_readonly_fields(request, obj) + additional_fields


class ReceivedInvoiceAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if "request" in self.fields:
            # filter selectable requests according to requester, annual_plan and status
            if hasattr(self.instance, "requester") and hasattr(self.instance, "annual_plan"):
                qs = Budget.objects.filter(
                    requester=self.instance.requester,
                    annual_plan=self.instance.annual_plan,
                    status__in=["approved", "done"],
                )
                if hasattr(self.instance, "budget"):
                    qs |= Budget.objects.filter(pk=self.instance.budget.pk)
                self.fields["budget"].queryset = qs.order_by("date")
            else:
                self.fields["request"].queryset = Request.objects.filter(
                    status__in=["approved", "done"],
                ).order_by("date")
        # make this field mandatory in the form although it can be null in the db for legacy reasons
        if "electronic_invoice" in self.fields and (
            self.instance is None or self.instance.electronic_invoice is not None
        ):
            self.fields["electronic_invoice"].widget = CheckboxInput()
            self.fields["electronic_invoice"].required = True


@admin.register(ReceivedInvoice)
class ReceivedInvoiceAdmin(ActionButtonMixin, InvoiceAdmin):
    search_fields = property(
        lambda _: super().search_fields
        + [
            "general_ledger_account__description",
            "vendor__name",
            "vendor_invoice_number",
            "receivedamount__gross",
            "receivedamount__net",
        ]
    )
    list_display = ["get_name", "calculate_booked_total", "status"]
    list_filter = ["status", "requester"]
    ordering = ["-annual_plan__year", "-internal_booking_number"]
    actions = property(
        lambda _: super().actions
        + [
            set_invoices_payable,
            print_invoices,
            finish_invoices_without_signer,
            finish_invoices,
            mark_invoice_as_sent,
            calculate_inwords,
        ]
    )

    readonly_fields_done = [
        "vendor_invoice_number",
        "reference",
        "vendor",
        "budget",
        "electronic_invoice",
        "signer",
        "total_in_words",
    ]

    autocomplete_fields = property(lambda _: super().autocomplete_fields + ["vendor", "requester"])
    date_hierarchy = "booking_date"

    inlines = [
        ReceivedAmountInlineAdmin,
        InventoryInlineAdmin,
    ]

    form = ReceivedInvoiceAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "vendor":
            kwargs["queryset"] = Vendor.objects.order_by(Lower("name"))
        if db_field.name == "general_ledger_account":
            kwargs["queryset"] = GeneralLedgerAccount.objects.order_by("number")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_name(self, obj):
        return str(obj)

    def gross_total(self, obj):
        return obj.calculate_gross()


@admin.register(IssuedInvoice)
class IssuedInvoiceAdmin(ActionButtonMixin, InvoiceAdmin):
    search_fields = property(
        lambda _: super().search_fields
        + [
            "general_ledger_account__description",
            "recipient__name",
            "bkz",
            "issuedamount__gross",
            "issuedamount__net",
        ]
    )
    list_display = ["get_name", "calculate_booked_total", "status"]
    list_filter = ["status", "requester"]

    actions = property(
        lambda _: super().actions
        + [set_invoices_payable, print_invoices, finish_invoices, mark_invoice_as_sent, calculate_inwords]
    )

    ordering = ["-annual_plan__year", "-internal_booking_number"]

    readonly_fields = ["change_form_file"]

    readonly_fields_done = [
        "recipient",
        "bkz",
        "monition",
        "signer",
        "total_in_words",
    ]

    autocomplete_fields = property(lambda _: super().autocomplete_fields + ["recipient", "requester"])
    date_hierarchy = "booking_date"

    inlines = [
        IssuedAmountInlineAdmin,
    ]

    def save_model(self, request, obj, form, change):
        if not change:
            super().save_model(request, obj, form, change)
            return
        instance_old = form._meta.model.objects.get(pk=form.instance.pk)
        gross_old = instance_old.calculate_gross()
        formsets, _ = self._create_formsets(request, form.instance, change)
        for formset in formsets:
            formset.save(commit=False)
            super().save_model(request, obj, form, change)

            if obj.status in ("new", "done"):
                pass

            elif obj.status == "cancelled":
                file_handle = generate_changeinvoice(obj, obj, Decimal("0.00"))

                # save change pdf
                file_name = obj.get_filename("change_form_file")

                obj.change_form_file.save(file_name, File(file_handle), save=False)

            elif obj.compare_status("sent") >= 0:
                if obj.note != "" and obj.note[-1] != "\n":
                    obj.note += "\n"
                if obj.general_ledger_account != instance_old.general_ledger_account:
                    obj.note += "{0:%d.%m.%Y}: Änderung Sachkonto von {1:} zu {2:}.\n".format(
                        date.today(), instance_old.general_ledger_account, obj.general_ledger_account
                    )
                if obj.bkz != instance_old.bkz:
                    obj.note += "{0:%d.%m.%Y}: Änderung BKZ von {1:} zu {2:}.\n".format(
                        date.today(), instance_old.bkz, obj.bkz
                    )
                if obj.recipient != instance_old.recipient:
                    obj.note += "{0:%d.%m.%Y}: Änderung Zahlungspflichtiger von {1:} zu {2:}.\n".format(
                        date.today(), instance_old.recipient, obj.recipient
                    )
                if gross_old != obj.calculate_gross():
                    obj.note += "{0:%d.%m.%Y}: Änderung Betrag von {1:} zu {2:}.\n".format(
                        date.today(), gross_old, obj.calculate_gross()
                    )
                if obj.monition != instance_old.monition:
                    obj.note += "{0:%d.%m.%Y}: Änderung Mahnschlüssel von {1:} zu {2:}.\n".format(
                        date.today(), instance_old.monition, obj.monition
                    )

                file_handle = generate_changeinvoice(obj, instance_old, gross_old)

                # save change pdf
                file_name = obj.get_filename("change_form_file")

                obj.change_form_file.save(file_name, File(file_handle), save=False)

            else:
                # TODO: only raise this when a relevant field is changed, allow changing eg note all the time
                raise ValidationError("Change not implemented for status {}".format(obj.status))

            obj.save()
            formset.save_m2m()

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "recipient":
            kwargs["queryset"] = Recipient.objects.order_by(Lower("name"))
        if db_field.name == "general_ledger_account":
            kwargs["queryset"] = GeneralLedgerAccount.objects.order_by("number")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_name(self, obj):
        return str(obj)

    def gross_total(self, obj):
        return obj.calculate_gross()
