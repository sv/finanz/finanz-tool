from django import forms
from django.contrib import admin

from finanztool.core.admin.booking import BookingAdmin
from finanztool.core.admin.util import ActionButtonMixin
from finanztool.core.models import AnnualPlan, CashSettlement, CashTransfer


class CashTransferInlineForm(forms.ModelForm):
    class Meta:
        model = CashTransfer
        exclude = []

    def __init__(self, *args, **kwargs):
        if "instance" not in kwargs:
            # auto increment booking number and set annual year for cashtransfer
            initial = kwargs.get("initial", {})
            if CashTransfer.objects.order_by("-annual_plan__year", "-internal_booking_number").count() != 0:
                # Prefills the next higher invoice number from the invoice with the newest booking year
                initial["internal_booking_number"] = str(
                    int(
                        CashTransfer.objects.order_by("-annual_plan__year", "-internal_booking_number")[
                            0
                        ].internal_booking_number
                    )
                    + 1
                )
                # If there are already invoices booked in the next year, set the annual_plan default to next year
                initial["annual_plan"] = CashTransfer.objects.order_by(
                    "-annual_plan__year", "-internal_booking_number"
                )[0].annual_plan
            else:
                # default value 1 and last annual plan that was created if no bookings are present
                initial["internal_booking_number"] = "001"
                initial["annual_plan"] = AnnualPlan.objects.last()
            kwargs["initial"] = initial

        super().__init__(*args, **kwargs)


class CashTransferInlineAdmin(admin.StackedInline):
    model = CashTransfer
    form = CashTransferInlineForm
    extra = 0

    autocomplete_fields = [
        "requester",
    ]
    readonly_fields = ["form_file"]


def generate_form(modeladmin, request, queryset):
    del modeladmin, request  # unused
    for booking in queryset:
        booking.generate_form()


generate_form.short_description = "Formular generieren"
generate_form.allowed_permissions = ("delete",)
generate_form.button_show = True


@admin.register(CashTransfer)
class CashTransferAdmin(ActionButtonMixin, BookingAdmin):
    model = CashTransfer
    autocomplete_fields = [
        "requester",
    ]
    search_fields = ["name", "amount", "requester"]
    readonly_fields = ["form_file"]

    readonly_fields_done = [
        "cashsettlement",
        "amount",
        "transfer_date",
        "signer",
        "total_in_words",
    ]
    actions = [generate_form]


@admin.register(CashSettlement)
class CashSettlementAdmin(admin.ModelAdmin):
    search_fields = [
        "internal_booking_number",
        "name",
    ]
    list_display = ["get_name"]
    filter_horizontal = ["cash_income", "cash_expenses"]

    inlines = [CashTransferInlineAdmin]

    # TODO: add this line when completed print generation
    # readonly_fields = ['payout_instruction_file', 'payin_instruction_file']

    def get_name(self, obj):
        return str(obj)
