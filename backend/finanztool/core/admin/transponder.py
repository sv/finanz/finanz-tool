import io

import fitz
from django.contrib import admin, messages
from django.http import FileResponse
from django.utils.datetime_safe import datetime
from django.utils.text import slugify

from finanztool.core.models import (
    Key,
    KeyGroup,
    LockingAuthorization,
    LockingAuthorizationGroup,
    Person,
    Transponder,
)
from finanztool.pdf.generation.pdf_generation import generate_transponder_contract


def print_contracts(modeladmin, request, queryset):
    doc = None

    for person in queryset:
        if person.transponder_set.count() > 1:
            modeladmin.message_user(
                request,
                person.name + " - Currently only one transponder p.P. is supported for contract generation!",
                messages.ERROR,
            )
            return super(PersonAdmin)
        if person.transponder_set.count() == 0:
            modeladmin.message_user(request, person.name + " - This person has no transponder!", messages.ERROR)
            return super(PersonAdmin)

    for person in queryset:
        file_handle = generate_transponder_contract(person, request)
        if doc is None:
            doc = fitz.open(stream=file_handle, filetype="application/pdf")
        else:
            doc.insert_pdf(fitz.open(stream=file_handle, filetype="application/pdf"))

    return FileResponse(
        io.BytesIO(doc.write()),
        content_type="application/pdf",
        as_attachment=True,
        filename="Vertrag_{}_{}.pdf".format(slugify(person.name), datetime.today().strftime("%Y-%m-%d")),
    )


print_contracts.short_description = "Verträge erstellen und drucken"


@admin.register(Transponder)
class TransponderAdmin(admin.ModelAdmin):
    filter_horizontal = [
        "locking_authorizations",
    ]
    list_filter = ["status"]
    autocomplete_fields = ["carrier"]


@admin.register(LockingAuthorization)
class LockingAuthorizationsAdmin(admin.ModelAdmin):
    list_display = ["lock_id", "name"]
    autocomplete_fields = ["internal_group"]


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    actions = [print_contracts]
    search_fields = ["name", "role"]


@admin.register(Key)
class KeyAdmin(admin.ModelAdmin):
    autocomplete_fields = ["carrier", "group"]


@admin.register(KeyGroup)
class KeyGroupAdmin(admin.ModelAdmin):
    search_fields = ["name"]


@admin.register(LockingAuthorizationGroup)
class LockingAuthorizationGroupAdmin(admin.ModelAdmin):
    search_fields = ["name"]
