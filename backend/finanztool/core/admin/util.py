from django.contrib import admin, messages
from django.contrib.admin.models import CHANGE, LogEntry
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.urls import path, reverse
from django.utils.html import format_html, mark_safe


class ActionButtonMixin:
    """
    Provides model actions as action buttons in the changelist view

    To configure the look of the action buttons, use the following:

      # Show the action button for the action (will be hidden by default)
      action.button_show = True

      # Determine if button should be shown by status
      action.button_show = lambda obj: obj.status == STATUS_NEW

      # Set specific action button label
      action.button_text = "Delete"
    """

    change_list_template = "admin/change_list_action_buttons.html"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(
                "action_button/",
                self.admin_site.admin_view(self._do_action),
                name="{}-action_button".format(self.model.__name__.lower()),
            ),
        ]
        return my_urls + urls

    def _do_action(self, request):
        if request.method == "POST":
            for name in request.POST.keys():
                # search for selected action button in the form action-<action_name>-<pk>
                parts = name.split("-")
                if len(parts) != 3 or parts[0] != "action":
                    continue
                actions = self.get_actions(request)
                if parts[1] not in actions:
                    return HttpResponseForbidden()
                response = actions[parts[1]][0](self, request, self.model.objects.filter(pk=int(parts[2])))
                if response is not None:
                    return response
                break
            return HttpResponseRedirect(reverse("admin:core_{}_changelist".format(self.model.__name__.lower())))
        return HttpResponseForbidden()

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context["action_button_url"] = reverse("admin:{}-action_button".format(self.model.__name__.lower()))
        return super().changelist_view(request, extra_context=extra_context)

    def get_list_display(self, request):
        def actions(obj):
            return mark_safe(
                " ".join(
                    [
                        format_html(
                            '<button style="margin: 0; padding: .4em;" form="action_buttons" class="button" name="action-{}-{}">{}</button>',
                            name,
                            obj.pk,
                            action.button_text if hasattr(action, "button_text") else description,
                        )
                        for action, name, description in self.get_actions(request).values()
                        if hasattr(action, "button_show")
                        and (action.button_show(obj) if callable(action.button_show) else action.button_show)
                    ]
                )
            )

        if not any(
            map(
                lambda action: hasattr(action[0], "button_show") and action[0].button_show,
                self.get_actions(request).values(),
            )
        ):
            # User cannot execute any actions with action buttons. Hide the whole column
            return super().get_list_display(request)
        return list(super().get_list_display(request)) + [actions]


def admin_action_individual_errorhandling(func):
    """
    Wrap an admin action function so that every element in the queryset will be handled
    individually by the given function and ocurring ValidationErrors will be caught and
    presented to the user
    """

    def inner(modeladmin, request, queryset):
        for element in queryset:
            try:
                func(modeladmin, request, (element,))
            except ValidationError as e:
                modeladmin.message_user(
                    request,
                    format_html(
                        'Validierung fehlgeschlagen für <a href="{}">{}</a>: {}',
                        reverse("admin:core_{}_change".format(type(element).__name__.lower()), args=(element.pk,)),
                        str(element),
                        e,
                    ),
                    messages.WARNING,
                )

    # derive function name for original action so django recognizes them as unique
    inner.__name__ = func.__name__
    return inner


@admin_action_individual_errorhandling
def validate_all(modeladmin, request, queryset):
    del modeladmin, request  # unused
    for invoice in queryset:
        invoice.full_clean()


validate_all.short_description = "Daten auf Konsistenz überprüfen"
admin.site.add_action(validate_all)


def log_change(obj, request, change_message):
    LogEntry.objects.log_action(
        user_id=request.user.id,
        content_type_id=ContentType.objects.get_for_model(obj).id,
        object_id=obj.id,
        object_repr=repr(obj),
        action_flag=CHANGE,
        change_message=change_message,
    )
