from datetime import date
from decimal import Decimal

from django.core.exceptions import ValidationError
from django.core.files import File
from django.core.mail import EmailMultiAlternatives
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models.functions import JSONObject
from django.template.loader import render_to_string

from finanztool.core.models.util import AutoFileRenameMixin, RecursiveValueCacheMixin
from finanztool.pdf.generation.pdf_check import validate_file_size
from finanztool.pdf.generation.pdf_generation import generate_direktauftrag

from .booking import Booking
from .requester import Requester

BUDGET_STATUS = (
    ("requested", "Beantragt"),
    ("approved", "Genehmigt"),
    ("done", "Ausgeschöpft"),
    ("closed", "Geschlossen"),
)

EMAIL_INVOICE_TYPE = [
    ("invoice", "Rechnung AStA"),
    ("privatebill", "Privatrechnung"),
    ("asta", "Bestellung durch uns"),
]


class Budget(RecursiveValueCacheMixin, models.Model):
    class Meta:
        verbose_name = "Budget"
        verbose_name_plural = "Budgets"

    parent = models.ForeignKey("Budget", on_delete=models.PROTECT, blank=True, null=True)
    annual_plan = models.ForeignKey(
        "core.AnnualPlan",
        on_delete=models.PROTECT,
        verbose_name="Haushaltsplan",
        default="core.AnnualPlan.get_current",
    )
    requester = models.ForeignKey(Requester, on_delete=models.PROTECT)
    target = models.DecimalField(max_digits=32, decimal_places=2, verbose_name="Zielvorgabe")
    status = models.CharField(choices=BUDGET_STATUS, verbose_name="Status", max_length=16, default="requested")
    note = models.TextField(blank=True, verbose_name="Notiz", help_text="Interne Notiz")
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, verbose_name="Beschreibung")
    date = models.DateField(verbose_name="Erstellungsdatum", auto_now_add=True)

    def clean(self):
        # check that selected requester is in selected annual plan
        errors = []
        if not self.annual_plan.has_receiver(self.requester):
            errors.append(ValidationError("Gewählter interner Empfänger hat kein Budget im gewählten Haushaltsjahr"))

        if self.parent and self.parent.requester != self.requester:
            errors.append(ValidationError("Muss gleichen Requester haben wie Überbudget"))

        # check recursion depth
        obj = self
        for _ in range(10):
            obj = obj.parent
            if not obj:
                break
            if obj.pk == self.pk:
                errors.append(ValidationError("Rekursionsschleife erkannt"))
                break
        else:
            errors.append(ValidationError("Maximale Rekursionstiefe überschritten"))

        if errors:
            raise ValidationError(errors)

    @classmethod
    def annotate_status(cls):
        """
        This method returns a json object that contains the given booking status and a percentage
        that indicates how much it is already processed
        """
        return JSONObject(
            text=models.Case(
                models.When(status="approved", then=models.Value("Budget genehmigt")),
                models.When(status="done", then=models.Value("Budget ausgeschöpft")),
                models.When(status="closed", then=models.Value("Budget geschlossen")),
                models.When(request__za3_requested=True, then=models.Value("Angefragt bei ZA3")),
                models.When(status="requested", then=models.Value("Antrag eingegangen")),
                default=models.Value("Status unbekannt"),
            ),
            progress=models.Case(
                models.When(status="approved", then=models.Value(0.3)),
                models.When(status="done", then=models.Value(0.9)),
                models.When(status="closed", then=models.Value(1.0)),
                models.When(request__za3_requested=True, then=models.Value(0.15)),
                models.When(status="requested", then=models.Value(0.0)),
                default=models.Value(0.0),
            ),
        )

    def __str__(self):
        return f"{self.requester.name} - {self.name}"

    def has_subbudgets(self):
        return Budget.objects.filter(parent=self).exists()

    def has_bookings(self):
        return any(t.objects.filter(budget=self).exists() for t in Booking.types())

    def is_locked(self):
        return self.has_subbudgets() or self.has_bookings()

    def add_note(self, note: str):
        """Adds the given note prepended by the current date as a line to the note field"""
        if self.note != "" and self.note[-1] != "\n":
            self.note += "\n"
        self.note += "{:%d.%m.%Y}: {}".format(date.today(), note)

    def get_children(self):
        yield from Budget.objects.filter(parent=self)
        for b in Booking.types():
            yield from b.objects.filter(budget=self)

    def get_parents(self):
        # pylint:disable=import-outside-toplevel
        from finanztool.core.models import BudgetReceiver

        yield self.parent or BudgetReceiver.objects.get(annual_plan=self.annual_plan, receiver=self.requester)

    def calculate_values(self, cache_obj):
        cache_obj.actual = Decimal(0)
        cache_obj.expected = Decimal(0)
        for obj in self.get_children():
            values = obj.get_values()
            cache_obj.actual += values.actual
            if isinstance(obj, Budget) and obj.get_target_or_none():
                cache_obj.expected += obj.get_target_or_none()
            else:
                cache_obj.expected += values.expected
        if abs(cache_obj.expected) < abs(self.target):
            cache_obj.expected = self.target

    def get_target_or_none(self):
        """Returns the target value for an open budget or None for a done one"""
        if self.status in ("requested", "approved"):
            return self.target
        return None

    def has_request(self):
        """Returns true if the budget has a request"""
        return Request.objects.filter(budget=self).exists()

    @classmethod
    def color(cls):
        return "#8F81EA"


class Request(AutoFileRenameMixin, models.Model):
    """Represents a request to buy/order something."""

    class Meta:
        verbose_name = "Antrag"
        verbose_name_plural = "Anträge"

    budget = models.OneToOneField(Budget, on_delete=models.CASCADE, related_name="request")

    reason = models.TextField(blank=True, verbose_name="Grund")
    denial_reason = models.TextField(
        blank=True, verbose_name="Ablehnungsgrund", help_text="Ablehnungsgrund für die E-Mail Benachrichtigung"
    )

    note = models.TextField(blank=True, verbose_name="Notiz", help_text="Interne Notiz")

    net_single_amount = models.DecimalField(
        max_digits=32,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name="Netto-Einzelpreis",
        validators=[MinValueValidator(0)],
    )

    za3_requested = models.BooleanField(verbose_name="Angefragt bei ZA3", default=False)

    receipt = models.FileField(
        blank=True, null=True, verbose_name="Belegdatei", validators=[validate_file_size], max_length=255
    )

    contract_award_notice_file = models.FileField(blank=True, null=True, verbose_name="Vergabevermerk", max_length=255)

    order_type = models.CharField(
        max_length=64,
        choices=EMAIL_INVOICE_TYPE,
        verbose_name="Bestellungsart",
        default="invoice",
    )

    @classmethod
    def annotate_status(cls):
        """
        This method returns a json object that contains the given booking status and a percentage
        that indicates how much it is already processed
        """
        return JSONObject(
            text=models.Case(
                models.When(budget__status="requested", then=models.Value("Antrag eingegangen")),
                models.When(budget__status="approved", then=models.Value("Budget genehmigt")),
                models.When(budget__status="done", then=models.Value("Budget ausgeschöpft")),
                models.When(budget__status="closed", then=models.Value("Budget geschlossen")),
                default=models.Value("Status unbekannt"),
            ),
            progress=models.Case(
                models.When(budget__status="requested", then=models.Value(0.0)),
                models.When(budget__status="approved", then=models.Value(0.3)),
                models.When(budget__status="done", then=models.Value(0.9)),
                models.When(budget__status="closed", then=models.Value(1.0)),
                default=models.Value(0.0),
            ),
        )

    @classmethod
    def annotate_link(cls, is_superuser: bool):
        if is_superuser:
            return models.functions.Concat(
                models.Value("/admin/core/request/"),
                models.F("pk"),
                models.Value("/change"),
                output_field=models.CharField(),
            )
        return models.Value("")

    @classmethod
    def color(cls):
        return "#B6ACF1"

    def add_note(self, note: str):
        """Adds the given note prepended by the current date as a line to the note field"""
        if self.note != "" and self.note[-1] != "\n":
            self.note += "\n"
        self.note += "{:%d.%m.%Y}: {}".format(date.today(), note)

    def clean(self):
        super().clean()

        if (
            self.net_single_amount
            and self.budget.target
            and self.net_single_amount > Decimal("-1.0") * self.budget.target
        ):
            raise ValidationError("Der Netto-Einzelpreis kann nicht höher als der Auftragspreis sein!")

        if self.za3_requested:
            if self.offer_set.count() < 3:
                raise ValidationError("Es müssen drei Vergleichsangebote eingetragen sein!")

            if self.offer_set.filter(is_desired=True).count() != 1:
                raise ValidationError("Es muss genau ein Angebot als Wunschangebot eingetragen sein!")

            if self.reason == "":
                raise ValidationError("Es muss eine Begründung eingetragen sein!")

    def generate_form(self):
        contract_award_notice = generate_direktauftrag(self)
        self.contract_award_notice_file.save(
            self.get_filename("contract_award_notice_file"),
            File(contract_award_notice),
        )

    def send_status_email(self, status):
        mail_options = {
            "new": ("Finanzanfrage erstellt: {}", "new"),
            "approved": ("Finanzanfrage genehmigt: {}", "approved_{}".format(self.order_type)),
            "declined": ("Finanzanfrage abgelehnt: {}", "denied"),
        }
        if status not in mail_options:
            return
        subject, filename = mail_options[status]
        subject = subject.format(self.budget.name)

        context = {"req": self}
        plaintext = render_to_string("email/request/{}.txt".format(filename), context)
        html = render_to_string("email/request/{}.html".format(filename), context)

        if self.budget.requester.email_address:
            to = [self.budget.requester.email_address]
        else:
            to = [user.email for user in self.budget.requester.user.all() if user.email]
        cc = ("asta-finanz@fs.tum.de",)
        if not to:
            # if no recipient is found, send to cc directly
            to = cc
            cc = None
        msg = EmailMultiAlternatives(
            subject=subject,
            body=plaintext,
            from_email=None,
            cc=cc,
            to=to,
            reply_to=("asta-finanz@fs.tum.de",),
        )
        if self.order_type == "privatebill":
            msg.attach_file(
                "finanztool/core/frontend/email/request/Privatrechnung.dotx",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.template",
            )
        msg.attach_alternative(html, "text/html")
        msg.send()

    # return annual_plan as property for AutoFileRenameMixin
    @property
    def annual_plan(self):
        return self.budget.annual_plan

    def __str__(self):
        return str(self.budget)
