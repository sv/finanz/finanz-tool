from django.db import models

from .recipient import Recipient


class Vendor(Recipient):
    class Meta:
        verbose_name = "Zulieferer"
        verbose_name_plural = "Zulieferer"

    customer_number = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name
