from django.db import models


class LockingAuthorizationGroup(models.Model):
    class Meta:
        verbose_name = "Schließrechtgruppe"
        verbose_name_plural = "Schließrechtgruppen"

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class LockingAuthorization(models.Model):
    class Meta:
        verbose_name = "Schließrecht"
        verbose_name_plural = "Schließrechte"

    lock_id = models.CharField(max_length=255, verbose_name="Schlossnummer", unique=True)
    name = models.CharField(max_length=255, verbose_name="Schlossbezeichnung")
    group = models.CharField(max_length=255, verbose_name="externe Schlossgruppe")
    internal_group = models.ForeignKey(
        LockingAuthorizationGroup,
        on_delete=models.PROTECT,
        related_name="locking_authorizations",
        null=True,
        blank=True,
        verbose_name="interne Schlossgruppe",
    )
    is_public = models.BooleanField(verbose_name="Öffentlich?", default=True)
    note = models.TextField(blank=True, verbose_name="Notiz")

    def __str__(self):
        return "{} - {}".format(self.lock_id, self.name)
