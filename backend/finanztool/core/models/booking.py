from datetime import date
from decimal import Decimal

from django.core.exceptions import ValidationError
from django.core.files import File
from django.db import models
from django.db.models.functions import JSONObject

from finanztool.core.models.amount import Amount
from finanztool.core.models.requester import Requester
from finanztool.core.models.util import AutoFileRenameMixin, RecursiveValueCacheMixin
from finanztool.pdf.generation.pdf_generation import generate_cashtransfer

BOOKING_STATES = [
    ("new", "Neu"),
    ("done", "Erledigt"),
    ("sap_done", "SAP verbucht"),
    ("paid", "Bezahlt"),
    ("cancelled", "Storniert"),
]


class CashSettlement(AutoFileRenameMixin, models.Model):
    class Meta:
        verbose_name = "Barabrechnung"
        verbose_name_plural = "Barabrechnungen"

    name = models.CharField(verbose_name="Name", max_length=255)
    """
        This field contains a date, that helps to assign this event to a certain time period
    """
    beginning_date = models.DateField(
        verbose_name="Veranstaltungsdatum",
        help_text="""Dieses Datum soll die Veranstaltung in einen zeitlichen Rahmen einordnen (z.B. das Datum des Festivalauftakts).
                    Dieses Datum wird nur intern verwendet.""",
    )

    cash_expenses = models.ManyToManyField("core.ReceivedInvoice", blank=True, verbose_name="Barausgaben")

    cash_income = models.ManyToManyField("core.IssuedInvoice", blank=True, verbose_name="Bareinnahmen")

    def __str__(self):
        return "{}_{}".format(
            self.name,
            self.beginning_date,
        )

    @classmethod
    def color(cls):
        return "#000"


_booking_types = []


class Booking(RecursiveValueCacheMixin, models.Model):
    class Meta:
        abstract = True

    date = models.DateField(verbose_name="Erstellungsdatum", auto_now_add=True)
    annual_plan = models.ForeignKey("core.AnnualPlan", on_delete=models.PROTECT, verbose_name="Haushaltsplan")
    internal_booking_number = models.CharField(max_length=255, verbose_name="Interne Buchungsnummer")

    status = models.CharField(
        max_length=64,
        choices=BOOKING_STATES,
        verbose_name="Status (nur intern)",
        default="new",
    )

    name = models.CharField(max_length=255, verbose_name="Name/Titel")

    requester = models.ForeignKey(Requester, on_delete=models.PROTECT, verbose_name="Interner Empfänger")

    booking_date = models.DateField(blank=True, null=True, verbose_name="Buchungsdatum")

    note = models.TextField(blank=True, verbose_name="Notiz (nur intern)")

    budget = models.ForeignKey(
        "Budget",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    @classmethod
    def unique_id_prefix(cls):
        """
        Return the single-letter prefix to use for generating unique ids
        """
        raise NotImplementedError()

    def unique_id(self):
        """
        This method returns a - for all booking types - unique id with following syntax:
        prefix + internal_booking_number + / + last two digits of the year.
        """
        return self.unique_id_prefix() + self.internal_booking_number + "/" + self.annual_plan.year[2:]

    @classmethod
    def annotate_unique_id(cls):
        """
        Generate Django database expression leading to unique_id
        """
        return models.functions.Concat(
            models.Value(cls.unique_id_prefix()),
            "internal_booking_number",
            models.Value("/"),
            models.functions.Substr("annual_plan__year", 3),
        )

    def display_name(self):
        """
        This method returns the name of the booking that should be displayed in the GUI
        """
        return self.name

    @classmethod
    def annotate_display_name(cls):
        """
        Generate Django database expression leading to display name
        """
        return models.F("name")

    @classmethod
    def annotate_status(cls):
        """
        This method returns a json object that contains the given booking status and a percentage
        that indicates how much it is already processed
        """
        return JSONObject(
            text=models.Case(
                models.When(status="new", then=models.Value("Buchung eingegangen")),
                models.When(status="done", then=models.Value("Buchung bearbeitet")),
                models.When(status="sap_done", then=models.Value("Gebucht")),
                models.When(status="paid", then=models.Value("Bezahlt")),
                models.When(status="cancelled", then=models.Value("Storniert")),
                default=models.Value("Status unbekannt"),
            ),
            progress=models.Case(
                models.When(status="new", then=models.Value(0.5)),
                models.When(status="done", then=models.Value(0.7)),
                models.When(status="sap_done", then=models.Value(0.85)),
                models.When(status="paid", then=models.Value(1.0)),
                models.When(status="cancelled", then=models.Value(1.0)),
                default=models.Value(0.0),
            ),
        )

    def display_booking_date(self):
        """
        This method returns the booking date (or the current date as default)
        """
        return self.booking_date or date.today()

    def folder(self):
        """
        This method returns the foldername in which the files should be saved
        """
        raise NotImplementedError()

    def link_to_file(self):
        """
        This method returns the link to the (invoice) file of the booking
        """
        return None

    def calculate_booked_total(self):
        """
        This method returns the total booked sum (net in case of the event fond, gross otherwise).
        Should return 0 for cancelled bookings.
        """
        raise NotImplementedError()

    @classmethod
    def annotate_booked_total(cls):
        """
        This method returns a django database expression leading to the total booked sum
        """
        raise NotImplementedError()

    @classmethod
    def annotate_link(cls, is_superuser: bool, class_type: str):
        """
        This method returns the link to the respective admin page if superuser
        or link to the invoice file if not
        """
        if is_superuser:
            return models.functions.Concat(
                models.Value("/admin/core/"),
                models.Value(class_type + "/"),
                models.F("pk"),
                models.Value("/change"),
                output_field=models.CharField(),
            )
        return models.Value("")

    def compare_status(self, other):
        states = [x[0] for x in BOOKING_STATES]
        return states.index(self.status) - states.index(other)

    @classmethod
    def statuses_geq(cls, status):
        states = [x[0] for x in BOOKING_STATES]
        return states[states.index(status) :]

    def clean(self):
        # Add leading zeros if not already there
        if len(self.internal_booking_number) < 3:
            self.internal_booking_number = "0" * (3 - len(self.internal_booking_number)) + self.internal_booking_number
        # pylint: disable=assignment-from-no-return
        cleaned = super().clean()
        errors = []

        # check that selected requester is in selected annual plan
        if not self.annual_plan.has_receiver(self.requester):
            errors.append(ValidationError("Gewählter interner Empfänger hat kein Budget im gewählten Haushaltsjahr"))

        if self.budget:
            if self.annual_plan != self.budget.annual_plan:  # pylint: disable=no-member
                errors.append(ValidationError("Jahresplan muss mit gewähltem Budget übereinstimmen"))
            if self.requester != self.budget.requester:  # pylint: disable=no-member
                errors.append(ValidationError("Interner Empfänger muss mit gewähltem Budget übereinstimmen"))

        if self.compare_status("done") >= 0:
            if not self.booking_date:
                errors.append(ValidationError('Buchungsdatum muss ab Status "Erledigt" gesetzt sein!'))
            if self.booking_date and str(self.booking_date.year) != self.annual_plan.year:  # pylint: disable=E1101
                errors.append(ValidationError("Buchungsdatum liegt außerhalb des Geschäftsjahrs"))

        if errors:
            raise ValidationError(errors)
        return cleaned

    def get_children(self):
        pass

    def get_parents(self):
        # pylint:disable=import-outside-toplevel
        from finanztool.core.models import BudgetReceiver

        yield self.budget or BudgetReceiver.objects.get(annual_plan=self.annual_plan, receiver=self.requester)

    def calculate_values(self, cache_obj):
        cache_obj.expected = self.calculate_booked_total()
        if self.compare_status("done") <= 0:
            cache_obj.actual = Decimal(0)
        else:
            cache_obj.actual = cache_obj.expected

    @classmethod
    def create_booking_id(cls):
        # pylint: disable=import-outside-toplevel
        # circular import avoidance
        from finanztool.core.models import AnnualPlan

        num = "001"
        annual_plan = AnnualPlan.get_current()
        latest_booking = cls.objects.filter(annual_plan=annual_plan).order_by("-internal_booking_number")
        if latest_booking.count() != 0:
            # Prefills the next higher invoice number from the invoice with the newest booking year
            num = str(int(latest_booking[0].internal_booking_number) + 1)

        return num, annual_plan

    def add_note(self, note):
        """Adds the given note prepended by the current date as a line to the note field"""
        if self.note != "" and self.note[-1] != "\n":
            self.note += "\n"
        self.note += "{:%d.%m.%Y}: {}".format(date.today(), note)

    @staticmethod
    def register(subclass):
        """Decorator registers class as subclass of booking"""
        _booking_types.append(subclass)
        return subclass

    @staticmethod
    def types():
        """Get list of all booking types"""
        return _booking_types


@Booking.register
class CashTransfer(AutoFileRenameMixin, Booking):
    class Meta:
        verbose_name = "Barabschlag"
        verbose_name_plural = "Barabschläge"
        unique_together = ("annual_plan", "internal_booking_number")

    cashsettlement = models.ForeignKey(
        CashSettlement, on_delete=models.PROTECT, blank=True, null=True, verbose_name="Barabrechnung"
    )

    """
        This field indicates the amount of the cashtransfer. Positive sign means it is deposited.
    """
    amount = models.DecimalField(
        max_digits=21,
        decimal_places=2,
        verbose_name="Ein(Aus)zahlungsbetrag",
        help_text="Dieses Feld gibt den Einzahlungsbetrag (positives Vorzeichen) bzw. Auszahlungsbetrag (negatives Vorzeichen) des Barabschlags an.",
    )

    total_in_words = models.CharField(
        max_length=255,
        blank=True,
        verbose_name="Ein(Aus)zahlungsbetrag in Worten",
        help_text="In diesem Feld bitte bei Ein- und Auszahlungen den positiven Wert in Worten angeben.",
    )

    transfer_date = models.TextField(
        verbose_name="Abschlags(Event)datum",
        # this field length is limited due to the EDVBK
        max_length=27,
        help_text="""Gib bei einer Einzahlung die Daten der zugehörigen Auszahlungen an, bei einer Auszahlung das Datum des Events.
            (Bei einer Einzahlung, die mit Auszahlungen am 06.03.2000 und am 12.03.2000 zusammenhängt, soll '06.03.2000, 12.03.2000' eingegeben werden.""",
    )

    signer = models.CharField(max_length=2, blank=True, verbose_name="Finanzreferentenkürzel")

    form_file = models.FileField(blank=True, verbose_name="Anweisungsformular", max_length=255)

    @classmethod
    def unique_id_prefix(cls):
        return "B"

    def calculate_booked_total(self):
        return self.amount

    @classmethod
    def annotate_booked_total(cls):
        return models.F("amount")

    def display_name(self):
        return "{}_{}".format(self.cashsettlement.name, self.name)

    @classmethod
    def annotate_display_name(cls):
        return models.functions.Concat("cashsettlement__name", models.Value("_"), "name")

    def generate_form(self):
        file_handle = generate_cashtransfer(self)

        self.form_file.save(
            self.get_filename("form_file"),
            File(file_handle),
        )

    def __str__(self):
        return "{}_{}_{}".format(
            self.unique_id(),
            self.cashsettlement.name,
            self.name,
        )

    def clean(self):
        cleaned_data = super().clean()
        errors = []
        if self.total_in_words == "" and abs(self.amount) >= Decimal("1000.00"):
            errors.append(ValidationError("Betrag in Worten ab 1000€ benötigt"))
        if self.total_in_words != "" and self.total_in_words != Amount.calculate_total_in_words(abs(self.amount)):
            errors.append(
                ValidationError(
                    "Betrag in Worten passt nicht zum Gesamtbetrag. Erwartet: {}".format(
                        Amount.calculate_total_in_words(abs(self.amount))
                    )
                )
            )
        if errors:
            raise ValidationError(errors)
        return cleaned_data


@Booking.register
class ZeroSumBooking(AutoFileRenameMixin, Booking):
    class Meta:
        verbose_name = "Nullsummenbuchung"
        verbose_name_plural = "Nullsummenbuchungen"
        unique_together = ("annual_plan", "internal_booking_number")

    data_file = models.FileField(blank=True, verbose_name="Rechnungen/Informationen", max_length=255)

    @classmethod
    def unique_id_prefix(cls):
        return "N"

    def __str__(self):
        return "{}_{}_{}".format(self.internal_booking_number, self.requester.name, self.name)

    def calculate_booked_total(self):
        return Decimal("0.00")

    @classmethod
    def annotate_booked_total(cls):
        return models.Value(Decimal("0.00"))

    def link_to_file(self):
        return self.data_file.url if self.data_file else ""


@Booking.register
class InternalBooking(Booking):
    class Meta:
        verbose_name = "Interne Buchung"
        verbose_name_plural = "Interne Buchungen"
        unique_together = ("annual_plan", "internal_booking_number")

    amount = models.DecimalField(max_digits=21, decimal_places=2, verbose_name="Betrag")
    total_in_words = models.CharField(
        max_length=255,
        blank=True,
        verbose_name="Umbuchungsbetrag in Worten",
        help_text="In diesem Feld bitte bei Ein- und Auszahlungen den Wert in Worten angeben.",
    )

    sender = models.ForeignKey(Requester, on_delete=models.PROTECT, verbose_name="Sender", related_name="sender")

    def clean(self):
        cleaned_data = super().clean()
        errors = []

        if self.requester.fond != self.sender.fond:
            errors.append(ValidationError("Es kann nur im gleichen Fond gebucht werden!"))
        if self.amount < Decimal("0.0"):
            errors.append(ValidationError('Betrag darf ab Status "Erledigt" nicht kleiner 0 sein!'))
        if self.total_in_words == "" and abs(self.amount) >= Decimal("1000.00"):
            errors.append(ValidationError("Betrag in Worten ab 1000€ benötigt"))
        if self.total_in_words != "" and self.total_in_words != Amount.calculate_total_in_words(self.amount):
            errors.append(
                ValidationError(
                    "Betrag in Worten passt nicht zum Gesamtbetrag. Erwartet: {}".format(
                        Amount.calculate_total_in_words(self.amount)
                    )
                )
            )
        if errors:
            raise ValidationError(errors)
        return cleaned_data

    @classmethod
    def unique_id_prefix(cls):
        return "I"

    def display_name_outgoing(self):
        return "nach " + self.requester.name + " - " + self.name

    @classmethod
    def annotate_display_name_outgoing(cls):
        return models.functions.Concat(models.Value("nach "), "requester__name", models.Value(" - "), "name")

    def display_name(self):
        return "von " + self.sender.name + " - " + self.name

    @classmethod
    def annotate_display_name(cls):
        return models.functions.Concat(models.Value("von "), "sender__name", models.Value(" - "), "name")

    def calculate_booked_total_outgoing(self):
        if self.status == "cancelled":
            return Decimal("0.00")
        return Decimal(-1) * self.amount

    def calculate_booked_total(self):
        if self.status == "cancelled":
            return Decimal("0.00")
        return self.amount

    @classmethod
    def annotate_booked_total(cls):
        return models.Case(
            models.When(status="cancelled", then=models.Value(Decimal("0.00"))), default=models.F("amount")
        )

    def get_parents(self):
        yield from super().get_parents()
        # pylint:disable=import-outside-toplevel
        from finanztool.core.models import BudgetReceiver

        yield self.budget or BudgetReceiver.objects.get(annual_plan=self.annual_plan, receiver=self.sender)

    def __str__(self):
        return self.internal_booking_number + "_" + self.name
