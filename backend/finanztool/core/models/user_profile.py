from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    class Meta:
        verbose_name = "Profildaten"
        verbose_name_plural = "Profildaten"

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    sso_username = models.CharField(
        verbose_name="SSO-Kennung", help_text="Nutzername für SSO-Provider (AStA-Kennung oder LRZ-ID)", max_length=64
    )
