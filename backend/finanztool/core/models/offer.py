from django.core.validators import MinValueValidator
from django.db import models
from django.utils.text import slugify

from finanztool.core.models.util import AutoFileRenameMixin
from finanztool.pdf.generation import pdf_check

from .budget import Request


class Offer(AutoFileRenameMixin, models.Model):
    bidder = models.CharField(max_length=255, verbose_name="Bieter")
    net = models.DecimalField(
        max_digits=21, decimal_places=2, verbose_name="Nettopreis", validators=[MinValueValidator(0)]
    )
    offer_file = models.FileField(
        verbose_name="Angebot", validators=[pdf_check.validate_file_size, pdf_check.is_pdf], max_length=255
    )

    def offer_file_path(self):
        return f"{self.request.budget.annual_plan.year}/request/receipt/{slugify(self.request.budget.requester.name + ' ' + self.request.budget.name + ' ' + self.bidder)}.pdf"

    is_desired = models.BooleanField(verbose_name="Wunschangebot?")
    request = models.ForeignKey(Request, on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {:_>9}€".format(self.bidder, self.net)
