from django.core.exceptions import ValidationError
from django.db import models

from .locking_authorization import LockingAuthorization
from .person import Person

TRANSPONDER_STATES = [
    # Do not change the left side of the tuple!
    ("vault", "Tresor"),
    ("sent", "Ausgegeben"),
]


def validate_transponder_id(value: str):
    if not isinstance(value, str):
        raise ValidationError("Transpondernummer ist kein string")
    if "O" in value:
        raise ValidationError('Transpondernummer enthält "O", meintest du "0"?')
    if not value.isalnum():
        raise ValidationError("Transpondernummer enthält Sonderzeichen")
    if not value.isupper():
        raise ValidationError("Transpondernummer enthält Kleinbuchstaben")


class Transponder(models.Model):
    class Meta:
        verbose_name = "Transponder"
        verbose_name_plural = "Transponder"

    transponder_id = models.CharField(
        max_length=10, verbose_name="Transpondernummer", validators=[validate_transponder_id]
    )

    carrier = models.ForeignKey(Person, on_delete=models.PROTECT, null=True, blank=True, verbose_name="Besitzer")

    locking_authorizations = models.ManyToManyField(LockingAuthorization, blank=True, verbose_name="Schließrechte")

    status = models.CharField(
        max_length=64, choices=TRANSPONDER_STATES, verbose_name="Status (nur intern)", default="vault"
    )

    def __str__(self):
        return "{}{}".format(self.transponder_id, " - " + self.carrier.name if self.carrier else "")

    def save(self, *args, **kwargs):
        if self.carrier is not None:
            self.status = "sent"
        super().save()

    def clean(self):
        super().clean()
        if self.status == "sent" and self.carrier is None:
            raise ValidationError("Es muss ein Besitzer eingetragen sein, wenn der Status auf ausgegeben ist!")
