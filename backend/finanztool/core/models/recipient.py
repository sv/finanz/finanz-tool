import string

from django.core.exceptions import ValidationError
from django.db import models

LETTERS = {ord(d): str(i) for i, d in enumerate(string.digits + string.ascii_uppercase)}


def _number_iban(iban):
    return (iban[4:] + iban[:4]).replace(" ", "").translate(LETTERS)


def validate_iban(iban):
    try:
        iban_num = int(_number_iban(iban))
    except:
        raise ValidationError("Not a valid IBAN")
    if iban_num % 97 != 1:
        raise ValidationError("IBAN Checksum invalid")


class Recipient(models.Model):
    class Meta:
        verbose_name = "Rechnungsempfänger"
        verbose_name_plural = "Rechnungsempfänger"

    salutation = models.CharField(max_length=255, verbose_name="Anrede", blank=True)
    name = models.CharField(max_length=255)

    street = models.CharField(max_length=255, blank=True)
    post_place = models.CharField(max_length=255, blank=True)
    country_code = models.CharField(max_length=2, blank=True)

    iban = models.CharField(max_length=255, validators=[validate_iban])
    bic = models.CharField(max_length=255)
    bankname = models.CharField(max_length=255)

    intern = models.BooleanField(default=False)

    def __str__(self):
        return self.name
