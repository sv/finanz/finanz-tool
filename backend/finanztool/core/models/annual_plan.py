from datetime import date
from decimal import Decimal

from django.db import models

from finanztool.core.models.util import RecursiveValueCacheMixin

from .booking import Booking, InternalBooking
from .budget import Budget
from .requester import Requester


class AnnualPlan(models.Model):
    class Meta:
        verbose_name = "Jahresplan"
        verbose_name_plural = "Jahrespläne"

    year = models.CharField(max_length=4, unique=True)
    budget_allocation = models.DecimalField(max_digits=21, decimal_places=2, default=0)

    def has_receiver(self, receiver: Requester):
        "Checks whether the given receiver has a budget in this annual plan"
        return BudgetReceiver.objects.filter(annual_plan=self, receiver=receiver)

    @classmethod
    def get_current(cls):
        # try to use current year, fall back to latest annual plan
        try:
            return cls.objects.get(year=str(date.today().year))
        except cls.DoesNotExist:
            return cls.objects.last()

    def __str__(self):
        return self.year


class BudgetReceiver(RecursiveValueCacheMixin, models.Model):
    class Meta:
        verbose_name = "Budgetempfänger - Haushalt"
        verbose_name_plural = "Budgetempfänger - Haushalt"
        unique_together = ("annual_plan", "receiver")
        ordering = ["display_order"]

    target = models.DecimalField(max_digits=21, decimal_places=2, verbose_name="Geplanter Saldo")
    receiver = models.ForeignKey(Requester, on_delete=models.PROTECT, verbose_name="Empfänger")

    annual_plan = models.ForeignKey(AnnualPlan, on_delete=models.PROTECT)

    display_order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def calc_current_balance(self):
        balance = 0

        for internal_booking in InternalBooking.objects.filter(
            sender=self.receiver, annual_plan=self.annual_plan
        ).all():
            balance -= internal_booking.calculate_booked_total()

        for t in Booking.types():
            for booking in t.objects.filter(requester=self.receiver, annual_plan=self.annual_plan).all():
                balance += booking.calculate_booked_total()

        return balance

    def remaining_budget(self):
        return self.calc_current_balance() - self.target

    def get_children(self):
        yield from Budget.objects.filter(parent=None, annual_plan=self.annual_plan, requester=self.receiver).only("pk")
        for b in Booking.types():
            yield from b.objects.filter(budget=None, annual_plan=self.annual_plan, requester=self.receiver).only("pk")
        for obj in InternalBooking.objects.filter(annual_plan=self.annual_plan, sender=self.receiver).only("pk"):
            obj._neg = True  # pylint: disable=protected-access
            yield obj

    def get_parents(self):
        return
        yield

    def calculate_values(self, cache_obj):
        cache_obj.actual = Decimal(0)
        cache_obj.expected = Decimal(0)
        for obj in self.get_children():
            values = obj.get_values()
            factor = 1
            if isinstance(obj, InternalBooking) and hasattr(obj, "_neg"):
                factor = -1
            cache_obj.actual += factor * values.actual
            if isinstance(obj, Budget) and obj.status in ("requested", "approved"):
                cache_obj.expected += obj.target
            else:
                cache_obj.expected += factor * values.expected

    def __str__(self):
        return "{}".format(self.receiver.name)
