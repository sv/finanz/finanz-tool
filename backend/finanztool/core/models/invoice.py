from decimal import Decimal

from django.core.exceptions import ValidationError
from django.core.files import File
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.functions import Coalesce, Concat, JSONObject

from finanztool.core.models.booking import Booking
from finanztool.core.models.util import AutoFileRenameMixin
from finanztool.pdf.generation.pdf_generation import (
    generate_issuedinvoice,
    generate_receivedinvoice,
)

from .booking import Booking
from .fond import GROSS, NET
from .general_ledger_account import GeneralLedgerAccount
from .recipient import Recipient
from .vendor import Vendor

# order is used in compare_status below for a logical flow order
INVOICE_STATES = [
    ("new", "Neu"),
    ("payable", "Rechnung zahlbar"),
    ("done", "Erledigt"),
    ("sent", "Versandt"),
    ("sap_done", "SAP verbucht"),
    ("paid", "Bezahlt"),
    ("cancelled", "Storniert"),
]

MONITION_STATES = [
    ("23", "keine Mahnung"),
    ("25", "Mahnung (ohne Gebühren)"),
    ("27", "Mahnung (Gebühren)"),
]


class Invoice(AutoFileRenameMixin, Booking):
    class Meta:
        abstract = True

    invoice_file = models.FileField(blank=True, verbose_name="Rechnungsdatei", max_length=255)

    form_file = models.FileField(blank=True, verbose_name="Anweisungsformular", max_length=255)

    change_form_file = models.FileField(blank=True, verbose_name="Änderungsformular", max_length=255)

    general_ledger_account = models.ForeignKey(GeneralLedgerAccount, on_delete=models.PROTECT, verbose_name="Sachkonto")

    reason = models.TextField(blank=True, verbose_name="Begründung")

    signer = models.CharField(max_length=2, blank=True, verbose_name="Finanzreferentenkürzel")

    status = models.CharField(
        max_length=64,
        choices=INVOICE_STATES,
        verbose_name="Status (nur intern)",
        default="new",
    )

    total_in_words = models.CharField(max_length=255, blank=True, verbose_name="Gesamtbetrag in Worten")

    def calculate_gross(self):
        pass

    @classmethod
    def color(cls):
        return "#F9BF4E"

    @classmethod
    def annotate_gross(cls):
        pass

    def calculate_net(self):
        pass

    @classmethod
    def annotate_net(cls):
        pass

    def calculate_booked_total(self):
        if self.status == "cancelled":
            return Decimal("0.00")
        booking_type = self.requester.fond.booking_type
        if booking_type == GROSS:
            return self.calculate_gross()
        if booking_type == NET:
            return self.calculate_net()
        raise RuntimeError("Unsupported booking type: {}".format(booking_type))

    @classmethod
    def annotate_booked_total(cls):
        return models.Case(
            models.When(status="cancelled", then=models.Value(Decimal("0.00"))),
            models.When(requester__fond__booking_type=GROSS, then=cls.annotate_gross()),
            models.When(requester__fond__booking_type=NET, then=cls.annotate_net()),
        )

    def display_booking_date(self):
        return self.booking_date or self.date

    @classmethod
    def annotate_status(cls):
        """
        This method returns a json object that contains the given booking status and a percentage
        that indicates how much it is already processed
        """
        return JSONObject(
            text=models.Case(
                models.When(status="new", then=models.Value("Rechnung eingegangen")),
                models.When(status="payable", then=models.Value("Rechnung bezahlbar")),
                models.When(status="done", then=models.Value("Rechnung bearbeitet")),
                models.When(status="sap_done", then=models.Value("Gebucht")),
                models.When(status="sent", then=models.Value("Rechnung versandt an ZA3")),
                models.When(status="paid", then=models.Value("Bezahlt")),
                models.When(status="cancelled", then=models.Value("Storniert")),
                default=models.Value("Status unbekannt"),
            ),
            progress=models.Case(
                models.When(status="new", then=models.Value(0.5)),
                models.When(status="payable", then=models.Value(0.6)),
                models.When(status="done", then=models.Value(0.7)),
                models.When(status="sent", then=models.Value(0.8)),
                models.When(status="sap_done", then=models.Value(0.9)),
                models.When(status="paid", then=models.Value(1.0)),
                models.When(status="cancelled", then=models.Value(1.0)),
                default=models.Value(0.0),
            ),
        )

    def is_cancelled(self):
        return self.status == "cancelled"

    def link_to_file(self):
        return self.invoice_file.url if self.invoice_file else None

    def compare_status(self, other):
        states = [x[0] for x in INVOICE_STATES]
        return states.index(self.status) - states.index(other)

    @classmethod
    def annotate_link(cls, is_superuser: bool, class_type: str):
        """
        This method returns the link to the respective admin page if superuser
        or link to the invoice file if not
        """
        if is_superuser:
            return models.functions.Concat(
                models.Value("/admin/core/"),
                models.Value(class_type + "/"),
                models.F("pk"),
                models.Value("/change"),
                output_field=models.CharField(),
            )
        return models.functions.Concat(
            models.Value("/media/"), models.F("invoice_file"), output_field=models.CharField()
        )

    @classmethod
    def statuses_geq(cls, status):
        states = [x[0] for x in INVOICE_STATES]
        return states[states.index(status) :]

    def clean(self):
        cleaned = super().clean()
        errors = []

        if self.compare_status("done") >= 0:
            if not self.signer:
                errors.append(ValidationError('Finanzreferentenkürzel muss ab Status "Erledigt" gesetzt sein!'))
            if not self.invoice_file:
                errors.append(ValidationError('Rechnungsdatei muss ab Status "Erledigt" hochgeladen sein!'))
        if errors:
            raise ValidationError(errors)
        return cleaned


@Booking.register
class ReceivedInvoice(Invoice):
    class Meta:
        verbose_name = "Eingehende Rechnung"
        verbose_name_plural = "Eingehende Rechnungen"
        unique_together = (
            ("vendor", "vendor_invoice_number"),
            ("annual_plan", "internal_booking_number"),
        )

    vendor = models.ForeignKey(Vendor, on_delete=models.PROTECT, parent_link=True, verbose_name="Rechnungssteller")
    vendor_invoice_number = models.CharField(max_length=255, verbose_name="Rechnungsnummer")
    reference = models.CharField(max_length=255, verbose_name="Verwendungszweck")

    electronic_invoice = models.BooleanField(null=True, verbose_name="Elektronische Rechnung")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if hasattr(self, "request"):
            self.previous_request = self.request

    @classmethod
    def unique_id_prefix(cls):
        return ""

    def folder(self):
        return "received"

    def display_name(self):
        return self.vendor.name + " - " + self.name

    @classmethod
    def annotate_display_name(cls):
        return Concat("vendor__name", models.Value(" - "), "name")

    def calculate_gross(self):
        return self.receivedamount_set.aggregate(sum=Coalesce(models.Sum("gross"), models.Value(Decimal("0.00"))))[
            "sum"
        ]

    @classmethod
    def annotate_gross(cls):
        return Coalesce(models.Sum("receivedamount__gross"), models.Value(Decimal("0.00")))

    def calculate_net(self):
        return self.receivedamount_set.aggregate(sum=Coalesce(models.Sum("net"), models.Value(Decimal("0.00"))))["sum"]

    @classmethod
    def annotate_net(cls):
        return Coalesce(models.Sum("receivedamount__net"), models.Value(Decimal("0.00")))

    def calculate_booked_total(self):
        # Incoming invoices are negative
        return -super().calculate_booked_total()

    @classmethod
    def annotate_booked_total(cls):
        return -super().annotate_booked_total()

    def generate_form(self):
        file_handle = generate_receivedinvoice(self)

        self.form_file.save(
            self.get_filename("form_file"),
            File(file_handle),
        )

        if hasattr(self, "inventory_set"):
            for inventory in self.inventory_set.all():
                inventory.generate_form()

    def save(self, *args, **kwargs):
        # TODO: Handle saving (model has to be saved twice until formset is up to date)
        # TODO: Update assigned budget
        super().save(*args, **kwargs)

    def __str__(self):
        return "{}_{}_{}_{}_{}".format(
            self.internal_booking_number,
            "V" if self.requester.fond.name == "Veranstaltungsfond" else "H",
            self.requester.name,
            self.vendor.name,
            self.name,
        )


@Booking.register
class IssuedInvoice(Invoice):
    class Meta:
        verbose_name = "Ausgestellte Rechnung"
        verbose_name_plural = "Ausgestellte Rechnungen"
        unique_together = ("annual_plan", "internal_booking_number")

    bkz = models.CharField(
        max_length=255,
        unique=True,
        verbose_name="BKZ",
        validators=[
            RegexValidator(regex=r"^[0-9]{4}\.[0-9]{4}\.[0-9]{4}$", message="BKZ has to be in format XXXX.XXXX.XXXX")
        ],
    )

    monition = models.CharField(
        max_length=64,
        choices=MONITION_STATES,
        verbose_name="Mahnschlüssel",
        default="25",
    )

    recipient = models.ForeignKey(Recipient, on_delete=models.PROTECT, verbose_name="Rechnungsempfänger")

    @classmethod
    def unique_id_prefix(cls):
        return "E"

    def folder(self):
        return "issued"

    def display_name(self):
        return self.recipient.name + " - " + self.name

    @classmethod
    def annotate_display_name(cls):
        return Concat("recipient__name", models.Value(" - "), "name")

    def calculate_gross(self):
        return self.issuedamount_set.aggregate(sum=Coalesce(models.Sum("gross"), models.Value(Decimal("0.00"))))["sum"]

    @classmethod
    def annotate_gross(cls):
        return Coalesce(models.Sum("issuedamount__gross"), models.Value(Decimal("0.00")))

    def calculate_net(self):
        return self.issuedamount_set.aggregate(sum=Coalesce(models.Sum("net"), models.Value(Decimal("0.00"))))["sum"]

    @classmethod
    def annotate_net(cls):
        return Coalesce(models.Sum("issuedamount__net"), models.Value(Decimal("0.00")))

    def calculate_tax(self):
        amount_sum = Decimal("0")

        for amount in self.issuedamount_set.all():
            amount_sum += amount.calc_tax_amount()
        return amount_sum

    def generate_form(self):
        file_handle = generate_issuedinvoice(self)

        self.form_file.save(
            self.get_filename("form_file"),
            File(file_handle),
        )

    def __str__(self):
        return "{}_{}_{}_{}_{}".format(
            self.internal_booking_number,
            "V" if self.requester.fond.name == "Veranstaltungsfond" else "H",
            self.requester.name,
            self.recipient.name,
            self.name,
        )
