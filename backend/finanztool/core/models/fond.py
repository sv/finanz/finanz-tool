from django.db import models

NET = 1
GROSS = 2

# don't change (used as magic numbers in templates)
BOOKING_TYPE = (
    (NET, "Net"),
    (GROSS, "Gross"),
)


class Fond(models.Model):
    name = models.CharField(max_length=255)

    number = models.CharField(max_length=255)

    booking_type = models.IntegerField(choices=BOOKING_TYPE)

    def __str__(self):
        return self.name
