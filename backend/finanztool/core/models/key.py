from django.core.exceptions import ValidationError
from django.db import models

from .person import Person

KEY_STATES = [
    # Do not change the left side of the tuple!
    ("vault", "Tresor"),
    ("sent", "Ausgegeben"),
]


class KeyGroup(models.Model):
    class Meta:
        verbose_name = "Schlüsselgruppe"
        verbose_name_plural = "Schlüsselgruppen"

    name = models.CharField(max_length=255)

    description = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Key(models.Model):
    class Meta:
        verbose_name = "Schlüssel"
        verbose_name_plural = "Schlüssel"
        unique_together = ("number", "group")

    number = models.CharField(max_length=10)

    carrier = models.ForeignKey(Person, related_name="keys", on_delete=models.PROTECT, null=True, blank=True)

    group = models.ForeignKey(KeyGroup, on_delete=models.PROTECT)

    status = models.CharField(max_length=64, choices=KEY_STATES, verbose_name="Status (nur intern)", default="vault")

    def __str__(self):
        return "{} - {}{}".format(self.group.name, self.number, " - " + self.carrier.name if self.carrier else "")

    def save(self, *args, **kwargs):
        if self.carrier is not None:
            self.status = "sent"
        super().save()

    def clean(self):
        super().clean()
        if self.status == "sent" and self.carrier is None:
            raise ValidationError("Es muss ein Besitzer eingetragen sein, wenn der Status auf ausgegeben ist!")
