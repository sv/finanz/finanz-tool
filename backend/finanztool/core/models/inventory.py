from django.core.files import File
from django.db import models
from django.utils.text import slugify

from finanztool.core.models.invoice import ReceivedInvoice
from finanztool.core.models.util import AutoFileRenameMixin
from finanztool.pdf.generation.pdf_generation import generate_inventory_letter


class Inventory(AutoFileRenameMixin, models.Model):
    class Meta:
        verbose_name = "Inventarinformation"
        verbose_name_plural = "Inventarinformationen"

    dfg_key = models.CharField(max_length=255, verbose_name="DFG-Gerätegruppenschlüssel")
    contact = models.CharField(max_length=255, verbose_name="Ansprechpartner (Name)")
    contact_mail = models.CharField(max_length=255, verbose_name="Kontaktemailadresse")
    location = models.CharField(
        max_length=255,
        verbose_name="Standort",
        help_text="Bitte gib hier die Architektenraumnummer an (Bsp.: 0605@1200).",
    )
    invoice = models.ForeignKey(ReceivedInvoice, on_delete=models.CASCADE)
    generated_file = models.FileField(blank=True, verbose_name="Anlagenbuchhaltungsbrief", max_length=255)

    def generated_file_path(self):
        return f"{self.invoice.annual_plan.year}/receivedinvoice/inventory_information/{slugify(str(self).replace('/','_'))}.pdf"

    def __str__(self):
        return "{}_{}".format(self.invoice.unique_id(), self.contact)

    def generate_form(self):
        file_handle = generate_inventory_letter(self)

        self.generated_file.save(
            self.get_filename("generated_file"),
            File(file_handle),
        )
