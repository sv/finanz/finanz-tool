from django.db import models


class GeneralLedgerAccount(models.Model):
    number = models.CharField(max_length=255, verbose_name="Nummer")
    description = models.CharField(max_length=255, verbose_name="Beschreibung")

    def __str__(self):
        return "{}-{}".format(self.number, self.description)

    class Meta:
        verbose_name = "Sachkonto"
        verbose_name_plural = "Sachkonten"
