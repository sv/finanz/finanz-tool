from django.core.exceptions import ValidationError
from django.db import models

from .booking import Booking, CashTransfer, ZeroSumBooking
from .invoice import IssuedInvoice, ReceivedInvoice


class SAPBooking(models.Model):
    """
    since the domain language is hard to translate the german names are used to avoid further confusion.
    verbose_names of the field should match the collumn names for the csv import
    """

    fi_belegnummer = models.CharField(max_length=255, verbose_name="FI-Belegnummer")
    position_fi_belegnr = models.CharField(max_length=255, verbose_name="Position FI-Belegnr.")

    zahlungsstatus = models.BooleanField(default=False, verbose_name="Zahlungsstatus")
    buchungsdatum = models.DateField(blank=True, null=True, verbose_name="Buchungsdatum")
    belegdatum = models.DateField(blank=True, null=True, verbose_name="Belegdatum")
    ausgleichsdatum = models.DateField(blank=True, null=True, verbose_name="Ausgleichsdatum")
    geschaeftsjahr = models.CharField(max_length=4, verbose_name="Geschäftsjahr", blank=True, null=True)
    referenz = models.CharField(max_length=12, verbose_name="Referenz", blank=True, null=True)

    tw_betrag = models.DecimalField(
        max_digits=21, decimal_places=2, verbose_name="Betrag FK-Währung", blank=True, null=True
    )

    # address
    name = models.CharField(max_length=255, verbose_name="Name", blank=True, null=True)
    ort = models.CharField(max_length=255, verbose_name="Ort", blank=True, null=True)
    postleitzahl = models.CharField(max_length=255, verbose_name="Postleitzahl", blank=True, null=True)
    strasse = models.CharField(max_length=255, verbose_name="Straße", blank=True, null=True)

    text = models.TextField(blank=True, null=True, verbose_name="Text")

    fond = models.CharField(max_length=255, blank=True, null=True, verbose_name="Fonds")
    finanzstelle = models.CharField(max_length=255, blank=True, null=True, verbose_name="Finanzstelle")
    finanzposition = models.CharField(max_length=255, blank=True, null=True, verbose_name="Finanzposition")
    hauptbuchkonto = models.CharField(max_length=255, blank=True, null=True, verbose_name="Hauptbuchkonto")

    sachkontenlangtext = models.CharField(max_length=255, blank=True, null=True, verbose_name="Sachkontenlangtext")
    belegart = models.CharField(max_length=255, verbose_name="Belegart")
    benutzername = models.CharField(max_length=255, blank=True, null=True, verbose_name="Benutzername")
    belegkopftext = models.CharField(max_length=255, blank=True, null=True, verbose_name="Belegkopftext")
    steuerkennzeichen = models.CharField(max_length=255, blank=True, null=True, verbose_name="Steuerkennzeichen")

    internal_invoice_issued = models.ForeignKey(
        IssuedInvoice,
        on_delete=models.PROTECT,
        verbose_name="Interne Rechnung (ausgestellt)",
        blank=True,
        null=True,
    )

    internal_invoice_received = models.ForeignKey(
        ReceivedInvoice,
        on_delete=models.PROTECT,
        verbose_name="Interne Rechnung (eingehend)",
        blank=True,
        null=True,
    )

    internal_cashtransfer = models.ForeignKey(
        CashTransfer,
        on_delete=models.PROTECT,
        verbose_name="Interner Barabschlag",
        blank=True,
        null=True,
    )

    internal_zerosumbooking = models.ForeignKey(
        ZeroSumBooking,
        on_delete=models.PROTECT,
        verbose_name="Interne Nullsummenbuchung",
        blank=True,
        null=True,
    )

    INTERNAL_BOOKING_FIELDS = {
        "internal_invoice_issued": IssuedInvoice,
        "internal_invoice_received": ReceivedInvoice,
        "internal_cashtransfer": CashTransfer,
        "internal_zerosumbooking": ZeroSumBooking,
    }

    def set_internal_booking(self, booking: Booking):
        if booking is None:
            for field in self.INTERNAL_BOOKING_FIELDS:
                setattr(self, field, None)
            return
        found = False
        for field, t in self.INTERNAL_BOOKING_FIELDS.items():
            if isinstance(booking, t):
                setattr(self, field, booking)
                found = True
            else:
                setattr(self, field, None)
        if not found:
            raise RuntimeError("Given invoice type not supported: {}".format(type(booking)))

    def get_internal_booking(self) -> Booking:
        for field in self.INTERNAL_BOOKING_FIELDS:
            ret = getattr(self, field)
            if ret:
                return ret
        return None

    def clean(self):
        super().clean()
        found = False
        for field in self.INTERNAL_BOOKING_FIELDS:
            if getattr(self, field) is not None:
                if found:
                    raise ValidationError("SAPBooking can only have one invoice assigned")
                found = True

    def __str__(self):
        return self.fi_belegnummer + "_" + self.position_fi_belegnr

    class Meta:
        verbose_name = "SAP Buchung"
        verbose_name_plural = "SAP Buchungen"
        unique_together = (("fi_belegnummer", "position_fi_belegnr"),)
