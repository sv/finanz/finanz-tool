from decimal import ROUND_FLOOR, Decimal

from django.db import models
from num2words import num2words

TAX = (
    (Decimal("1.00"), "0%"),
    (Decimal("1.05"), "5%"),
    (Decimal("1.07"), "7%"),
    (Decimal("1.16"), "16%"),
    (Decimal("1.19"), "19%"),
)


class Amount(models.Model):
    gross = models.DecimalField(max_digits=21, decimal_places=2)
    net = models.DecimalField(max_digits=21, decimal_places=2)
    tax = models.DecimalField(choices=TAX, max_digits=3, decimal_places=2)

    @staticmethod
    def format(amount):
        return "€ {:.2f}".format(amount).replace(".", ",")

    @staticmethod
    def calculate_total_in_words(total):
        euros = total.quantize(Decimal("1"), ROUND_FLOOR)
        cents = (total - euros) * Decimal("100")
        return "{} {}/100".format(
            num2words(euros, lang="de"),
            int(cents),
        )

    def format_tax_amount(self):
        return Amount.format(self.calc_tax_amount())

    def format_net(self):
        return Amount.format(self.net)

    def format_gross(self):
        return Amount.format(self.gross)

    def calc_tax_amount(self):
        return self.gross - self.net

    def tax_string_short(self):
        return {
            Decimal("1.00"): "0%",
            Decimal("1.05"): "5%",
            Decimal("1.07"): "7%",
            Decimal("1.16"): "16%",
            Decimal("1.19"): "19%",
        }.get(self.tax, "")

    def tax_code(self):
        return {
            Decimal("1.00"): "V0",
            Decimal("1.05"): "V6",
            Decimal("1.07"): "V2",
            Decimal("1.16"): "V1",
            Decimal("1.19"): "V4",
        }.get(self.tax, "")

    def __str__(self):
        return "{:_>5}% | {:_>9}€ | {:_>9}€".format((self.tax - 1) * 100, self.gross, self.net or Decimal("nan"))

    def save(self):
        self.net = round(self.gross / self.tax, 2)
        return super().save()

    class Meta:
        abstract = True


class ReceivedAmount(Amount):
    invoice = models.ForeignKey("ReceivedInvoice", on_delete=models.CASCADE)


class IssuedAmount(Amount):
    invoice = models.ForeignKey("IssuedInvoice", on_delete=models.CASCADE)
