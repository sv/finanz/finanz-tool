from datetime import datetime

from django.db import models


class Person(models.Model):
    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "Personen"

    name = models.CharField(max_length=255, verbose_name="Name")
    role = models.CharField(max_length=255, verbose_name="Funktion")

    telephone_number = models.CharField(max_length=255, blank=True, verbose_name="Telefonnummer")
    street = models.CharField(max_length=255, blank=True, verbose_name="Straße")
    post_place = models.CharField(max_length=255, blank=True, verbose_name="PLZ und Ort")
    external_transponder_institution = models.CharField(
        blank=True, max_length=255, verbose_name="Name der externen Institution (Transponder)"
    )

    signer = models.CharField(max_length=255, blank=True, verbose_name="Finanzreferent")

    def __str__(self):
        return "{} - {}".format(self.name, self.role)

    def get_today_date(self):
        return datetime.today().strftime("%d.%m.%Y")

    def get_transponder(self):
        """
        Gets the tranponder that is occupied by the person. Currently only one transponder p.P. is supported
        """
        return self.transponder_set.all()[0] if self.transponder_set.count() > 0 else None
