import os
from decimal import Decimal

from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.utils.text import slugify


def make_unique(base, path: str):
    first, ext = os.path.splitext(path)
    for x in range(10000):
        path = f"{first}_{x:05}{ext}"
        if not os.path.exists(os.path.join(base, path)):
            return path
    raise RuntimeError("Too many files with same filename. WTF are you doing?")


class AutoFileRenameMixin:
    def save(self, rename_only=False):
        if not rename_only:
            super().save()
        for field in self._meta.get_fields(include_parents=True):
            if not isinstance(field, models.FileField):
                continue
            file = getattr(self, field.name)
            if not file:
                continue
            path_should = self._get_path(field.name)
            base, _ = os.path.splitext(path_should)
            if not file.name.startswith(base):
                path_should = make_unique(settings.MEDIA_ROOT, path_should)
                path_new = os.path.join(settings.MEDIA_ROOT, path_should)
                os.makedirs(os.path.dirname(os.path.join(settings.MEDIA_ROOT, path_should)), exist_ok=True)
                path_old = file.path
                os.rename(path_old, path_new)
                try:
                    file.name = path_should
                    super().save()
                except Exception as ex:
                    # roll-back if there are errors
                    print("roling back")
                    print(path_new, path_old)
                    os.rename(path_new, path_old)
                    raise ex

    def _get_path(self, fieldname: str):
        if hasattr(self, fieldname + "_path"):
            return getattr(self, fieldname + "_path")()
        return f"{self.annual_plan.year}/{self.__class__.__name__.lower()}/{fieldname.removesuffix('_file')}/{slugify(str(self).replace('/','_'))}.pdf"

    def get_filename(self, fieldname: str):
        return make_unique(settings.MEDIA_ROOT, self._get_path(fieldname))


class RecursiveValueCache:
    expected: Decimal = Decimal(0)
    actual: Decimal = Decimal(0)


class RecursiveValueCacheMixin:
    def get_parents(self):
        raise NotImplementedError()

    def get_children(self):
        raise NotImplementedError()

    def _cache_key(self):
        return f"rvc_{self._meta.model_name}_{self.id}"

    def get_values(self) -> RecursiveValueCache:
        cached = cache.get(self._cache_key())
        if cached:
            return cached
        cached = RecursiveValueCache()
        self.calculate_values(cached)
        cache.set(self._cache_key(), cached, None)
        return cached

    def invalidate_cache(self):
        cache.delete(self._cache_key())
        for parent in self.get_parents():
            if parent:
                parent.invalidate_cache()

    def save(self):
        try:
            self.__class__.objects.get(id=self.id).invalidate_cache()
        except self.__class__.DoesNotExist:
            # only delete caches if model existed before
            pass
        super().save()
        self.invalidate_cache()

    def calculate_values(self, cache_obj: RecursiveValueCache):
        raise NotImplementedError()
