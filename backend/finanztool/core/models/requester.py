from django.contrib.auth.models import User
from django.db import models

from .fond import Fond


class Requester(models.Model):
    class Meta:
        verbose_name = "Antragssteller"
        verbose_name_plural = "Antragssteller"

    name = models.CharField(max_length=255)

    address = models.TextField(blank=True)

    fond = models.ForeignKey(Fond, on_delete=models.PROTECT)

    cost_center = models.CharField(max_length=255)

    telephone_number = models.CharField(max_length=255, blank=True)

    post_adress = models.TextField(blank=True)

    email_address = models.EmailField(blank=True)

    karma = models.IntegerField(default=0)

    user = models.ManyToManyField(User, blank=True)

    # This field contains the hashed password. A newly entered password is hashed in RequesterAdmin class
    password = models.CharField(max_length=255, null=True, unique=True, blank=True)

    @staticmethod
    def get_by_name(req_name: str):
        requester: Requester = None
        try:
            requester: Requester = Requester.objects.get(name=str(req_name))  # try given name
        except Requester.DoesNotExist:
            try:
                requester = Requester.objects.get(name=str("XXUnbekannt"))  # default to XXUnbekannt
            except Requester.DoesNotExist:
                pass  # default to None if XXUnbekannt does not exist
        return requester

    def __str__(self):
        return "{} - {}".format(self.name, self.fond.name)
