from django.core.management import call_command
from django.core.management.base import BaseCommand

from finanztool.tests.util import TestData


class Command(BaseCommand):
    help = "Flushes db and populate again with mocked data"

    requires_system_checks = []

    def handle(self, *fixture_labels, **options):
        # Flushes test database.
        call_command("flush", "--database=test", "--noinput")

        # Populates test db with mocked data.
        TestData()
