from django.apps import apps
from django.core.management.base import BaseCommand, CommandError

from finanztool.core import models
from finanztool.core.models.util import AutoFileRenameMixin


class Command(BaseCommand):
    help = "Rename all files referenced in the db according to the currently implemented filename schema"

    def handle(self, *args, **options):
        for model in apps.get_app_config("finanztool.core").get_models():
            if hasattr(model, "get_filename"):
                for obj in model.objects.all():
                    # explicitly call the method of the mixin. A little dirty but works
                    super(type(obj), obj).save(rename_only=True)
                self.stdout.write(self.style.SUCCESS(f"Saved all {model.__name__} objects"))
