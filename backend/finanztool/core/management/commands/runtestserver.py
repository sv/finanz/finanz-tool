# This file is edited from this base version: https://github.com/django/django/blob/main/django/core/management/commands/testserver.py

from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import connection

from finanztool.tests.util import TestData


class Command(BaseCommand):
    help = "Runs a development server with a mocked db"

    requires_system_checks = []

    def add_arguments(self, parser):
        parser.add_argument(
            "--addrport",
            default="0.0.0.0:8000",
            help="Port number or ipaddr:port to run the server on.",
        )

    def handle(self, *fixture_labels, **options):
        verbosity = options["verbosity"]

        # Create a test database.
        db_name = connection.creation.create_test_db(verbosity=verbosity, autoclobber=True, serialize=False)

        # Populates test db with mocked data.
        TestData()

        # Run the development server on the test db. Turn off auto-reloading because it causes
        # a strange error -- it causes this handle() method to be called
        # multiple times.
        shutdown_message = (
            "\nServer stopped.\nNote that the test database, %r, has not been "
            "deleted. You can explore it on your own." % db_name
        )

        print("test")

        use_threading = connection.features.test_db_allows_multiple_connections
        call_command(
            "runserver",
            addrport=options["addrport"],
            shutdown_message=shutdown_message,
            use_reloader=False,
            use_threading=use_threading,
        )
