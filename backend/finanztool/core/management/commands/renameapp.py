import argparse
import re
from sqlite3 import OperationalError

from django.apps import apps
from django.core.management.base import BaseCommand
from django.db import connection


class Command(BaseCommand):
    help = "Renames app. Usage rename_app [old_name] [new_name] [classes ...]"

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument("old_name", nargs=1, type=str)
        parser.add_argument("new_name", nargs=1, type=str)
        parser.add_argument("models", nargs=argparse.REMAINDER, type=str)

    def to_snake_case(self, name):
        name = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
        name = re.sub("__([A-Z])", r"_\1", name)
        name = re.sub("([a-z0-9])([A-Z])", r"\1_\2", name)
        return name.lower()

    def handle(self, old_name, new_name, models, *args, **options):
        with connection.cursor() as cursor:
            # Rename model
            old_name = old_name[0]
            new_name = new_name[0]
            cursor.execute(
                "UPDATE django_content_type SET app_label='{}' WHERE app_label='{}'".format(new_name, old_name)
            )
            cursor.execute("UPDATE django_migrations SET app='{}' WHERE app='{}'".format(new_name, old_name))

            # rename model tables
            for model_class in apps.get_app_config(new_name).get_models():
                model = model_class.__name__.lower()
                print("Renaming model: {}".format(model))
                try:
                    cursor.execute(
                        "ALTER TABLE {old_name}_{model_name} RENAME TO {new_name}_{model_name}".format(
                            old_name=old_name, new_name=new_name, model_name=model
                        )
                    )
                except Exception as e:
                    print(str(e))

            # rename m2m relation tables
            for modelname, m2mrelation_field in (
                ("transponder", "locking_authorizations"),
                ("cashsettlement", "cash_expenses"),
                ("cashsettlement", "cash_income"),
                ("requester", "user"),
            ):
                print("Renaming m2m: {}".format(m2mrelation_field))
                try:
                    cursor.execute(
                        f"ALTER TABLE {old_name}_{modelname}_{m2mrelation_field} RENAME TO {new_name}_{modelname}_{m2mrelation_field}"
                    )
                except Exception as e:
                    print(str(e))
