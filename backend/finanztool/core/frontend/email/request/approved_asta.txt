{% extends 'email/base.txt' %}
{% block content %}
herzlichen Glückwunsch, deine Anfrage {{ req.budget.name }} wurde von einem Referenten für Finanzen genehmigt!
Wir werden deine Bestellung intern bestellen und brauchen dafür noch eine passende Lieferadresse, bitte leite diese inklusive dieser Mail an asta-finanz@fs.tum.de weiter. Wir kümmern uns dann um alles weitere!

Leite bitte diese Mail mit einem kurzen Text an asta-finanz@fs.tum.de weiter sobald die Ware funktionstüchtig angekommen ist, damit wir wissen wann wir die Rechnung bezahlen können. Und jetzt: Viel Spaß beim Bestellen!
{% endblock %}
