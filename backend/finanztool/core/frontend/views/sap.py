import csv
import io
from decimal import Decimal

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import models
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.datetime_safe import datetime
from django.views.generic import View

from finanztool.core.forms import SAPUploadForm
from finanztool.core.models import IssuedInvoice, ReceivedInvoice, SAPBooking


class SAPUploadView(LoginRequiredMixin, View):
    def get(self, request):
        if not request.user.is_superuser:
            return render(request, "rechnungstool/403.html", status=403)

        form = SAPUploadForm()
        return render(request, "rechnungstool/sap_upload.html", {"form": form})

    def post(self, request):
        if not request.user.is_superuser:
            return render(request, "rechnungstool/403.html", status=403)

        form = SAPUploadForm(request.POST, request.FILES)
        if form.is_valid():
            csv_file = request.FILES["file"]

            reader = csv.DictReader(io.StringIO(csv_file.read().decode("latin-1")), delimiter=";")

            already_present_rows = 0
            new_rows = 0
            modified_rows = 0

            for row in reader:
                fi_belegnummer = row["FI-Belegnummer"].strip()

                # some rows may be a result of weird excel formatting. Check if at least the fi_belegnummer is present.
                if not fi_belegnummer:
                    continue

                position_fi_belegnr = row["Position FI-Belegnr."].strip()

                booking = SAPBooking.objects.filter(
                    fi_belegnummer=fi_belegnummer, position_fi_belegnr=position_fi_belegnr
                ).first()
                if booking:
                    already_present_rows += 1
                else:
                    booking = SAPBooking(fi_belegnummer=fi_belegnummer, position_fi_belegnr=position_fi_belegnr)
                    new_rows += 1

                date_format = "%d.%m.%Y"

                modified = []
                for field in booking._meta.fields:
                    if isinstance(field, models.ForeignKey):
                        # These are not imported, used internally only
                        continue
                    if field.name in ("fi_belegnummer", "position_fi_belegnr"):
                        # These are the "primary key" and are handled above
                        continue

                    def mod(new_value):
                        old = getattr(booking, field.name)
                        if old != new_value:
                            setattr(booking, field.name, new_value)
                            modified.append(field.name)

                    if field.verbose_name in row:
                        if isinstance(field, (models.CharField, models.TextField)):
                            mod(row[field.verbose_name])
                        elif isinstance(field, models.BooleanField):
                            # this might need further work in the future
                            mod(row[field.verbose_name] == "X")
                        elif isinstance(field, models.DateField):
                            if row[field.verbose_name]:
                                mod(datetime.strptime(row[field.verbose_name], date_format).date())
                        elif isinstance(field, models.DecimalField):
                            data = row[field.verbose_name].strip()
                            if "," in data:
                                # german number formatting
                                data = data.replace(".", "").replace(",", ".")
                            mod(Decimal(data))
                        else:
                            raise Exception("Unsupported field type for field {}".format(field.name))
                if modified:
                    modified_rows += 1
                booking.clean()
                booking.save()

            return render(
                request,
                "rechnungstool/sap_after_upload.html",
                {
                    "already_present_rows": already_present_rows,
                    "new_rows": new_rows,
                    "modified_rows": modified_rows,
                },
            )
        return HttpResponse("ahhhhh")


class MatchingCheckView(LoginRequiredMixin, View):
    @staticmethod
    def render_form(request, context):
        return render(request, "rechnungstool/sap_matching_check.html", context)

    def get(self, request):
        return render(request, "rechnungstool/403.html", status=403)

    def post(self, request):
        if not request.user.is_superuser:
            return render(request, "rechnungstool/403.html", status=403)
        amount_matched = 0
        amount_unmatched = 0
        for k, v in request.POST.items():
            if not k.startswith("booking-"):
                continue
            booking = SAPBooking.objects.get(id=int(k[8:]))
            if booking.get_internal_booking():
                # Just a double sanity check. Should not occur
                raise Exception("Booking already matched")
            if v == "invoice-none":
                invoice = None
                amount_unmatched += 1
            elif v.startswith("invoice-issued-"):
                invoice = IssuedInvoice.objects.get(id=int(v[15:]))
                amount_matched += 1
            elif v.startswith("invoice-received-"):
                invoice = ReceivedInvoice.objects.get(id=int(v[17:]))
                amount_matched += 1
            else:
                raise Exception("Can not parse form value")
            booking.set_internal_booking(invoice)
            booking.clean()
            booking.save()
        return render(
            request,
            "rechnungstool/sap_done.html",
            {
                "amount_matched": amount_matched,
                "amount_unmatched": amount_unmatched,
            },
        )
