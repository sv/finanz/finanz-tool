from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import View

from finanztool.core.models import (
    Key,
    LockingAuthorization,
    LockingAuthorizationGroup,
    Person,
    Transponder,
)


class TransponderView(LoginRequiredMixin, View):
    def get_context(self):
        context = {
            "transponder_view": {
                "title": "Transponder",
                "locking_groups": LockingAuthorizationGroup.objects.all(),
                "sections": [
                    {
                        "title": "Freie Transponder",
                        "transponders": Transponder.objects.filter(status="vault").all(),
                    },
                    {
                        "title": "Ausgegebene Transponder",
                        "transponders": Transponder.objects.filter(status="sent").all(),
                    },
                ],
            },
            "key_view": {
                "title": "Schlüssel",
                "sections": [
                    {
                        "title": "Freie Schlüssel",
                        "keys": Key.objects.filter(status="vault").all(),
                    },
                    {
                        "title": "Ausgegebene Schlüssel",
                        "persons": Person.objects.exclude(keys=None).order_by("-name").all(),
                        "keys": Key.objects.filter(status="sent").order_by("-carrier__name").all(),
                    },
                ],
            },
        }
        return context

    def get(self, request):
        if not request.user.is_superuser:
            return render(request, "rechnungstool/403.html", status=403)

        return render(request, "rechnungstool/transponder.html", self.get_context())

    def post(self, request):
        if not request.user.is_superuser:
            return render(request, "rechnungstool/403.html", status=403)

        transponder_id = request.POST.get("transponder_id", 0)
        transponder = Transponder.objects.get(id=transponder_id)

        for i, locking_auth in enumerate(LockingAuthorization.objects.all()):
            has_locking_authorization = transponder.locking_authorizations.filter(id=locking_auth.id).count() != 0
            is_checked = bool(request.POST.get("lock_" + str(transponder_id) + "_" + str(i + 1), False))
            if is_checked and not has_locking_authorization:
                transponder.locking_authorizations.add(locking_auth)
            elif not is_checked and has_locking_authorization:
                locking_auth.transponder_set.remove(transponder)
                locking_auth.save()
        transponder.save()

        return render(request, "rechnungstool/transponder.html", self.get_context())
