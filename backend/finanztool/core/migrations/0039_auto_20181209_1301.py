# Generated by Django 2.1.3 on 2018-12-09 12:01

from decimal import Decimal
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0038_auto_20181209_1213'),
    ]

    operations = [
        migrations.CreateModel(
            name='IssuedAmount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gross', models.DecimalField(decimal_places=2, max_digits=21)),
                ('net', models.DecimalField(decimal_places=2, max_digits=21)),
                ('tax', models.DecimalField(choices=[(Decimal('1.00'), '0%'), (Decimal('1.07'), '7%'), (Decimal('1.19'), '19%')], decimal_places=2, max_digits=3)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ReceivedAmount',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gross', models.DecimalField(decimal_places=2, max_digits=21)),
                ('net', models.DecimalField(decimal_places=2, max_digits=21)),
                ('tax', models.DecimalField(choices=[(Decimal('1.00'), '0%'), (Decimal('1.07'), '7%'), (Decimal('1.19'), '19%')], decimal_places=2, max_digits=3)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='issuedinvoice',
            name='amounts',
        ),
        migrations.RemoveField(
            model_name='receivedinvoice',
            name='amounts',
        ),
        migrations.DeleteModel(
            name='Amount',
        ),
        migrations.AddField(
            model_name='receivedamount',
            name='invoice',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.ReceivedInvoice'),
        ),
        migrations.AddField(
            model_name='issuedamount',
            name='invoice',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.IssuedInvoice'),
        ),
    ]
