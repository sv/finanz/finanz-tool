# Generated by Django 2.1.15 on 2020-11-17 14:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0071_auto_20201117_1053'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='issuedinvoice',
            unique_together={('booking_year', 'internal_invoice_number')},
        ),
        migrations.AlterUniqueTogether(
            name='receivedinvoice',
            unique_together={('vendor', 'vendor_invoice_number'), ('booking_year', 'internal_invoice_number')},
        ),
    ]
