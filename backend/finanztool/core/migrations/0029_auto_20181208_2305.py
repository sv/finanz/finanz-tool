# Generated by Django 2.1.3 on 2018-12-08 22:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0028_auto_20181208_2303'),
    ]

    operations = [
        migrations.AlterField(
            model_name='anualplan',
            name='budget_allocation',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=21, null=True),
        ),
        migrations.AlterField(
            model_name='anualplan',
            name='budget_left',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=21, null=True),
        ),
        migrations.AlterField(
            model_name='anualplan',
            name='last_calculated',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
