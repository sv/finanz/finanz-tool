# Generated by Django 2.1.3 on 2018-12-08 19:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0020_remove_amount_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='amount',
            name='tax',
            field=models.DecimalField(decimal_places=2, max_digits=3),
        ),
    ]
