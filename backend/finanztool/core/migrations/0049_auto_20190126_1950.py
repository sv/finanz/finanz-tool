# Generated by Django 2.1.3 on 2019-01-26 18:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0048_auto_20190126_1847'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipient',
            name='salutation',
            field=models.CharField(blank=True, max_length=255, verbose_name='Anrede'),
        ),
        migrations.AlterField(
            model_name='vendor',
            name='customer_number',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
