# Generated by Django 3.1.3 on 2020-11-23 23:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0078_auto_20201122_0350'),
    ]

    operations = [
        migrations.AddField(
            model_name='issuedinvoice',
            name='total_in_words',
            field=models.CharField(blank=True, max_length=255, verbose_name='Gesamtbetrag in Worten'),
        ),
        migrations.AddField(
            model_name='receivedinvoice',
            name='total_in_words',
            field=models.CharField(blank=True, max_length=255, verbose_name='Gesamtbetrag in Worten'),
        ),
    ]
