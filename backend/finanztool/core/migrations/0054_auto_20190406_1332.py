# Generated by Django 2.1.3 on 2019-04-06 11:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0053_auto_20190406_0826'),
    ]

    operations = [
        migrations.CreateModel(
            name='InternalBooking',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
                ('booking_year', models.CharField(default=2019, max_length=4, verbose_name='Buchungsjahr')),
                ('internal_booking_number', models.CharField(max_length=255, verbose_name='Interne Buchungsnummer')),
                ('amount', models.DecimalField(decimal_places=2, max_digits=21, verbose_name='Betrag')),
                ('receiver', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='receiver', to='core.Requester', verbose_name='Empfänger')),
                ('sender', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='sender', to='core.Requester', verbose_name='Sender')),
            ],
            options={
                'verbose_name': 'Interne Buchung',
                'verbose_name_plural': 'Interne Buchungen',
            },
        ),
        migrations.AlterUniqueTogether(
            name='internalbooking',
            unique_together={('booking_year', 'internal_booking_number')},
        ),
    ]
