# Generated by Django 2.1.3 on 2018-12-08 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0014_auto_20181128_2216'),
    ]

    operations = [
        migrations.RenameField(
            model_name='request',
            old_name='requested_object',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='request',
            old_name='orderer',
            new_name='requester',
        ),
        migrations.RemoveField(
            model_name='request',
            name='invoice',
        ),
        migrations.AddField(
            model_name='request',
            name='reason_file',
            field=models.FileField(blank=True),
        ),
        migrations.AlterField(
            model_name='request',
            name='status',
            field=models.IntegerField(choices=[(1, 'New'), (2, 'BHG checked'), (3, 'offes needed'), (4, 'ZA3 requested'), (5, 'approved'), (6, 'declined'), (7, 'finished')]),
        ),
    ]
