# Generated by Django 2.1.3 on 2019-01-26 17:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0047_auto_20190126_1842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipient',
            name='country_code',
            field=models.CharField(blank=True, max_length=2),
        ),
        migrations.AlterField(
            model_name='recipient',
            name='post_place',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AlterField(
            model_name='recipient',
            name='street',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
