# Generated by Django 2.1.3 on 2019-02-10 13:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0050_auto_20190209_1946'),
    ]

    operations = [
        migrations.AlterField(
            model_name='issuedinvoice',
            name='status',
            field=models.CharField(choices=[('new', 'Neu'), ('done', 'Erledigt'), ('sap_done', 'SAP verbucht'), ('cancelled', 'Storniert')], default='new', max_length=64, verbose_name='Status (nur intern)'),
        ),
        migrations.AlterField(
            model_name='receivedinvoice',
            name='status',
            field=models.CharField(choices=[('new', 'Neu'), ('done', 'Erledigt'), ('sap_done', 'SAP verbucht'), ('cancelled', 'Storniert')], default='new', max_length=64, verbose_name='Status (nur intern)'),
        ),
    ]
