# Generated by Django 3.1.3 on 2020-11-22 03:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0077_auto_20201122_0326'),
    ]

    operations = [
        migrations.AddField(
            model_name='sapbooking',
            name='ort',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Ort'),
        ),
        migrations.AddField(
            model_name='sapbooking',
            name='postleitzahl',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Postleitzahl'),
        ),
        migrations.AddField(
            model_name='sapbooking',
            name='strasse',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Straße'),
        ),
    ]
