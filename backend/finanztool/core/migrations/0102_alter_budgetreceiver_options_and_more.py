# Generated by Django 4.0 on 2022-01-15 17:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0101_alter_key_unique_together'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='budgetreceiver',
            options={'verbose_name': 'Budgetempfänger - Haushalt', 'verbose_name_plural': 'Budgetempfänger - Haushalt'},
        ),
        migrations.AlterModelOptions(
            name='eventbudgetreceiver',
            options={'verbose_name': 'Budgetempfänger - Veranstaltung', 'verbose_name_plural': 'Budgetempfänger - Veranstaltungen'},
        ),
    ]
