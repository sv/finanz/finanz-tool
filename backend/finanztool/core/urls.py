from django.urls import path
from django.views.generic import RedirectView

from finanztool.core.frontend.views import (
    MatchingCheckView,
    SAPUploadView,
    TransponderView,
)

urlpatterns = [
    path("", RedirectView.as_view(url="/new/", permanent=True)),
    path("sap/upload", SAPUploadView.as_view(), name="sap_upload"),
    path("sap/matchingcheck", MatchingCheckView.as_view(), name="matching_check"),
    path("transponder", TransponderView.as_view(), name="transponder"),
]
