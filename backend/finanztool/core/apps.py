from django.apps import AppConfig


class FinanztoolConfig(AppConfig):
    name = "finanztool.core"
    label = "core"
