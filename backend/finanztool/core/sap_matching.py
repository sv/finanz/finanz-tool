import datetime
import re
from itertools import chain

import jellyfish
from django.db.models import Sum

from finanztool.core.models import Invoice, IssuedInvoice, ReceivedInvoice, SAPBooking


def relative_dist(text1, text2):
    "calculates the relative levenshtein distance (0-1)"

    if not text1 or not text2:
        return 1
    if max(len(text1), len(text2)):
        return jellyfish.levenshtein_distance(text1, text2) / max(len(text1), len(text2))  # pylint: disable=no-member
    return 1


def blockwise_dist(text1, text2):
    "calculates the distance between two texts (0-1) by matching all alphanumeric segments"

    # first text should be the longer one
    if len(text2) > len(text1):
        text1, text2 = text2, text1

    parts1 = re.split("[^a-zA-Z0-9äöüÄÖÜß]+", text1)
    parts2 = re.split("[^a-zA-Z0-9äöüÄÖÜß]+", text2)

    if not parts1 or not parts2:
        return 1

    total = max(sum(map(len, parts1)), sum(map(len, parts1)))
    if not total:
        return 1

    matched = 0
    for part in parts1.copy():
        if part in parts2:
            matched += len(part)
            parts2.remove(part)
            parts1.remove(part)

    # parts2 should have fewer elements than part1
    if len(parts2) > len(parts1):
        parts1, parts2 = parts2, parts1

    dists = [
        [jellyfish.levenshtein_distance(part1, part2) for part1 in parts1]  # pylint: disable=no-member
        for part2 in parts2
    ]

    while dists:
        best_match = min(
            enumerate(
                map(
                    lambda x: min(enumerate(x), key=lambda y: y[1]),
                    dists,
                )
            ),
            key=lambda x: x[1][1],
        )
        x, y, val = best_match[0], best_match[1][0], best_match[1][1]
        matched += max(len(parts2[x]), len(parts1[y])) - val
        del dists[x]
        for d in dists:
            del d[y]
        del parts2[x]
        del parts1[y]

    return 1 - matched / total


def parse_date_range_text_date(text: str):
    "Parse date part of dd.mm.yyyy/hh:mm where time is optional and ignored"
    if "/" in text:
        # strip off time
        text = text[: text.find("/")]
    return datetime.datetime.strptime(text, "%d.%m.%Y").date()


def parse_date_range_text(text: str):
    """
    Parse date range from SAP text field. Format: "______dd.mm.yyyy/hh:mm-dd.mm.yyyy/hh:mm"
    where the time is optional. Either returns two dates (start date and end date) or two
    times None.
    """
    if not text.startswith("______"):
        return None, None
    text = text[6:]
    parts = [x.strip() for x in text.split("-")]
    if len(parts) != 2:
        return None, None
    return parse_date_range_text_date(parts[0]), parse_date_range_text_date(parts[1])


def find_month_in_string(text: str, month: int):
    month_texts = ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"]
    return month_texts[month - 1] in text or str(month) in text


def find_months_in_string(text: str, start: datetime.date, end: datetime.date):
    for month in range(start.month, end.month + 1):
        if not find_month_in_string(text, month):
            return False
    return True


def telephone_scores(booking: SAPBooking, invoice: Invoice):
    "Specifically calculate scores for telephone bookings. Returns None if booking is not telephone"
    if not isinstance(invoice, ReceivedInvoice):
        return None
    if booking.hauptbuchkonto != "682200":
        return None
    if booking.hauptbuchkonto != invoice.general_ledger_account.number:
        return None
    if not booking.belegkopftext.endswith(booking.finanzstelle):
        return None
    start, end = parse_date_range_text(booking.text)
    if not start:
        return None
    # booking is defintively a telefone booking. Return bad scores instead of None from now
    if booking.finanzstelle != invoice.requester.cost_center:
        return {"telephone": 1}
    if not find_months_in_string(invoice.vendor_invoice_number, start, end):
        return {"telephone": 1}
    return {"telephone": 0}


def porto_scores(booking: SAPBooking, invoice: Invoice):
    "Specifically calculate scores for porto bookings. Returns None if booking is not porto."
    if not isinstance(invoice, ReceivedInvoice):
        return None
    if booking.hauptbuchkonto != "682100":
        return None
    if booking.hauptbuchkonto != invoice.general_ledger_account.number:
        return None
    if not booking.belegkopftext.startswith("Porto"):
        return None
    start, end = parse_date_range_text(booking.text)
    if not start:
        return None
    # booking is defintively a porto booking. Return bad scores instead of None from now
    if booking.finanzstelle != invoice.requester.cost_center:
        return {"porto": 1}
    if not find_months_in_string(invoice.vendor_invoice_number, start, end):
        return {"porto": 1}
    if not find_months_in_string(booking.belegkopftext, start, end):
        return {"porto": 1}
    return {"porto": 0}


def calculate_scores(booking: SAPBooking, inv: Invoice):
    # specific scoring functions for special cases
    scores = telephone_scores(booking, inv)
    if scores:
        return scores
    scores = porto_scores(booking, inv)
    if scores:
        return scores

    scores = {}
    recipient = inv.vendor if isinstance(inv, ReceivedInvoice) else inv.recipient
    scores["street"] = 1 if not booking.strasse else relative_dist(recipient.street, booking.strasse)
    scores["place"] = (
        1
        if not booking.postleitzahl or not booking.ort
        else relative_dist(recipient.post_place, booking.postleitzahl + " " + booking.ort)
    )
    scores["name"] = 1 if not booking.name else blockwise_dist(recipient.name, booking.name)
    scores["general_ledger_account"] = 0 if inv.general_ledger_account.number == booking.hauptbuchkonto else 1
    scores["cost_center"] = 0 if inv.requester.cost_center == booking.finanzstelle else 1
    scores["date"] = min(1, abs((booking.belegdatum - inv.booking_date).days) / 60)
    if isinstance(inv, ReceivedInvoice):
        scores["invoice_number"] = 0 if inv.vendor_invoice_number in booking.text else 1
        scores["text"] = blockwise_dist(inv.reference, booking.text)
    else:
        if booking.referenz:
            scores["bkz"] = 0 if booking.referenz == inv.bkz.replace(".", "") else 1
    return scores


def match_sap(queryset):
    """
    Match invoices based on heuristics. Returns a form to be presented to the user
    for reviewing the matches.
    """
    # get list of all relevant years
    years = list(
        map(
            lambda x: x["geschaeftsjahr"],
            queryset.values("geschaeftsjahr").order_by("geschaeftsjahr").distinct(),
        )
    )

    # precache relevant invoies
    received_invoices = list(
        ReceivedInvoice.objects.filter(annual_plan__year__in=years)
        .filter(status="sent")
        .select_related("requester__fond", "vendor", "general_ledger_account")
        .annotate(booked_total=ReceivedInvoice.annotate_booked_total()),
    )
    issued_invoices = list(
        IssuedInvoice.objects.filter(annual_plan__year__in=years)
        .filter(status="sent")
        .select_related("requester__fond", "recipient", "general_ledger_account")
        .annotate(booked_total=IssuedInvoice.annotate_booked_total()),
    )

    # Precache amount sums per fi_belegnummer
    sums = dict(
        map(lambda x: (x["fi_belegnummer"], x["sum"]), queryset.values("fi_belegnummer").annotate(sum=Sum("tw_betrag")))
    )

    # matches to present to the user
    certain_matches = []
    uncertain_matches = []

    for booking in queryset:
        matches = []
        for inv in chain(received_invoices, issued_invoices):
            # definitive exclusion criteria
            if inv.requester.fond.number != booking.fond:
                continue
            if inv.annual_plan.year != booking.geschaeftsjahr:
                continue

            scores = {
                "exact_amount": 0,
            }
            # pylint: disable=consider-using-in
            if inv.booked_total != booking.tw_betrag and inv.booked_total != sums[booking.fi_belegnummer]:
                if (
                    abs(inv.booked_total - sums[booking.fi_belegnummer]) < 0.05
                ):  # less than 5ct due to tax rounding issues
                    scores["exact_amount"] = 1
                else:
                    continue

            scores.update(calculate_scores(booking, inv))
            if "telephone" in scores and scores["telephone"] == 1:
                continue
            if "porto" in scores and scores["porto"] == 1:
                continue

            # These are currently just estimated and should be improved using training data
            weights = {
                "exact_amount": 5,
                "cost_center": 3,
                "place": 3,
                "street": 2,
                "name": 1,
                "general_ledger_account": 0.2,
                "date": 0.5,
                "invoice_number": 3,
                "text": 1,
                "bkz": 10,
            }

            max_score = len(scores)
            for k, v in weights.items():
                if k in scores:
                    max_score += v - 1
                    scores[k] *= v

            score = sum(scores.values()) / max_score
            matches.append(
                {
                    "score": score,
                    "invoice": inv,
                    "scores": scores,
                }
            )

        if matches:
            matches.sort(key=lambda x: x["score"])
            final_match = {
                "booking": booking,
                "matches": matches,
            }
            if matches[0]["score"] < 0.3 and (len(matches) == 1 or matches[1]["score"] - matches[0]["score"] > 0.15):
                certain_matches.append(final_match)
            else:
                uncertain_matches.append(final_match)

    return {
        "sections": [
            {
                "title": "Wahrscheinliche Zuordnungen",
                "matches": certain_matches,
            },
            {
                "title": "Kritischere Zuordnungen",
                "matches": uncertain_matches,
            },
        ],
    }
