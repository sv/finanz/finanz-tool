import logging

from django.conf import settings
from django.core.exceptions import SuspiciousOperation
from django.urls import reverse
from django.utils.http import urlencode
from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from mozilla_django_oidc.utils import absolutify

from finanztool.core.models import UserProfile

LOGGER = logging.getLogger(__name__)


def provider_logout(request):
    print("TEST")
    redirect_url = (
        settings.OIDC_OP_LOGOUT_URL
        + "?"
        + urlencode({"post_logout_redirect_uri": absolutify(request, "/"), "client_id": settings.OIDC_RP_CLIENT_ID})
    )
    return redirect_url


class KeycloakAuth(OIDCAuthenticationBackend):
    def filter_users_by_claims(self, claims):
        username = claims.get("preferred_username")
        if not username:
            return self.UserModel.objects.none()

        try:
            profile = UserProfile.objects.get(sso_username=username)
            save = False
            first_name = claims.get("given_name")
            if first_name and first_name != profile.user.first_name:
                profile.user.first_name = first_name
                save = True
            last_name = claims.get("family_name")
            if last_name and last_name != profile.user.last_name:
                profile.user.last_name = last_name
                save = True
            email = claims.get("email")
            if email and email != profile.user.email:
                profile.user.email = email
                save = True
            if save:
                profile.user.save()
            return [profile.user]
        except UserProfile.DoesNotExist:
            return self.UserModel.objects.none()

    # THIS METHOD WAS MODIFIED FROM: https://github.com/mozilla/mozilla-django-oidc/blob/main/mozilla_django_oidc/auth.py
    def authenticate(self, request, **kwargs):
        """Authenticates a user based on the OIDC code flow."""

        self.request = request  # pylint: disable=W0201
        if not self.request:
            return None

        state = self.request.GET.get("state")
        code = self.request.GET.get("code")
        nonce = kwargs.pop("nonce", None)

        if not code or not state:
            return None

        reverse_url = self.get_settings("OIDC_AUTHENTICATION_CALLBACK_URL", "oidc_authentication_callback")

        token_payload = {
            "client_id": self.OIDC_RP_CLIENT_ID,
            "client_secret": self.OIDC_RP_CLIENT_SECRET,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": absolutify(self.request, reverse(reverse_url)),
        }

        # Get the token
        token_info = self.get_token(token_payload)
        id_token = token_info.get("id_token")
        access_token = token_info.get("access_token")
        refresh_token = token_info.get("refresh_token")

        # Validate the token
        payload = self.verify_token(id_token, nonce=nonce)

        if payload:
            self.store_tokens(access_token, id_token)
            self.request.session["oidc_refresh_token"] = refresh_token
            try:
                return self.get_or_create_user(access_token, id_token, payload)
            except SuspiciousOperation as exc:
                LOGGER.warning("failed to get or create user: %s", exc)
                return None

        return None
