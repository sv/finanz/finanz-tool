import fitz
from django.core.handlers.wsgi import WSGIRequest
from django.test import TestCase
from django.test.client import RequestFactory
from django.urls import reverse
from freezegun import freeze_time

from finanztool.pdf.generation import pdf_generation
from finanztool.tests import pdfutil, util


@freeze_time("2023-01-06")
class PDFGenerationTest(TestCase):
    def setUp(self):
        self.data = util.TestData()

        self.client.force_login(self.data.superuser)

    def assertFormContent(self, doc: fitz.Document, fields_should: dict):
        for page in doc:
            widget: fitz.Widget = page.first_widget
            while widget:
                for key, val in fields_should.items():
                    # Match prefix as fields are renamed with model pk for uniqueness
                    if widget.field_name.startswith(key):
                        self.assertEqual(widget.field_value, fields_should[key])
                        break

                # TODO: test fields not given in fields_should
                # if widget.field_type_string == "CheckBox":
                #     if "AuthNo" in widget.field_name:
                #         self.assertEqual(widget.field_value, "Ja")
                #     else:
                #         self.assertEqual(widget.field_value, "Off")
                widget = widget.next

    def assertMetadata(self, doc, title_should: str):
        self.assertEqual(doc.page_count, 4)
        self.assertEqual(doc.metadata.get("title"), title_should)
        self.assertEqual(doc.metadata.get("author"), "Studentische Vertretung - Finanztool")
        self.assertEqual(doc.metadata.get("subject"), "PDF-Formular")
        self.assertEqual(doc.metadata.get("creator"), "Studentische Vertretung - Finanztool")

    def test_internal_contract_generation(self):
        request: WSGIRequest = RequestFactory().get(reverse("admin:core_person_changelist"))
        request.user = self.data.superuser

        doc = pdfutil.file_response_to_document(
            util.admin_action(self.client, "print_contracts", [self.data.person_with_transponder])
        )

        self.assertMetadata(doc, str(self.data.person_with_transponder.pk))
        self.assertFormContent(
            doc,
            {
                "ContractValidUntil_1": "30.11.2023",
                "Role_1": "Referentin für Musterangelegenheiten",
                "Treasurer_0_1": self.data.superuser.get_full_name(),
                "Treasurer_1_1": self.data.superuser.get_full_name(),
                "Carrier_0_1": self.data.person_with_transponder.name,
                "Carrier_1_1": self.data.person_with_transponder.name,
                "Date_0_1": "06.01.2023",
                "Date_1_1": "06.01.2023",
                "Date_2_1": "06.01.2023",
                "Date_3_1": "06.01.2023",
                "TransponderNr_1": "XX1234XX",
                "TransponderAuthYes_1_1": "Ja",
                "TransponderAuthNo_1_1": "Off",
                "TransponderAuth_1_1": "Musterschließgruppe (002)",
                "TransponderAuthYes_2_1": "Ja",
                "TransponderAuthNo_2_1": "Off",
                "TransponderAuth_2_1": "{} - Musterschließrecht 1".format(
                    self.data.locking_authorization_without_group.id
                ),
            },
        )

        doc.close()
