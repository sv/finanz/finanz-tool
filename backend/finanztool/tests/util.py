from datetime import date, timedelta
from decimal import Decimal
from os import path

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.files.base import ContentFile
from django.db.models import Model
from django.test import TestCase
from django.urls import reverse

from finanztool.core.models import (
    AnnualPlan,
    Budget,
    BudgetReceiver,
    Fond,
    GeneralLedgerAccount,
    LockingAuthorization,
    LockingAuthorizationGroup,
    Person,
    ReceivedAmount,
    Recipient,
    Request,
    Requester,
    Transponder,
    Vendor,
    fond,
)
from finanztool.core.models.invoice import ReceivedInvoice


def file(name):
    """returns the content of the given file in the tests/data directory as bytes"""
    with open(path.join(path.dirname(__file__), "data", name), "rb") as f:
        return ContentFile(f.read(), "test/{}".format(name))


def cs(model: Model):
    """Clean and save given model and return it"""
    model.full_clean()
    model.save()
    # convert to string to have syntax check for the __str__() methods
    str(model)
    return model


def admin_action(client, action, queryset):
    data = {"action": action, "_selected_action": [f.pk for f in queryset]}
    return client.post(reverse("admin:core_" + type(queryset[0])()._meta.model_name + "_changelist"), data, follow=True)


class TestData(TestCase):
    def __init__(self):
        cache.clear()
        # A user account
        self.user = cs(
            User.objects.create_user(
                username="user",
                password="pw",
                first_name="Us",
                last_name="Er",
                is_staff=True,
            )
        )

        self.superuser = cs(
            User.objects.create_superuser(
                username="superuser",
                password="pw",
                first_name="Super",
                last_name="User",
                is_staff=True,
            )
        )

        self.user_no_requesters = cs(
            User.objects.create_user(
                username="noaccess",
                password="pw",
                first_name="No",
                last_name="Access",
                is_staff=True,
            )
        )

        # One Vendor/Recipient
        self.vendor = cs(
            Vendor(
                iban="DE82 5001 0517 8152 5248 36",
                name="Musterfirma",
                street="Musterstraße 11",
                post_place="12345 Musterstadt",
                country_code="DE",
                bic="BANKDEBYTUM",
                bankname="TUM Bank of Excellence",
            )
        )

        # Two Fonds
        self.fond_event = cs(
            Fond(
                pk=200,
                name="Veranstaltungsfond",
                number="1337",
                booking_type=fond.NET,
            )
        )
        self.fond_haus = cs(
            Fond(
                pk=100,
                name="Haushaltsfond",
                number="7353",
                booking_type=fond.GROSS,
            )
        )

        # One Requester per fond
        self.requester_event = cs(
            Requester(
                pk=200,
                fond=self.fond_event,
                name="Megaparty",
                cost_center="K05T5TELL3",
            )
        )
        self.requester_event_2 = cs(
            Requester(
                pk=201,
                fond=self.fond_event,
                name="Miniparty",
                cost_center="K05T5TELL3",
            )
        )
        self.requester_event_3 = cs(
            Requester(
                pk=202,
                fond=self.fond_event,
                name="Scheinposten",
                cost_center="K05T5TELL3",
            )
        )
        self.requester_event.user.add(self.user)
        self.requester_event_2.user.add(self.user)
        self.requester_event_3.user.add(self.user)
        cs(self.requester_event)
        cs(self.requester_event_2)
        cs(self.requester_event_3)
        self.requester_haus = cs(
            Requester(
                fond=self.fond_haus,
                name="Fachschaft Bergbau",
                cost_center="K05T5TELL4",
                password=make_password("1234"),
            )
        )
        self.requester_haus.user.add(self.user)
        cs(self.requester_haus)

        # One GeneralLedgerAccount
        self.general_ledger_account = cs(
            GeneralLedgerAccount(number="999999", description="GEZ-Gebühren")  # dAs hEißT BeItRaG!!!!11!
        )

        # One Annual Plan
        self.annual_plan = cs(
            AnnualPlan(
                year="2023",
                budget_allocation=Decimal("1000.00"),
            )
        )

        # Budget receivers for both requesters in the current annual plan
        self.budget_receiver = cs(
            BudgetReceiver(
                pk=100,
                annual_plan=self.annual_plan,
                receiver=self.requester_haus,
                target=Decimal("-1000.00"),
            )
        )

        self.event_budget_receiver = cs(
            BudgetReceiver(
                pk=200,
                annual_plan=self.annual_plan,
                receiver=self.requester_event,
                target=Decimal("0.00"),
            )
        )

        self.event_budget_receiver_2 = cs(
            BudgetReceiver(
                pk=250,
                annual_plan=self.annual_plan,
                receiver=self.requester_event_2,
                target=Decimal("400.00"),
            )
        )

        # Save annual plan again after receivers are created for some calculations
        self.annual_plan.save()

        self.budget_fixed_costs = cs(
            Budget(
                pk=210,
                annual_plan=self.annual_plan,
                requester=self.requester_event,
                target="-500",
                status="requested",
                name="Fixkosten",
            )
        )

        self.budget_purchases = cs(
            Budget(
                pk=220,
                annual_plan=self.annual_plan,
                requester=self.requester_event,
                target="-500",
                status="requested",
                name="Einkäufe",
            )
        )

        self.budget_hamberger = cs(
            Budget(
                pk=221,
                parent=self.budget_purchases,
                annual_plan=self.annual_plan,
                requester=self.requester_event,
                target="-500",
                status="requested",
                name="Einkauf Hamberger",
            )
        )

        self.budget_metro = cs(
            Budget(
                pk=222,
                parent=self.budget_purchases,
                annual_plan=self.annual_plan,
                requester=self.requester_event,
                target="-200",
                status="requested",
                name="Einkauf Hornbach",
            )
        )

        self.request = cs(
            Request(
                pk=221,
                budget=self.budget_hamberger,
                reason="Einkauf Helfergetränke",
                net_single_amount="10.00",
            )
        )

        self.budget_stale = cs(
            Budget(
                pk=223,
                parent=self.budget_fixed_costs,
                annual_plan=self.annual_plan,
                requester=self.requester_event,
                target="-200",
                status="requested",
                name="Altes Budget",
            )
        )

        self.stale_request = cs(
            Request(
                pk=222,
                budget=self.budget_stale,
                reason="Alter Antrag",
                net_single_amount="100.00",
            )
        )
        self.budget_stale.date = date.today() - timedelta(days=90)
        cs(self.budget_stale)

        self.recipient = cs(Recipient(name="Muster Recipient", iban="1", bic="BYLADEMX", bankname="Musterbank"))

        self.receivedinvoice_event = cs(
            ReceivedInvoice(
                annual_plan=self.annual_plan,
                internal_booking_number="101",
                name="eingehende Musterrechnung",
                date=date.today(),
                general_ledger_account=self.general_ledger_account,
                vendor=self.vendor,
                requester=self.requester_event,
                vendor_invoice_number="RE0010",
                reference="Test Verwendungszweck",
                electronic_invoice=True,
            )
        )

        self.stale_receivedinvoice = cs(
            ReceivedInvoice(
                pk=1001,
                annual_plan=self.annual_plan,
                internal_booking_number="102",
                name="eingehende Musterrechnung",
                status="new",
                general_ledger_account=self.general_ledger_account,
                vendor=self.vendor,
                requester=self.requester_event,
                vendor_invoice_number="RE0011",
                reference="Test Verwendungszweck",
                electronic_invoice=True,
            )
        )
        self.stale_receivedinvoice.date = date.today() - timedelta(days=60)
        cs(self.stale_receivedinvoice)

        self.person_with_transponder = cs(
            Person(
                name="Nadine Musterreferentin",
                role="Referentin für Musterangelegenheiten",
            )
        )

        self.person = cs(
            Person(
                name="Jo Notranspondero",
                role="Referat für interne Tests",
            )
        )

        self.transponder = cs(
            Transponder(
                transponder_id="XX1234XX",
                carrier=self.person_with_transponder,
            )
        )

        self.locking_authorization_group = cs(LockingAuthorizationGroup(name="Musterschließgruppe"))

        self.locking_authorization_without_group = cs(
            LockingAuthorization(
                name="Musterschließrecht 1",
                lock_id="001",
                group="EG.001",
                is_public=True,
            )
        )

        self.locking_authorization_with_group = cs(
            LockingAuthorization(
                name="Musterschließrecht 2",
                lock_id="002",
                group="EG.002",
                internal_group=self.locking_authorization_group,
                is_public=True,
            )
        )

        self.transponder.locking_authorizations.add(self.locking_authorization_without_group)
        self.transponder.locking_authorizations.add(self.locking_authorization_with_group)
        cs(self.transponder)

        self.receivedinvoice = cs(
            ReceivedInvoice(
                annual_plan=self.annual_plan,
                internal_booking_number="001/77",
                name="eingehende Musterrechnung",
                date=date.today(),
                general_ledger_account=self.general_ledger_account,
                vendor=self.vendor,
                requester=self.requester_haus,
                vendor_invoice_number="RE0001",
                reference="Test Verwendungszweck",
                electronic_invoice=True,
            )
        )
