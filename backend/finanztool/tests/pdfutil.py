import io

import fitz
from django.http import FileResponse
from pdfrw import PdfReader


def file_response_to_document(data: FileResponse):
    if isinstance(data, FileResponse):
        file_handle = io.BytesIO()
        for b in data.streaming_content:
            file_handle.write(b)
        file_handle.seek(0)
    return fitz.open(stream=file_handle, filetype="application/pdf")


class PDF:
    def __init__(self, data):
        if isinstance(data, FileResponse):
            file_handle = io.BytesIO()
            for b in data.streaming_content:
                file_handle.write(b)
            file_handle.seek(0)
            data = file_handle.read()
        self._reader = PdfReader(fdata=data)
        if not self._reader.pages:
            raise Exception("Not a proper PDF")

    def num_pages(self):
        return len(self._reader.pages)
