import types
import unittest.mock
from datetime import date
from decimal import Decimal

from django.contrib.admin import ModelAdmin
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ValidationError
from django.http import HttpRequest
from django.test import TestCase
from freezegun import freeze_time

from finanztool.core.models import (
    AnnualPlan,
    Budget,
    BudgetReceiver,
    Offer,
    ReceivedInvoice,
    Request,
    Requester,
    ZeroSumBooking,
)
from finanztool.tests import util


# Create your tests here.
class RequestTest(TestCase):
    def setUp(self):
        self.data = util.TestData()
        # Login as superuser
        self.client.force_login(self.data.superuser)

    def test_calculate_values(self):
        # create request
        budget = util.cs(
            Budget(
                date=date(2023, 1, 3),
                annual_plan=self.data.annual_plan,
                requester=self.data.requester_haus,
                name="Musterantrag",
                target=Decimal(-600),
            )
        )
        values = budget.get_values()
        self.assertEqual(values.actual, 0)
        self.assertEqual(values.expected, -600)

    def test_request_invoice_link(self):
        # create request
        budget = util.cs(
            Budget(
                date=date(2023, 1, 3),
                annual_plan=self.data.annual_plan,
                requester=self.data.requester_haus,
                name="Musterantrag",
                target=Decimal(-600),
            )
        )
        request = util.cs(
            Request(
                budget=budget,
                reason="Weil halt.",
            )
        )

        for i in range(3):
            util.cs(
                Offer(
                    request=request,
                    bidder="Firma {}".format(i),
                    net=Decimal(600 + i * 100),
                    is_desired=i == 0,
                    offer_file=util.file("one_page.pdf"),
                )
            )

        # TODO
        # request.status = model_request.FINISHED
        # with self.assertRaises(ValidationError):
        #    request.clean()

        budget.status = "approved"
        util.cs(budget)
        util.cs(request)

        # create invoice with wrong request
        invoice = ReceivedInvoice(
            requester=self.data.requester_event,
            annual_plan=self.data.annual_plan,
            general_ledger_account=self.data.general_ledger_account,
            internal_booking_number="00{}".format(i + 1),
            name="Musterbestellung",
            reason="Weil halt. Wichtig und so.",
            date=date(2023, 1, 5),
            vendor=self.data.vendor,
            vendor_invoice_number="100",
            reference="Re. 100",
            budget=budget,
            electronic_invoice=False,
        )
        with self.assertRaises(ValidationError):
            util.cs(invoice)

        invoice.requester = self.data.requester_haus
        util.cs(invoice)
        request.refresh_from_db()
        # TODO
        # self.assertEqual(request.status, model_request.FINISHED)

        self.assertTrue(budget.is_locked())

    def test_za3_conditions(self):
        # create request
        budget = util.cs(
            Budget(
                date=date(2023, 1, 3),
                annual_plan=self.data.annual_plan,
                requester=self.data.requester_haus,
                name="Musterantrag",
                target=Decimal(-600),
            )
        )
        request = util.cs(
            Request(
                budget=budget,
            )
        )

        # too high net_single_amount
        request.net_single_amount = Decimal(700)
        with self.assertRaises(ValidationError):
            request.clean()
        request.refresh_from_db()

        # no offers, no reasoning
        request.za3_requested = True
        with self.assertRaises(ValidationError):
            request.clean()
        request.refresh_from_db()

        for i in range(3):
            offer = util.cs(
                Offer(
                    request=request,
                    bidder="Firma {}".format(i),
                    net=Decimal(600 + i * 100),
                    is_desired=i == 0,
                    offer_file=util.file("one_page.pdf"),
                )
            )

        # no reasoning
        util.admin_action(self.client, "set_za3_requested", [request])
        request.refresh_from_db()
        self.assertFalse(request.za3_requested)

        request.reason = "Hamma immer scho so gmacht"
        util.cs(request)
        util.admin_action(self.client, "set_za3_requested", [request])
        request.refresh_from_db()
        self.assertTrue(request.za3_requested)

        offer.is_desired = True
        util.cs(offer)
        # to many wanted offers
        request.refresh_from_db()
        with self.assertRaises(ValidationError):
            request.clean()

        offer.delete()
        # to few offers
        request.refresh_from_db()
        with self.assertRaises(ValidationError):
            request.clean()

    # @freeze_time("2078-01-01")
    # def test_move_to_new_annual_plan(self) -> None:
    #     # create annual_plan 2078
    #     self.annual_plan_2078 = util.cs(AnnualPlan(year=2078, budget_allocation=Decimal("1000.00")))
    #     self.annual_plan_2079 = util.cs(AnnualPlan(year=2079, budget_allocation=Decimal("1000.00")))
    #     # create requester to update to
    #     self.requester_update = util.cs(
    #         Requester(
    #             fond=self.data.fond_haus,
    #             name="Rücklagen - Ausgaben Vorjahr",
    #             cost_center="K05T5TELL4",
    #             password=make_password("1234"),
    #         )
    #     )
    #     self.requester_update.user.add(self.data.user)
    #     util.cs(self.requester_update)
    #     # create budgets for requesters in annual_plans
    #     util.cs(
    #         BudgetReceiver(receiver=self.requester_update, annual_plan=self.annual_plan_2078, planned=Decimal(-1000))
    #     )
    #     util.cs(
    #         BudgetReceiver(receiver=self.requester_update, annual_plan=self.annual_plan_2079, planned=Decimal(-1000))
    #     )
    #     util.cs(
    #         BudgetReceiver(receiver=self.data.requester_haus, annual_plan=self.annual_plan_2078, planned=Decimal(-1000))
    #     )
    #     util.cs(
    #         BudgetReceiver(receiver=self.data.requester_haus, annual_plan=self.annual_plan_2079, planned=Decimal(-1000))
    #     )

    #     # create moveable budget
    #     self.budget_moveable = util.cs(
    #         Budget(
    #             date=date(2078, 1, 3),
    #             annual_plan=self.annual_plan_2078,
    #             requester=self.data.requester_haus,
    #             name="moveable",
    #             target=Decimal(-600),
    #         )
    #     )
    #     request = util.cs(
    #         Request(
    #             budget=self.budget_moveable,
    #             reason="Weil halt.",
    #         )
    #     )
    #     # create closed budget
    #     self.budget_closed = util.cs(
    #         Budget(
    #             date=date(2023, 1, 3),
    #             annual_plan=self.data.annual_plan,
    #             requester=self.data.requester_haus,
    #             name="Musterantrag",
    #             target=Decimal(-600),
    #             status="closed",
    #         )
    #     )
    #     # create budget with subbudget
    #     self.budget_locked1 = util.cs(
    #         Budget(
    #             date=date(2023, 1, 3),
    #             annual_plan=self.data.annual_plan,
    #             requester=self.data.requester_haus,
    #             name="Musterantrag",
    #             target=Decimal(-600),
    #         )
    #     )
    #     util.cs(
    #         Budget(
    #             date=date(2023, 1, 3),
    #             annual_plan=self.data.annual_plan,
    #             requester=self.data.requester_haus,
    #             name="Musterantrag 2",
    #             target=Decimal(-300),
    #             parent=self.budget_locked1,
    #         )
    #     )
    #     # create budget with booking
    #     self.budget_locked2 = util.cs(
    #         Budget(
    #             date=date(2023, 1, 3),
    #             annual_plan=self.data.annual_plan,
    #             requester=self.data.requester_haus,
    #             name="Musterantrag",
    #             target=Decimal(-600),
    #         )
    #     )
    #     util.cs(
    #         ZeroSumBooking(
    #             requester=self.data.requester_haus,
    #             name="Musterbuchung",
    #             annual_plan=self.data.annual_plan,
    #             internal_booking_number="001",
    #             budget=self.budget_locked2,
    #         )
    #     )
    #     # create old budget
    #     self.budget_old: Budget = util.cs(
    #         Budget(
    #             date=date(2023, 1, 3),
    #             annual_plan=self.data.annual_plan,
    #             requester=self.data.requester_haus,
    #             name="Old",
    #             target=Decimal(-600),
    #         )
    #     )
    #     # create future request
    #     self.budget_future: Budget = util.cs(
    #         Budget(
    #             date=date(2079, 1, 3),
    #             annual_plan=self.annual_plan_2079,
    #             requester=self.data.requester_haus,
    #             name="Future",
    #             target=Decimal(-600),
    #         )
    #     )
    #     # create current year request
    #     self.budget_current_year: Budget = util.cs(
    #         Budget(
    #             date=date(2078, 1, 3),
    #             annual_plan=self.annual_plan_2078,
    #             requester=self.data.requester_haus,
    #             name="Moveable",
    #             target=Decimal(-600),
    #         )
    #     )
    #     # create fallback requester
    #     self.requester_fallback = util.cs(
    #         Requester(
    #             fond=self.data.fond_haus, name="XXUnbekannt", cost_center="K05T5TELL4", password=make_password("1234")
    #         )
    #     )
    #     self.requester_fallback.user.add(self.data.user)
    #     util.cs(self.requester_fallback)

    #     admin = unittest.mock.create_autospec(ModelAdmin)
    #     admin.message_user = types.MethodType(self.my_message_user, self)

    #     # action
    #     util.admin_action(
    #         self.client,
    #         "move_to_new_year",
    #         [
    #             self.budget_moveable,
    #             self.budget_closed,
    #             self.budget_future,
    #             self.budget_locked1,
    #             self.budget_locked2,
    #             self.budget_current_year,
    #         ],
    #     )
    #     # move_to_new_year(admin, HttpRequest(), {self.budget_moveable, self.budget_closed})

    #     # works
    #     self.assertEqual(self.budget_moveable.annual_plan.year, "2079")
    #     self.assertEqual(self.budget_moveable.requester, self.requester_update)
    #     self.assertIn("Antrag in Jahresplan 2079 verschoben", self.budget_moveable.note)
    #     self.assertIn('und Antragssteller auf "Rücklagen - Ausgaben Vorjahr" geändert.', self.budget_moveable.note)

    #     # reject locked budgets
    #     self.assertEqual(self.budget_locked1.annual_plan.year, "2078")
    #     self.assertEqual(self.budget_locked1.requester, self.data.requester_haus)
    #     self.assertEqual(self.budget_locked1.note, "")
    #     self.assertEqual(self.budget_locked2.annual_plan.year, "2078")
    #     self.assertEqual(self.budget_locked2.requester, self.data.requester_haus)
    #     self.assertEqual(self.budget_locked2.note, "")

    #     # reject old budget
    #     self.assertEqual(self.budget_old.annual_plan.year, "2023")
    #     self.assertEqual(self.budget_old.requester, self.data.requester_haus)
    #     self.assertEqual(self.budget_old.note, "")

    #     # reject future budget
    #     self.assertEqual(self.budget_future.annual_plan.year, "2079")
    #     self.assertEqual(self.budget_future.requester, self.data.requester_haus)
    #     self.assertEqual(self.budget_future.note, "")

    #     # current year
    #     self.budget_moveable.delete()
    #     self.budget_locked1.delete()
    #     self.budget_locked2.delete()
    #     self.budget_old.delete()
    #     self.budget_future.delete()
    #     self.annual_plan_2079.delete()

    #     self.assertEqual(self.budget_current_year.annual_plan.year, "2078")
    #     self.assertEqual(self.budget_current_year.requester, self.data.requester_haus)
    #     self.assertEqual(self.budget_current_year.note, "")

    def my_message_user(self, request: HttpRequest, message: str, level, tags: str):
        self.assertIsNone(level)
        pass
