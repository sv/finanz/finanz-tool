from datetime import date
from decimal import Decimal

from django.http import FileResponse
from django.test import TestCase
from django.urls import reverse
from freezegun import freeze_time

from finanztool.core.models import (
    Budget,
    Offer,
    ReceivedAmount,
    ReceivedInvoice,
    Request,
    Requester,
)
from finanztool.tests import lprmock, pdfutil, util


@freeze_time("2023-01-06")
class E2ETest(TestCase):
    def assert_in_history(self, obj, content):
        resp = self.client.get(reverse("admin:core_" + type(obj)()._meta.model_name + "_history", args=(obj.pk,)))
        self.assertIn(content, resp.content)

    def setUp(self):
        self.data = util.TestData()
        # Login as superuser
        self.client.force_login(self.data.superuser)

    def test_e2e_invoice_received(self):
        # create budget an request
        budget = util.cs(
            Budget(
                date=date(2023, 1, 3),
                annual_plan=self.data.annual_plan,
                requester=self.data.requester_haus,
                name="Musterantrag",
                target=Decimal(-600),
            )
        )
        request = util.cs(
            Request(
                budget=budget,
                reason="Weil halt.",
            )
        )

        for i in range(3):
            util.cs(
                Offer(
                    request=request,
                    bidder="Firma {}".format(i),
                    net=Decimal(600 + i * 100),
                    is_desired=i == 0,
                    offer_file=util.file("one_page.pdf"),
                )
            )
        util.admin_action(self.client, "set_za3_requested", [request])
        self.assert_in_history(request, b"ZA3 requested")
        request.refresh_from_db()
        self.assertTrue(request.za3_requested)
        self.assertTrue(request.contract_award_notice_file)

        response = util.admin_action(self.client, "print_contract_award_notice", [request])
        self.assertEqual(response["content-type"], "application/pdf")
        self.assertTrue(isinstance(response, FileResponse))
        pdf = pdfutil.PDF(response)
        self.assertEqual(pdf.num_pages(), 2 + 3 * 1)  # 2 from form + 3 offer files

        util.admin_action(self.client, "set_approved", [budget])
        budget.refresh_from_db()
        self.assertEqual(budget.status, "approved")
        self.assert_in_history(budget, b"approved")

        # create invoices
        invoices = []
        for i, req in enumerate((self.data.requester_event, self.data.requester_haus)):
            invoice = util.cs(
                ReceivedInvoice(
                    requester=req,
                    annual_plan=self.data.annual_plan,
                    general_ledger_account=self.data.general_ledger_account,
                    internal_booking_number="00{}".format(i + 1),
                    name="Musterbestellung",
                    reason="Weil halt. Wichtig und so.",
                    date=date(2023, 1, 5),
                    vendor=self.data.vendor,
                    vendor_invoice_number="100" + req.name,
                    reference="Re. 100" + req.name,
                    electronic_invoice=i == 0,
                    invoice_file=util.file("one_page.pdf"),
                )
            )
            invoices.append(invoice)

            amount = ReceivedAmount(
                gross=Decimal("500.00") if i == 1 else Decimal("595.00"),
                tax=Decimal("1.19"),
                invoice=invoice,
            )
            amount.clean()
            amount.save()

        # assign budget to invoice
        invoices[1].budget = budget
        util.cs(invoices[1])
        request.refresh_from_db()
        # TODO: check that request has invoice now

        # basic budget checks
        self.assertEqual(self.data.event_budget_receiver.calc_current_balance(), Decimal("-500.00"))
        self.assertEqual(self.data.event_budget_receiver.remaining_budget(), Decimal("-500.00"))
        self.assertEqual(self.data.budget_receiver.calc_current_balance(), Decimal("-500.00"))
        self.assertEqual(self.data.budget_receiver.remaining_budget(), Decimal("500.00"))

        # Pass invoice through states using actions as in real life
        for invoice in invoices:
            self.assertIn(b"erfolgreich", util.admin_action(self.client, "set_invoices_payable", [invoice]).content)
            self.assert_in_history(invoice, b"payable")
            invoice.refresh_from_db()
            self.assertEqual(invoice.status, "payable")
            self.assertIn(b"erfolgreich", util.admin_action(self.client, "finish_invoices", [invoice]).content)
            self.assert_in_history(invoice, b"done")
            invoice.refresh_from_db()
            self.assertEqual(invoice.status, "done")
            self.assertTrue(invoice.form_file)

        with lprmock.PrintMock() as mock:
            util.admin_action(self.client, "print_invoices", [invoices[0]])
            self.assertEqual(len(mock.jobs), 2)
            self.assertIn("Original", mock.jobs[0].title)
            self.assertEqual(2, mock.jobs[0].pdf.num_pages())  # form + single page invoice
            self.assertIn("Kopie", mock.jobs[1].title)
            self.assertEqual(2, mock.jobs[1].pdf.num_pages())  # form + single page invoice (copy)
            mock.reset()

            util.admin_action(self.client, "print_invoices", [invoices[1]])
            self.assertEqual(len(mock.jobs), 2)
            self.assertIn("Formular", mock.jobs[0].title)
            self.assertEqual(1, mock.jobs[0].pdf.num_pages())  # only the form should be printed
            self.assertIn("Kopie", mock.jobs[1].title)
            self.assertEqual(2, mock.jobs[1].pdf.num_pages())  # form + single page invoice (copy)

        for invoice in invoices:
            util.admin_action(self.client, "mark_invoice_as_sent", [invoice])
            self.assert_in_history(invoice, b"sent")
            invoice.refresh_from_db()
            self.assertEqual(invoice.status, "sent")

            # Try to update the SAP status, should not change anything
            util.admin_action(self.client, "update_sap_status", [invoice])
            invoice.refresh_from_db()
            self.assertEqual(invoice.status, "sent")
