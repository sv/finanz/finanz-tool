from unittest.mock import patch

from finanztool.tests import pdfutil


class Job:
    def __init__(self, file: bytes, profile, title):
        self.pdf = pdfutil.PDF(file)
        self.profile: str = profile
        self.title: str = title


class PrintMock:
    """Overwrites lpr function with a mocked version when used as context manager.
    Print jobs are stored to self.jobs instead of being printed"""

    def __init__(self):
        self.jobs: list[Job] = []
        self._patcher = None

    def __call__(self, *args, **kwargs):
        self.jobs.append(Job(*args, **kwargs))

    def reset(self):
        self.jobs = []

    def __enter__(self):
        self._patcher = patch("finanztool.pdf.printing.lpr._print_file", self)
        self._patcher.__enter__()
        return self

    def __exit__(self, *args):
        self._patcher.__exit__(*args)
