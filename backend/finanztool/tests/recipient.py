from django.core.exceptions import ValidationError
from django.test import TestCase

from finanztool.core.models import Recipient


# Create your tests here.
class RecipientTest(TestCase):
    def test_iban_validation(self):
        r = Recipient(iban="1NV4L1D1b4N", bic="BYLADEM1", bankname="Bank", name="Bla")
        with self.assertRaisesMessage(ValidationError, "Not a valid IBAN"):
            r.full_clean()
        r.iban = "DE12345678912345678900"
        with self.assertRaisesMessage(ValidationError, "IBAN Checksum invalid"):
            r.full_clean()
        r.iban = "DE12 3456 7891 2345 6789 00"
        with self.assertRaisesMessage(ValidationError, "IBAN Checksum invalid"):
            r.full_clean()
        r.iban = "DE82500105178152524836"  # this is random, don't send money there
        r.full_clean()
        r.iban = "DE82 5001 0517 8152 5248 36"
        r.full_clean()
