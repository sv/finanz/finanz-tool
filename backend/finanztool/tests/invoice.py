from datetime import date
from decimal import Decimal

from django.test import TestCase
from django.urls import reverse

from finanztool.core.models import Amount, ReceivedAmount, ReceivedInvoice
from finanztool.tests import util


# Create your tests here.
class InvoiceTest(TestCase):
    def setUp(self):
        self.data = util.TestData()
        # Login as superuser
        self.client.force_login(self.data.superuser)

    def test_amount_net_calculation(self):
        amount = util.cs(
            ReceivedAmount(
                gross=Decimal("119.00"),
                tax=Decimal("1.19"),
                net=Decimal("0.00"),  # dummy, overridden in save
                invoice=self.data.receivedinvoice,
            )
        )
        self.assertEqual(amount.net, Decimal("100.00"))

    def test_amount_taxes(self):
        amount = util.cs(
            ReceivedAmount(
                gross=Decimal("100.00"),
                tax=Decimal("1.00"),
                net=Decimal("0.00"),  # dummy, overridden in save
                invoice=self.data.receivedinvoice,
            )
        )
        self.assertEqual(amount.tax_string_short(), "0%")
        self.assertEqual(amount.tax_code(), "V0")

        amount.tax = Decimal("1.19")
        util.cs(amount)
        self.assertEqual(amount.tax_string_short(), "19%")
        self.assertEqual(amount.tax_code(), "V4")

        amount.tax = Decimal("1.07")
        util.cs(amount)
        self.assertEqual(amount.tax_string_short(), "7%")
        self.assertEqual(amount.tax_code(), "V2")

        amount.tax = Decimal("1.16")
        util.cs(amount)
        self.assertEqual(amount.tax_string_short(), "16%")
        self.assertEqual(amount.tax_code(), "V1")

        amount.tax = Decimal("1.05")
        util.cs(amount)
        self.assertEqual(amount.tax_string_short(), "5%")
        self.assertEqual(amount.tax_code(), "V6")

    def test_amount_format(self):
        amount = util.cs(
            ReceivedAmount(
                gross=Decimal("107.00"),
                tax=Decimal("1.07"),
                net=Decimal("0.00"),  # dummy, overridden in save
                invoice=self.data.receivedinvoice,
            )
        )

        self.assertEqual(amount.format_tax_amount(), "€ 7,00")
        self.assertEqual(amount.format_gross(), "€ 107,00")
        self.assertEqual(amount.format_net(), "€ 100,00")
        self.assertEqual(
            Amount.calculate_total_in_words(amount.gross + amount.tax),
            "einhundertacht 7/100",
        )

    def test_amount_invoice_link(self):
        amount = util.cs(
            ReceivedAmount(
                gross=Decimal("107.00"),
                tax=Decimal("1.07"),
                net=Decimal("0.00"),  # dummy, overridden in save
                invoice=self.data.receivedinvoice,
            )
        )

        self.assertEqual(amount.invoice, self.data.receivedinvoice)

    def test_total_in_words_validation_validation(self):
        # STILL WIP
        received_data = {
            "annual_plan": self.data.annual_plan.pk,
            "internal_booking_number": 999,
            "name": "Received Muster",
            "reception_date": date.today(),
            "general_ledger_account": self.data.general_ledger_account.pk,
            "vendor": self.data.vendor.pk,
            "requester": self.data.requester_event.pk,
            "vendor_invoice_number": "RE0055",
            "reference": "Test Verwendungszweck",
            "electronic_invoice": True,
            "receivedamount_set-TOTAL_FORMS": 3,
            "receivedamount_set-INITIAL_FORMS": 0,
            "receivedamount_set-MIN_NUM_FORMS": 0,
            "receivedamount_set-MAX_NUM_FORMS": 1000,
            "receivedamount_set-0-gross": "1000.00",
            "receivedamount_set-0-tax": 1.07,
            "inventory_set-TOTAL_FORMS": 0,
            "inventory_set-INITIAL_FORMS": 0,
        }

        response = self.client.post(reverse("admin:core_receivedinvoice_add"), received_data)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Betrag in Worten ab 1000", response.content)

        # wrong inwords
        received_data["total_in_words"] = "this is wrong"
        response = self.client.post(reverse("admin:core_receivedinvoice_add"), received_data, follow=True)
        self.assertIn(b"Betrag in Worten passt nicht zum Gesamtbetrag", response.content)

        # correct inwords
        received_data["total_in_words"] = "eintausend 0/100"
        response = self.client.post(reverse("admin:core_receivedinvoice_add"), received_data, follow=True)
        self.assertIn(b"wurde erfolgreich hinzugef", response.content)

        issued_data = {
            "annual_plan": self.data.annual_plan.pk,
            "internal_booking_number": 999,
            "name": "Received Muster",
            "reception_date": date.today(),
            "general_ledger_account": self.data.general_ledger_account.pk,
            "vendor": self.data.vendor.pk,
            "requester": self.data.requester_event.pk,
            "vendor_invoice_number": "RE0055",
            "reference": "Test Verwendungszweck",
            "bkz": "9999.9999.9999",
            "monition": "25",
            "recipient": self.data.recipient.pk,
            "issuedamount_set-TOTAL_FORMS": 3,
            "issuedamount_set-INITIAL_FORMS": 0,
            "issuedamount_set-MIN_NUM_FORMS": 0,
            "issuedamount_set-MAX_NUM_FORMS": 1000,
            "issuedamount_set-0-gross": "1000.00",
            "issuedamount_set-0-tax": 1.07,
            "inventory_set-TOTAL_FORMS": 0,
            "inventory_set-INITIAL_FORMS": 0,
        }

        response = self.client.post(reverse("admin:core_issuedinvoice_add"), issued_data)
        self.assertIn(b"Betrag in Worten ab 1000", response.content)

        # wrong inwords
        issued_data["total_in_words"] = "this is wrong"
        response = self.client.post(reverse("admin:core_issuedinvoice_add"), issued_data, follow=True)
        self.assertIn(b"Betrag in Worten passt nicht zum Gesamtbetrag", response.content)

        issued_data["total_in_words"] = "eintausend 0/100"
        response = self.client.post(reverse("admin:core_issuedinvoice_add"), issued_data, follow=True)
        self.assertIn(b"wurde erfolgreich hinzugef", response.content)
