import json

from rest_framework.fields import DateField
from rest_framework.test import APITestCase

from finanztool.tests import util
from finanztool.tests.api.budget import assertHasAttrs


class BookingTest(APITestCase):
    def setUp(self):
        self.data = util.TestData()

    def login(self, superuser: bool = False):
        if superuser:
            self.client.force_login(self.data.superuser)
        else:
            self.client.force_login(self.user_haus)

    def test_list_stale_bookings_superuser(self):
        self.login(True)

        response = self.client.get("/api/bookings/stale/")

        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            json.loads(response.content),
            [
                dict(
                    pk=self.data.stale_receivedinvoice.requester.pk,
                    name=self.data.stale_receivedinvoice.requester.name,
                    items=[
                        dict(
                            unique_id=self.data.stale_receivedinvoice.unique_id(),
                            date=DateField().to_representation(self.data.stale_receivedinvoice.date),
                            display_name=self.data.stale_receivedinvoice.display_name(),
                            requester=self.data.stale_receivedinvoice.requester.name,
                            link="/admin/core/receivedinvoice/{}/change".format(self.data.stale_receivedinvoice.pk),
                            status=dict(progress=0.5, text="Rechnung eingegangen"),
                            booked_total="0.00",
                        )
                    ],
                )
            ],
        )

    def test_list_recent_bookings_superuser(self):
        self.login(True)

        response = self.client.get("/api/bookings/recent/")

        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            json.loads(response.content),
            [
                dict(
                    pk=self.data.receivedinvoice_event.requester.pk,
                    name=self.data.receivedinvoice_event.requester.name,
                    items=[
                        dict(
                            unique_id=self.data.receivedinvoice_event.unique_id(),
                            date=DateField().to_representation(self.data.receivedinvoice_event.date),
                            display_name=self.data.receivedinvoice_event.display_name(),
                            requester=self.data.receivedinvoice_event.requester.name,
                            link="/admin/core/receivedinvoice/{}/change".format(self.data.receivedinvoice_event.pk),
                            status=dict(progress=0.5, text="Rechnung eingegangen"),
                            booked_total="0.00",
                        )
                    ],
                ),
                dict(
                    pk=self.data.receivedinvoice.requester.pk,
                    name=self.data.receivedinvoice.requester.name,
                    items=[
                        dict(
                            unique_id=self.data.receivedinvoice.unique_id(),
                            date=DateField().to_representation(self.data.receivedinvoice.date),
                            display_name=self.data.receivedinvoice.display_name(),
                            requester=self.data.receivedinvoice.requester.name,
                            link="/admin/core/receivedinvoice/{}/change".format(self.data.receivedinvoice.pk),
                            status=dict(progress=0.5, text="Rechnung eingegangen"),
                            booked_total="0.00",
                        )
                    ],
                ),
            ],
        )

    # todo: search for other fields too
    def test_list_bookings_superuser(self):
        self.login(True)

        response = self.client.get("/api/bookings/?search=RE001")

        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            json.loads(response.content),
            [
                dict(
                    unique_id=self.data.receivedinvoice_event.unique_id(),
                    date=DateField().to_representation(self.data.receivedinvoice_event.date),
                    display_name=self.data.receivedinvoice_event.display_name(),
                    requester=self.data.receivedinvoice_event.requester.name,
                    link="/admin/core/receivedinvoice/{}/change".format(self.data.receivedinvoice_event.pk),
                    status=dict(progress=0.5, text="Rechnung eingegangen"),
                    booked_total="0.00",
                ),
                dict(
                    unique_id=self.data.stale_receivedinvoice.unique_id(),
                    date=DateField().to_representation(self.data.stale_receivedinvoice.date),
                    display_name=self.data.stale_receivedinvoice.display_name(),
                    requester=self.data.stale_receivedinvoice.requester.name,
                    link="/admin/core/receivedinvoice/{}/change".format(self.data.stale_receivedinvoice.pk),
                    status=dict(progress=0.5, text="Rechnung eingegangen"),
                    booked_total="0.00",
                ),
            ],
        )

    def test_get_stats(self):
        self.login(True)

    def test_noaccess(self):
        self.client.force_login(self.data.user_no_requesters)

        response = self.client.get("/api/bookings/stale/")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(json.loads(response.content), [])

        response = self.client.get("/api/requests/stale/")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(json.loads(response.content), [])
