import json
from unittest.mock import MagicMock

from django.contrib.auth.models import AnonymousUser
from django.http import HttpRequest
from django.test import TestCase
from rest_framework.fields import DateField
from rest_framework.request import Request
from rest_framework.test import APITestCase

from finanztool.api.querysets import get_fond_queryset, get_requester_queryset
from finanztool.core.models import Fond, Requester
from finanztool.tests import util


class QuerysetTest(TestCase):
    def setUp(self):
        self.data = util.TestData()

    def test_get_requester_queryset(self):
        request = Request(request=HttpRequest())
        request.user = self.data.superuser
        self.assertListEqual(list(get_requester_queryset(request)), list(Requester.objects.all()))

        request.user = self.data.user
        self.assertListEqual(list(get_requester_queryset(request)), list(Requester.objects.filter(user=self.data.user)))

        request.user = AnonymousUser()
        self.assertListEqual(list(get_requester_queryset(request)), [])

    def test_get_fond_queryset(self):
        request = Request(request=HttpRequest())
        request.user = self.data.superuser
        self.assertListEqual(list(get_fond_queryset(request)), list(Fond.objects.all().distinct()))

        request.user = self.data.user
        self.assertListEqual(
            list(get_fond_queryset(request).values_list("pk")),
            list(Requester.objects.filter(user=self.data.user).values_list("fond").distinct()),
        )

        request.user = AnonymousUser()
        self.assertListEqual(list(get_fond_queryset(request)), [])
