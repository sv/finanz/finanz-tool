from datetime import date, timedelta, timezone
from decimal import Decimal
from json import dumps, loads

from django.contrib.admin.models import ADDITION, CHANGE, DELETION, LogEntry
from django.contrib.contenttypes.models import ContentType
from rest_framework.fields import DateField, DateTimeField
from rest_framework.test import APITestCase

from finanztool.api.views.misc import (
    LogSerializer,
    get_absolute_deviation,
    get_relative_deviation,
)
from finanztool.core.models import BudgetReceiver, Fond, SAPBooking
from finanztool.core.models.util import RecursiveValueCache
from finanztool.tests import util


class BookingTest(APITestCase):
    def setUp(self):
        self.data = util.TestData()

    def login(self, superuser: bool = False):
        if superuser:
            self.client.force_login(self.data.superuser)
        else:
            self.client.force_login(self.user_haus)

    def test_list_stats(self):
        self.login(True)

        self.sap_booking = util.cs(
            SAPBooking(
                fi_belegnummer="FI0001",
                position_fi_belegnr="Belegposition",
                belegart="Belegart",
                buchungsdatum=date.today() - timedelta(days=5),
            )
        )

        response = self.client.get("/api/misc/stats/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["sap"], DateField().to_representation(self.sap_booking.buchungsdatum))
        # todo: add real testing case for request_processing_time
        self.assertEqual(response.data["request_processing_time"], 0)
        self.assertListEqual(
            loads(response.content)["budget_utilization"],
            [
                dict(
                    name=self.data.fond_event.name,
                    labels=["Megaparty", "Miniparty"],
                    relative_values=["0.00", "-1.00"],
                    absolute_values=["-1000.00", "-400.00"],
                ),
                dict(
                    name=self.data.fond_haus.name,
                    labels=["Fachschaft Bergbau"],
                    relative_values=["1.00"],
                    absolute_values=["1000.00"],
                ),
            ],
        )
        # todo: test invoices and requests by status

    def test_logserializer_get_action(self):
        self.event = util.cs(
            LogEntry(
                user=self.data.user,
                action_flag=ADDITION,
                content_type=ContentType.objects.filter(model="receivedinvoice", app_label="core")[0],
                object_repr="Object representation",
                object_id=self.data.receivedinvoice.pk,
            )
        )
        self.assertEqual(LogSerializer().get_action(self.event), "Neu hinzugefügt")

        self.event.action_flag = DELETION
        self.assertEqual(LogSerializer().get_action(self.event), "Gelöscht")

        self.event.action_flag = 304503
        self.assertEqual(LogSerializer().get_action(self.event), "Sonstiges")

        self.event.action_flag = CHANGE
        self.assertEqual(LogSerializer().get_action(self.event), "Geändert")

        self.event.change_message = dumps([{"changed": {"fields": []}}])
        self.assertEqual(LogSerializer().get_action(self.event), "Geändert")

        self.event.change_message = dumps([{"changed": {"fields": ["reason"]}}])
        self.assertEqual(LogSerializer().get_action(self.event), "Feld reason geändert")

        self.event.change_message = dumps([{"changed": {"fields": ["reason", "total_in_words"]}}])
        self.assertEqual(LogSerializer().get_action(self.event), "Mehrere Felder geändert")

    def test_logserializer_get_name(self):
        # request type case
        self.event_request = util.cs(
            LogEntry(
                user=self.data.user,
                action_flag=CHANGE,
                content_type=ContentType.objects.filter(model="request", app_label="core")[0],
                object_repr="Object representation",
                object_id=self.data.request.pk,
            )
        )
        self.assertEqual(LogSerializer().get_name(self.event_request), self.data.request.budget.name)

        self.event = util.cs(
            LogEntry(
                user=self.data.user,
                action_flag=ADDITION,
                content_type=ContentType.objects.filter(model="receivedinvoice", app_label="core")[0],
                object_repr="Object representation",
                object_id=self.data.receivedinvoice.pk,
            )
        )
        self.assertEqual(LogSerializer().get_name(self.event), self.data.receivedinvoice.name)

    def test_list_logs(self):
        self.login(True)

        # addition case
        self.event = util.cs(
            LogEntry(
                user=self.data.user,
                action_flag=ADDITION,
                content_type=ContentType.objects.filter(model="receivedinvoice", app_label="core")[0],
                object_repr="Object representation",
                object_id=self.data.receivedinvoice.pk,
            )
        )

        response = self.client.get("/api/misc/logs/")

        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            response.data,
            [
                dict(
                    action_time=DateTimeField().to_representation(self.event.action_time),
                    user=self.event.user.get_full_name(),
                    name=self.data.receivedinvoice.name,
                    color=self.data.receivedinvoice.color(),
                    action="Neu hinzugefügt",
                    link="/admin/core/receivedinvoice/{}/change/".format(self.data.receivedinvoice.pk),
                    class_name="Eingehende Rechnung",
                )
            ],
        )

    def test_get_deviation(self):
        budget_receiver = BudgetReceiver(
            target=Decimal("0.00"),
            receiver=self.data.requester_event,
            annual_plan=self.data.annual_plan,
        )
        budget_receiver.get_values = lambda: RecursiveValueCache()
        self.assertEqual(get_relative_deviation(budget_receiver), Decimal("0.00"))
        self.assertEqual(get_absolute_deviation(budget_receiver), Decimal("0.00"))

        budget_receiver.target = Decimal("100.00")
        self.assertEqual(get_relative_deviation(budget_receiver), Decimal("-1.00"))
        self.assertEqual(get_absolute_deviation(budget_receiver), Decimal("-100.00"))

    def test_misc_noaccess(self):
        self.client.force_login(self.data.user_no_requesters)

        response = self.client.get("/api/misc/logs/")

        self.assertEqual(response.status_code, 404)

        response = self.client.get("/api/misc/stats/")

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.data,
            {
                "sap": "-",
                "request_processing_time": 0,
                "budget_utilization": [],
                "invoices_by_status": {
                    "labels": [
                        "Neu",
                        "Rechnung zahlbar",
                        "Erledigt",
                        "Versandt",
                        "SAP verbucht",
                        "Bezahlt",
                        "Storniert",
                    ],
                    "recent_values": [0, 0, 0, 0, 0, 0, 0],
                    "stale_values": [0, 0, 0, 0, 0, 0, 0],
                    "count_previous_year": 0,
                },
                "requests_by_status": {
                    "labels": ["Beantragt", "Genehmigt", "Ausgeschöpft", "Geschlossen"],
                    "recent_values": [0, 0, 0, 0],
                    "stale_values": [0, 0, 0, 0],
                    "count_previous_year": 0,
                },
            },
        )
