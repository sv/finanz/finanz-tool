import json
from datetime import date
from decimal import Decimal

from django.contrib.auth.models import User
from rest_framework.fields import DateField
from rest_framework.test import APITestCase

from finanztool.core.models import Budget, Offer, Request
from finanztool.tests import util


def assertHasAttrs(cls, obj, **kwargs):
    for k, v in kwargs.items():
        cls.assertTrue(hasattr(obj, k))
        cls.assertEqual(getattr(obj, k), v)


class BudgetTest(APITestCase):
    def setUp(self):
        self.data = util.TestData()

        # add request that should not be listed
        budget = util.cs(
            Budget(
                date=date(2023, 1, 3),
                annual_plan=self.data.annual_plan,
                requester=self.data.requester_haus,
                status="closed",
                name="Bereits abgeschlossener Antrag",
                target=Decimal("-123.45"),
            )
        )
        util.cs(
            Request(
                budget=budget,
                net_single_amount=Decimal("12.00"),
                reason="Zum Testen.",
            )
        )

        self.user_haus = util.cs(
            User(
                username="user_haus",
                password="pw",
                first_name="User",
                last_name="Haus",
            )
        )
        self.data.requester_haus.user.add(self.user_haus)

    def login(self, superuser: bool = False):
        if superuser:
            self.client.force_login(self.data.superuser)
        else:
            self.client.force_login(self.user_haus)

    def create_request(self, has_offers: bool = False) -> Request:
        """Creates a request that is not saved"""
        if has_offers:
            budget = Budget(
                date=date(2023, 1, 3),
                annual_plan=self.data.annual_plan,
                requester=self.data.requester_haus,
                status="requested",
                name="Musterantrag mit Angeboten",
                target=Decimal("-1123.45"),
            )
            return Request(
                budget=budget,
                net_single_amount=Decimal("543.21"),
                reason="Weil halt.",
            )
        else:
            budget = Budget(
                date=date(2023, 1, 3),
                annual_plan=self.data.annual_plan,
                requester=self.data.requester_haus,
                status="requested",
                name="Musterantrag",
                target=Decimal("-123.45"),
            )
            return Request(
                budget=budget,
                net_single_amount=Decimal("12.00"),
                reason="Weil halt.",
            )

    def create_offers(self, request: Request):
        return [
            Offer(
                request=request,
                bidder="Firma {}".format(i),
                net=Decimal(600 + i * 100),
                is_desired=i == 0,
                offer_file=util.file("one_page.pdf"),
            )
            for i in range(3)
        ]

    def test_requesters_superuser(self):
        self.login(True)
        response = self.client.get("/api/requesters/")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            response.data,
            [
                dict(
                    pk=obj.pk,
                    name=obj.name,
                    booking_type=obj.fond.booking_type,
                )
                for obj in (
                    self.data.requester_event_3,
                    self.data.requester_event_2,
                    self.data.requester_event,
                    self.data.requester_haus,
                )
            ],
        )

    def test_requesters_user(self):
        self.login(False)
        response = self.client.get("/api/requesters/")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            response.data,
            [
                dict(
                    pk=self.data.requester_haus.pk,
                    name=self.data.requester_haus.name,
                    booking_type=self.data.requester_haus.fond.booking_type,
                )
            ],
        )

    def test_requesters_requester_superuser(self):
        self.login(True)
        for obj in (self.data.requester_haus, self.data.requester_event):
            response = self.client.get(f"/api/requesters/{obj.pk}/")
            self.assertEqual(response.status_code, 200)
            self.assertDictEqual(
                response.data,
                dict(
                    pk=obj.pk,
                    name=obj.name,
                    booking_type=obj.fond.booking_type,
                ),
            )

    def test_requesters_requester_user(self):
        self.login(False)

        # positive response for our own
        obj = self.data.requester_haus
        response = self.client.get(f"/api/requesters/{obj.pk}/")
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.data,
            dict(
                pk=obj.pk,
                name=obj.name,
                booking_type=obj.fond.booking_type,
            ),
        )

        # negative response for other
        obj = self.data.requester_event
        response = self.client.get(f"/api/requesters/{obj.pk}/")
        self.assertEqual(response.status_code, 404)

    def test_e2e_create_budget_superuser(self):
        self.login(False)
        # create budget
        budgetCount = Budget.objects.count()
        requestCount = Request.objects.count()
        data = dict(
            name="ABX",
            requester=self.data.requester_haus.pk,
            target="-300.12",
        )
        response = self.client.post(
            "/api/budgets/",
            data,
        )
        self.assertEqual(response.status_code, 200)
        resp_create = response.data
        pk = response.data["pk"]
        # test if budget is created, but no request
        self.assertEqual(Budget.objects.count(), budgetCount + 1)
        self.assertEqual(Request.objects.count(), requestCount)
        assertHasAttrs(
            self,
            Budget.objects.get(pk=pk),
            name="ABX",
            requester=self.data.requester_haus,
            target=Decimal("-300.12"),
        )

        response = self.client.get(f"/api/budgets/{pk}/")
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(resp_create, response.data)

        response = self.client.patch(f"/api/budgets/{pk}/", dict(target="-312.32"))
        self.assertEqual(response.status_code, 200)
        assertHasAttrs(self, Budget.objects.get(pk=pk), target=Decimal("-312.32"))

        response = self.client.patch(f"/api/budgets/{pk}/", dict(request=dict(reason="Because")))

    def test_list_recent_requests_superuser(self):
        self.login(True)

        response = self.client.get("/api/requests/recent/")

        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            json.loads(response.content),
            [
                dict(
                    pk=self.data.request.budget.requester.pk,
                    name=self.data.request.budget.requester.name,
                    items=[
                        dict(
                            date=DateField().to_representation(self.data.request.budget.date),
                            display_name=self.data.request.budget.name,
                            requester=self.data.request.budget.requester.name,
                            link="/admin/core/request/{}/change".format(self.data.request.pk),
                            status=dict(progress=0.0, text="Antrag eingegangen"),
                            booked_total="-500.00",
                        )
                    ],
                )
            ],
        )

    def test_list_stale_requests_superuser(self):
        self.login(True)

        response = self.client.get("/api/requests/stale/")

        self.assertEqual(response.status_code, 200)
        self.assertListEqual(
            json.loads(response.content),
            [
                dict(
                    pk=self.data.stale_request.budget.requester.pk,
                    name=self.data.stale_request.budget.requester.name,
                    items=[
                        dict(
                            date=DateField().to_representation(self.data.stale_request.budget.date),
                            display_name=self.data.stale_request.budget.name,
                            requester=self.data.stale_request.budget.requester.name,
                            link="/admin/core/request/{}/change".format(self.data.stale_request.pk),
                            status=dict(progress=0.0, text="Antrag eingegangen"),
                            booked_total="-200.00",
                        )
                    ],
                )
            ],
        )

    def test_budget_noaccess(self):
        self.client.force_login(self.data.user_no_requesters)

        response = self.client.get("/api/requests/stale/")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, [])

        response = self.client.get("/api/requests/recent/")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, [])

        response = self.client.get("/api/requesters/")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, [])

        response = self.client.get("/api/budgets/1/")
        self.assertEqual(response.status_code, 404)

        response = self.client.get("/api/budgets/1/")
        self.assertEqual(response.status_code, 404)
