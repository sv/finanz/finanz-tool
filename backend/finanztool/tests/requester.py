from django.contrib.auth.hashers import make_password
from django.test import TestCase
from rechnungstool.models import Requester
from rechnungstool.tests import util


class RequesterTest(TestCase):
    def setUp(self):
        self.data = util.TestData()
        # Login as superuser
        self.client.force_login(self.data.superuser)

    def test_get_by_name(self) -> None:
        # delete all requesters and test None fallback
        self.data.receivedinvoice.delete()
        self.data.budget_receiver.delete()
        self.data.event_budget_receiver.delete()
        self.data.requester_haus.delete()
        self.data.requester_event.delete()
        self.assertIsNone(Requester.get_by_name("No U"))

        # create default fallback
        self.requester_unknown = util.cs(
            Requester(
                fond=self.data.fond_haus,
                name="XXUnbekannt",
                cost_center="K05T5TELL4",
                password=make_password("1234"),
            )
        )
        # create random requester
        self.requester_unknown.user.add(self.data.user)
        util.cs(self.requester_unknown)
        self.requester_lappen = util.cs(
            Requester(
                fond=self.data.fond_haus,
                name="Fachschaft Lappen",
                cost_center="K05T5TELL4",
                password=make_password("1234"),
            )
        )
        self.requester_lappen.user.add(self.data.user)
        util.cs(self.requester_lappen)

        # test
        self.assertEqual(Requester.get_by_name("Fachschaft Lappen"), self.requester_lappen)
        self.assertEqual(Requester.get_by_name("Why so serious"), self.requester_unknown)
