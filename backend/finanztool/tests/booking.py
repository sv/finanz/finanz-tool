from decimal import Decimal

from django.core.exceptions import ValidationError
from django.test import TestCase

from finanztool.core.models import InternalBooking
from finanztool.tests import util


# Create your tests here.
class BookingTest(TestCase):
    def setUp(self):
        self.data = util.TestData()
        # Login as superuser
        self.client.force_login(self.data.superuser)

    def test_amount_in_words_validation(self):
        # below 1000 euro no amount in words is necessary
        util.cs(
            InternalBooking(
                amount=Decimal("999.99"),
                annual_plan=self.data.annual_plan,
                internal_booking_number=40,
                name="Muster Booking",
                sender=self.data.requester_event,
                requester=self.data.requester_event_2,
            )
        )

        # over 1000 euro amount in words is necessary
        util.cs(
            InternalBooking(
                amount=Decimal("1000.00"),
                annual_plan=self.data.annual_plan,
                internal_booking_number=42,
                total_in_words="eintausend 0/100",
                name="Muster Booking",
                sender=self.data.requester_event,
                requester=self.data.requester_event_2,
            )
        )

        try:
            util.cs(
                InternalBooking(
                    amount=Decimal("1000.00"),
                    annual_plan=self.data.annual_plan,
                    internal_booking_number=43,
                    name="Muster Booking",
                    sender=self.data.requester_event,
                    requester=self.data.requester_event_2,
                )
            )
            self.fail("Total in words should be necessarily added")
        except ValidationError as ex:
            self.assertIn("Betrag in Worten ab 1000", ex.__str__())

        try:
            util.cs(
                InternalBooking(
                    amount=Decimal("1000.00"),
                    annual_plan=self.data.annual_plan,
                    internal_booking_number=43,
                    name="Muster Booking",
                    sender=self.data.requester_event,
                    requester=self.data.requester_event_2,
                    total_in_words="this is wrong",
                )
            )
            self.fail("Total in words should be necessarily added")
        except ValidationError as ex:
            self.assertIn("Betrag in Worten passt nicht zum Gesamtbetrag", ex.__str__())
